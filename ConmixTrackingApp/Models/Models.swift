//
//  Models.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 06/05/21.
//

import Foundation
import UIKit

var strVehicleID: String? = nil
var strOrderId: String? = nil
//var royaltyQty: Double? = nil

protocol AppCodable: Codable {}

public struct AppData<T: Codable>: Codable {
  public var data: T
}

public struct AppSessionData: AppCodable {
  
  #if DEBUG
  var accessToken = Constants.APIConstants.accessToken
  var registrationToken = UUID().uuidString
  #else
  var accessToken = Constants.APIConstants.accessToken
  var registrationToken = UUID().uuidString
  #endif
}

public struct SessionToken: Codable {
  
  public var token : String?
  
  public init(token: String?) {
    self.token = token
  }
  
  public enum CodingKeys: String, CodingKey {
    case token
  }
}

public struct CustomToken: Codable {
  
  public var customToken : String?
  
  public init(token: String?) {
    self.customToken = token
  }
  
  public enum CodingKeys: String, CodingKey {
    case customToken
  }
}

public struct Code: Codable {
  
  public var code : String?

  public init(code: String?) {
    self.code = code
  }
  
  enum CodingKeys: String, CodingKey {
    case code
  }
}

public struct TMDetails: Codable {

    public var _id: String?
    public var tMCategoryId: String?
    public var tMSubCategoryId: String?
    public var tMRcNumber: String?
    public var perCuMtrKm: Double?
    public var pickupLocation: Location?
    public var line1: String?
    public var line2: String?
    public var pincode: Double?
    public var cityName: String?
    public var stateName: String?
    public var deliveryRange: Double?
    public var minTripPrice: Double?
    public var manufacturerName: String?
    public var manufactureYear: Double?
    public var tMModel: String?
    public var isActive: Bool?
    public var isGpsEnabled: Bool?
    public var isInsuranceActive: Bool?
    public var rcBookImageUrl: String?
    public var insuranceImageUrl: String?
    public var tMImageUrl: String?
    public var userId: String?
    public var createdAt: String?
    public var tMCategory: TMCategoryInfo?
    public var tMSubCategory: TMSubCategoryInfo?
    public var driver1Info: DriverInfo?
    public var driver2Info: DriverInfo?
    public var stateId: String?
    public var calculatedTripPrice: Double?

    public init(_id: String?, tMCategoryId: String?, tMSubCategoryId: String?, tMRcNumber: String?, perCuMtrKm: Double?, pickupLocation: Location?, line1: String?, line2: String?, pincode: Double?, cityName: String?, stateName: String?, deliveryRange: Double?, minTripPrice: Double?, manufacturerName: String?, manufactureYear: Double?, tMModel: String?, isActive: Bool?, isGpsEnabled: Bool?, isInsuranceActive: Bool?, rcBookImageUrl: String?, insuranceImageUrl: String?, tMImageUrl: String?, userId: String?, createdAt: String?, tMCategory: TMCategoryInfo?, tMSubCategory: TMSubCategoryInfo?, driver1Info: DriverInfo?, driver2Info: DriverInfo?, stateId: String?, calculatedTripPrice: Double?) {
        self._id = _id
        self.tMCategoryId = tMCategoryId
        self.tMSubCategoryId = tMSubCategoryId
        self.tMRcNumber = tMRcNumber
        self.perCuMtrKm = perCuMtrKm
        self.pickupLocation = pickupLocation
        self.line1 = line1
        self.line2 = line2
        self.pincode = pincode
        self.cityName = cityName
        self.stateName = stateName
        self.deliveryRange = deliveryRange
        self.minTripPrice = minTripPrice
        self.manufacturerName = manufacturerName
        self.manufactureYear = manufactureYear
        self.tMModel = tMModel
        self.isActive = isActive
        self.isGpsEnabled = isGpsEnabled
        self.isInsuranceActive = isInsuranceActive
        self.rcBookImageUrl = rcBookImageUrl
        self.insuranceImageUrl = insuranceImageUrl
        self.tMImageUrl = tMImageUrl
        self.userId = userId
        self.createdAt = createdAt
        self.tMCategory = tMCategory
        self.tMSubCategory = tMSubCategory
        self.driver1Info = driver1Info
        self.driver2Info = driver2Info
        self.stateId = stateId
        self.calculatedTripPrice = calculatedTripPrice
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case tMCategoryId = "TM_category_id"
        case tMSubCategoryId = "TM_sub_category_id"
        case tMRcNumber = "TM_rc_number"
        case perCuMtrKm = "per_Cu_mtr_km"
        case pickupLocation = "pickup_location"
        case line1
        case line2
        case pincode
        case cityName = "city_name"
        case stateName = "state_name"
        case deliveryRange = "delivery_range"
        case minTripPrice = "min_trip_price"
        case manufacturerName = "manufacturer_name"
        case manufactureYear = "manufacture_year"
        case tMModel = "TM_model"
        case isActive = "is_active"
        case isGpsEnabled = "is_gps_enabled"
        case isInsuranceActive = "is_insurance_active"
        case rcBookImageUrl = "rc_book_image_url"
        case insuranceImageUrl = "insurance_image_url"
        case tMImageUrl = "TM_image_url"
        case userId = "user_id"
        case createdAt = "created_at"
        case tMCategory = "TM_category"
        case tMSubCategory = "TM_sub_category"
        case driver1Info = "driver1_info"
        case driver2Info = "driver2_info"
        case stateId = "state_id"
        case calculatedTripPrice = "calculated_trip_price"
    }


}




public struct OrderDetails: Codable {
  
  public var order: VendorOrderDetailsForTracking?
  
  public init(order: VendorOrderDetailsForTracking?) {
    self.order = order
  }
  
  public enum CodingKeys: String, CodingKey {
    case order
  }
  
}

public struct OrderList: Codable {
  
  public var order: [VendorOrderDetailsForTracking]?
  
  public init(order: [VendorOrderDetailsForTracking]?) {
    self.order = order
  }
  
  public enum CodingKeys: String, CodingKey {
    case order
  }
  
}

public struct VendorOrderDetailsForTracking: Codable {

    public var _id: String?
    public var assignedAt: String?
    public var quantity: Double?
    public var pickupQuantity: Double?
    public var remainingQuantity: Double?
    public var eventStatus: String?
    public var pickedupAt: String?
    public var createdAt: String?
    public var challanImageUrl: String?
    public var royaltyPassImageUrl: String?
    public var waySlipImageUrl: String?
    public var invoiceImageUrl: String?
    public var supplierPickupSignature: String?
    public var driverPickupSignature: String?
    public var buyerDropSignature: String?
    public var driverDropSignature: String?
    public var tMInfo: TMInfo?
    public var driver1Info: DriverInfo?
    public var tMCategoryInfo: TMCategoryInfo?
    public var tMSubCategoryInfo: TMSubCategoryInfo?
    public var buyerInfo: BuyerInfo?
    public var vendorInfo: VendorInfo?
    public var supplierInfo: SupplierInfo?
    public var vendorOrderInfo: VendorOrderInfo?
    public var pickupAddressInfo: PickUpAddressInfo?
    public var deliveryAddressInfo: DeliveryAddressInfo?
    public var orderInfo: OrderInfo?
    public var orderItemInfo: OrderItemInfo?
    public var CPInfo: ConcretePumpDetails?
    public var CPCategoryInfo: ConcretePumpCategoriesDetails?
    public var operatorInfo: OperatorInfo?
    public var invoiceUrl: String?
    
  
  public init(_id: String?, assignedAt: String?, quantity: Double?, pickupQuantity: Double?, remainingQuantity: Double?, eventStatus: String?, pickedupAt: String?, createdAt: String?, challanImageUrl: String?, royaltyPassImageUrl: String?, waySlipImageUrl: String?, invoiceImageUrl: String?, supplierPickupSignature: String?, driverPickupSignature: String?, buyerDropSignature: String?, driverDropSignature: String?, tMInfo: TMInfo?, driver1Info: DriverInfo?, tMCategoryInfo: TMCategoryInfo?, tMSubCategoryInfo: TMSubCategoryInfo?, buyerInfo: BuyerInfo?, vendorInfo: VendorInfo?, supplierInfo: SupplierInfo?, vendorOrderInfo: VendorOrderInfo?, pickupAddressInfo: PickUpAddressInfo?, deliveryAddressInfo: DeliveryAddressInfo?, orderInfo: OrderInfo?, orderItemInfo: OrderItemInfo?, CPInfo: ConcretePumpDetails?, CPCategoryInfo: ConcretePumpCategoriesDetails?, operatorInfo: OperatorInfo?, invoiceUrl: String?) {
        self._id = _id
        self.assignedAt = assignedAt
        self.quantity = quantity
        self.pickupQuantity = pickupQuantity
        self.remainingQuantity = remainingQuantity
        self.eventStatus = eventStatus
        self.pickedupAt = pickedupAt
        self.createdAt = createdAt
        self.challanImageUrl = challanImageUrl
        self.royaltyPassImageUrl = royaltyPassImageUrl
        self.waySlipImageUrl = waySlipImageUrl
        self.invoiceImageUrl = invoiceImageUrl
        self.supplierPickupSignature = supplierPickupSignature
        self.driverPickupSignature = driverPickupSignature
        self.buyerDropSignature = buyerDropSignature
        self.driverDropSignature = driverDropSignature
        self.tMInfo = tMInfo
        self.driver1Info = driver1Info
        self.tMCategoryInfo = tMCategoryInfo
        self.tMSubCategoryInfo = tMSubCategoryInfo
        self.buyerInfo = buyerInfo
        self.vendorInfo = vendorInfo
        self.supplierInfo = supplierInfo
        self.vendorOrderInfo = vendorOrderInfo
        self.pickupAddressInfo = pickupAddressInfo
        self.deliveryAddressInfo = deliveryAddressInfo
        self.orderInfo = orderInfo
        self.orderItemInfo = orderItemInfo
        self.CPInfo = CPInfo
        self.CPCategoryInfo = CPCategoryInfo
        self.operatorInfo = operatorInfo
        self.invoiceUrl = invoiceUrl
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case assignedAt = "assigned_at"
        case quantity
        case pickupQuantity = "pickup_quantity"
        case remainingQuantity = "remaining_quantity"
        case eventStatus = "event_status"
        case pickedupAt = "pickedup_at"
        case createdAt = "created_at"
        case challanImageUrl = "challan_image_url"
        case royaltyPassImageUrl = "royalty_pass_image_url"
        case waySlipImageUrl = "way_slip_image_url"
        case invoiceImageUrl = "invoice_image_url"
        case supplierPickupSignature = "supplier_pickup_signature"
        case driverPickupSignature = "driver_pickup_signature"
        case buyerDropSignature = "buyer_drop_signature"
        case driverDropSignature = "driver_drop_signature"
        case tMInfo = "TM_info"
        case driver1Info = "driver1_info"
        case tMCategoryInfo = "TM_category_info"
        case tMSubCategoryInfo = "TM_sub_category_info"
        case buyerInfo = "buyer_info"
        case vendorInfo = "vendor_info"
        case supplierInfo = "supplier_info"
        case vendorOrderInfo = "vendor_order_info"
        case pickupAddressInfo = "pickup_address_info"
        case deliveryAddressInfo = "delivery_address_info"
        case orderInfo = "order_info"
        case orderItemInfo = "order_item_info"
        case CPInfo = "CP_info"
        case CPCategoryInfo = "CP_category_info"
        case operatorInfo = "operator_info"
        case invoiceUrl = "invoice_url"
    }
}

public struct TMInfo: Codable {
  
  public var _id: String?
  public var tMRCNumber: String?
  public var tMCategoryID: String?
  public var tMSubCategoryID: String?
  public var tMImageUrl: String?
  
  public init(_id: String?, tMRCNumber: String?, tMCategoryID: String?, tMSubCategoryID: String?, tMImageUrl: String?) {
    
    self._id = _id
    self.tMRCNumber = tMRCNumber
    self.tMCategoryID = tMCategoryID
    self.tMSubCategoryID = tMSubCategoryID
    self.tMImageUrl = tMImageUrl
    
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case tMRCNumber = "TM_rc_number"
    case tMCategoryID = "TM_category_id"
    case tMSubCategoryID = "TM_sub_category_id"
    case tMImageUrl = "TM_image_url"
    
  }
}

public struct DriverInfo: Codable {
  
  public var driverName: String?
  public var driverMobileNumber: String?
  
  public init(driverName: String?, driverMobileNumber: String?, tMCategoryID: String?, tMSubCategoryID: String?, tMImageUrl: String?) {
    
    self.driverName = driverName
    self.driverMobileNumber = driverMobileNumber
    
  }
  
  public enum CodingKeys: String, CodingKey {
    case driverName = "driver_name"
    case driverMobileNumber = "driver_mobile_number"
  }
}

public struct TMCategoryInfo: Codable {
  
  public var _id: String?
  public var tMCategoryName: String?
  
  public init(_id: String?, tMCategoryName: String?) {
    
    self._id = _id
    self.tMCategoryName = tMCategoryName
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case tMCategoryName = "category_name"
  }
}

public struct TMSubCategoryInfo: Codable {
  
  public var _id: String?
  public var tMSubCategoryName: String?
  public var loadCapacity: Double?
  
  public init(_id: String?, tMSubCategoryName: String?, loadCapacity: Double?) {
    
    self._id = _id
    self.tMSubCategoryName = tMSubCategoryName
    self.loadCapacity = loadCapacity
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case tMSubCategoryName = "sub_category_name"
    case loadCapacity = "load_capacity"
  }
}

public struct BuyerInfo: Codable {
  
  public var _id: String?
  public var fullName: String?
  public var mobileNumber: String?
  public var companyName: String?
  
  public init(_id: String?, fullName: String?, mobileNumber: String?, companyName: String?) {
    
    self._id = _id
    self.fullName = fullName
    self.mobileNumber = mobileNumber
    self.companyName = companyName
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case fullName = "full_name"
    case mobileNumber = "mobile_number"
    case companyName = "company_name"
  }
}

public struct VendorInfo: Codable {
  
  public var _id: String?
  public var fullName: String?
  public var mobileNumber: String?
  public var companyName: String?
  public var addressID: String?
  public var createdAt: String?
  
  public init(_id: String?, fullName: String?, mobileNumber: String?, companyName: String?, addressID: String?, createdAt: String?) {
    
    self._id = _id
    self.fullName = fullName
    self.mobileNumber = mobileNumber
    self.companyName = companyName
    self.addressID = addressID
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case fullName = "full_name"
    case mobileNumber = "mobile_number"
    case companyName = "company_name"
    case addressID  = "address_id"
    case createdAt = "created_at"
  }
}

public struct SupplierInfo: Codable {
  
  public var _id: String?
  public var fullName: String?
  public var mobileNumber: String?
  
  public init(_id: String?, fullName: String?, mobileNumber: String?) {
    
    self._id = _id
    self.fullName = fullName
    self.mobileNumber = mobileNumber
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case fullName = "full_name"
    case mobileNumber = "mobile_number"
  }
}

public struct VendorOrderInfo: Codable {
  
  public var _id: String?
  public var quantity: Double?
  public var createdAt: String?
  
  public init(_id: String?, quantity: Double?, createdAt: String?) {
    
    self._id = _id
    self.quantity = quantity
    self.createdAt = createdAt
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case quantity
    case createdAt = "created_at"
  }
}

public struct PickUpAddressInfo: Codable {
  
  public var _id: String?
  public var line1: String?
  public var line2: String?
  public var pincode: Double?
  public var cityName: String?
  public var stateName: String?
  public var pickupLocation: Location?
  
  public init(_id: String?, line1: String?, line2: String?, pincode: Double?, cityName: String?, stateName: String?, pickupLocation: Location?) {
    
    self._id = _id
    self.line1 = line1
    self.line2 = line2
    self.pincode = pincode
    self.cityName = cityName
    self.stateName = stateName
    self.pickupLocation = pickupLocation
    
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case line1
    case line2
    case pincode
    case cityName = "city_name"
    case stateName = "state_name"
    case pickupLocation = "pickup_location"
  }
}

public struct Location: Codable {
  
  public var type: String?
  public var coordinates: [Double]?
  
  public init(type: String?, coordinates: [Double]?) {
    self.type = type
    self.coordinates = coordinates
  }
  
  public enum CodingKeys: String, CodingKey {
    case type
    case coordinates
  }
  
}

public struct DeliveryAddressInfo: Codable {
  
  public var _id: String?
  public var line1: String?
  public var line2: String?
  public var pincode: Double?
  public var cityName: String?
  public var stateName: String?
  public var deliveryLocation: Location?
  
  public init(_id: String?, line1: String?, line2: String?, pincode: Double?, cityName: String?, stateName: String?, deliveryLocation: Location?) {
    
    self._id = _id
    self.line1 = line1
    self.line2 = line2
    self.pincode = pincode
    self.cityName = cityName
    self.stateName = stateName
    self.deliveryLocation = deliveryLocation
    
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case line1 =  "line1"
    case line2 = "line2"
    case pincode
    case cityName = "city_name"
    case stateName = "state_name"
    case deliveryLocation = "delivery_location"
  }
}

public struct OrderInfo: Codable {
  
  public var _id: String?
  
  public init(_id: String?) {
    
    self._id = _id
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
  }
}

public struct OrderItemInfo: Codable {
  
  public var _id: String?
  
  public init(_id: String?) {
    
    self._id = _id
  }
  
  
  public enum CodingKeys: String, CodingKey {
    case _id
  }
}


public struct ConcretePumpDetails: Codable {

    public var _id: String?
    public var companyName: String?
    public var concretePumpModel: String?
    public var manufactureYear: Double?
    public var serialNumber: String?
    public var pipeConnection: Double?
    public var concretePumpCapacity: Double?
    public var isOperator: Bool?
    public var operatorId: String?
    public var isHelper: Bool?
    public var transportationCharge: Double?
    public var concretePumpPrice: Double?
    public var concretepumpcatID: String?
    public var concretePumpImageUrl: String?
    public var createdAt: String?
    public var concretePumpCategory: ConcretePumpCategoriesDetails?
    public var operatorInfo: OperatorInfo?

  public init(_id: String?, companyName: String?, concretePumpModel: String?, manufactureYear: Double?, pipeConnection: Double?, concretePumpCapacity: Double?, isOperator: Bool?, operatorId: String?, isHelper: Bool?, transportationCharge: Double?, concretePumpPrice: Double?, concretepumpcatID: String?, concretePumpImageUrl: String?, createdAt: String?, concretePumpCategory: ConcretePumpCategoriesDetails?, operatorInfo: OperatorInfo?, serialNumber: String?) {
        self._id = _id
        self.companyName = companyName
        self.concretePumpModel = concretePumpModel
        self.manufactureYear = manufactureYear
        self.pipeConnection = pipeConnection
        self.concretePumpCapacity = concretePumpCapacity
        self.isOperator = isOperator
        self.operatorId = operatorId
        self.isHelper = isHelper
        self.transportationCharge = transportationCharge
        self.concretePumpPrice = concretePumpPrice
        self.concretepumpcatID = concretepumpcatID
        self.concretePumpImageUrl = concretePumpImageUrl
        self.createdAt = createdAt
        self.concretePumpCategory = concretePumpCategory
        self.operatorInfo = operatorInfo
        self.serialNumber = serialNumber
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case companyName = "company_name"
        case concretePumpModel = "concrete_pump_model"
        case manufactureYear = "manufacture_year"
        case pipeConnection = "pipe_connection"
        case concretePumpCapacity = "concrete_pump_capacity"
        case isOperator = "is_Operator"
        case operatorId = "operator_id"
        case isHelper = "is_helper"
        case transportationCharge = "transportation_charge"
        case concretePumpPrice = "concrete_pump_price"
        case concretepumpcatID = "concrete_pump_category_id"
        case concretePumpImageUrl = "concrete_pump_image_url"
        case createdAt = "created_at"
        case concretePumpCategory = "concrete_pump_category"
        case operatorInfo = "operator_info"
        case serialNumber = "serial_number"
    }


}

public struct ConcretePumpCategoriesDetails: Codable {

    public var _id: String?
    public var categoryName: String?
    public var createdAt: String?
    public var isActive: Bool?
    public var updatedAt: String?

    public init(_id: String, categoryName: String, createdAt: String, isActive: Bool?, updatedAt: String?) {
        self._id = _id
        self.categoryName = categoryName
        self.createdAt = createdAt
        self.isActive = isActive
        self.updatedAt = updatedAt
    }

    public enum CodingKeys: String, CodingKey {
        case _id
        case categoryName = "category_name"
        case createdAt = "created_at"
        case isActive = "is_active"
        case updatedAt = "updated_at"
    }


}

public struct OperatorInfo: Codable {
  
  public var _id: String?
  public var operatorName: String?
  public var operatorMobileNumber: String?
  
  public init(_id: String?, operatorName: String?, operatorMobileNumber: String?) {
    self._id = _id
    self.operatorName = operatorName
    self.operatorMobileNumber = operatorMobileNumber
  }
  
  public enum CodingKeys: String, CodingKey {
    case _id
    case operatorName = "operator_name"
    case operatorMobileNumber = "operator_mobile_number"
  }
  
  
}




public struct UploadDocument {
  
  public var title: String?
  public var img: UIImage?
  public var data: Data?
  public var name: String?
  public var fileName: String?
}


