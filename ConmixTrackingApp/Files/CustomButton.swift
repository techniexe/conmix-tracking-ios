//
//  CustomButton.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 10/03/21.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {

  @IBInspectable var cornerRadius: CGFloat = 0 {
      didSet {
          layer.cornerRadius = cornerRadius
          layer.masksToBounds = cornerRadius > 0
      }
  }
  @IBInspectable var borderWidth: CGFloat = 0.0 {
      didSet {
          layer.borderWidth = borderWidth
      }
  }
  @IBInspectable var borderColor: UIColor? {
      didSet {
          layer.borderColor = borderColor?.cgColor
      }
  }
  
}




