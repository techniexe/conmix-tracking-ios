//
//  GlobalFunction.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 08/03/21.
//

import UIKit

@objc protocol CustomProtocol {
  
  @objc optional func collectionView(_ indexPath: IndexPath)
  @objc optional func pressSeeAll()
  @objc optional func addNewAddressSite()
}

class GlobalFunction: NSObject {

  //MARK:- No Network Connection
  class func noNetworkConnection() {
    Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
  }

  //MARK:- Set RootView Controller
  class func setRootViewController(direction: UIWindow.TransitionOptions.Direction) {
    
    guard let alredyLogIn = UserDefaults.standard.bool(forKey:Constants.UserDefaults.AlreadyLogIn)  as Bool?, alredyLogIn == true  else {
      
      openLoginScreen(direction: direction)
      
      return
    }
    
    if let objSelectionListVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"SelectionListVC") as? SelectionListVC {
      let navController = UINavigationController(rootViewController:objSelectionListVC)
      navController.setNavigationBarHidden(true, animated: false)
      Constants.kAppDelegate.window?.setRootViewController(navController, options: UIWindow.TransitionOptions(direction: .toRight))
    }
    
//    if let objTMListVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"TransitMixerListVC") as? TransitMixerListVC {
//      let navController = UINavigationController(rootViewController:objTMListVC)
//      navController.setNavigationBarHidden(true, animated: false)
//      Constants.kAppDelegate.window?.setRootViewController(navController, options: UIWindow.TransitionOptions(direction: .toRight))
//    }
    
  }
  
  
  // MARK: - Remove Default Setting When User Logout
  class func removeDefaultSettingWhenLogOut() {
    
    Helper.clearDocumentDirectory()
    
    UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.AlreadyLogIn)
    UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.UserType)
    UserDefaults.standard.removeObject(forKey:Constants.UserDefaults.CustomToken)
    
    openLoginScreen(direction: UIWindow.TransitionOptions.Direction.toLeft)
  }
  
  
  // MARK:- Open Login Screen
  class func openLoginScreen(direction: UIWindow.TransitionOptions.Direction) {
    
    if let objLoginVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"LoginVC") as? LoginVC {
      let navController = UINavigationController(rootViewController:objLoginVC)
      navController.setNavigationBarHidden(true, animated: false)
      Constants.kAppDelegate.window?.setRootViewController(navController, options: UIWindow.TransitionOptions(direction: direction))
    }
    
    
  }

  
  //MARK: - Check User Login Or Not
  class func checkLoggedInUser(_ completion: @escaping(Bool) -> Void) {

    if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.AlreadyLogIn) == true {
      if let alredyLogIn = UserDefaults.standard.bool(forKey:Constants.UserDefaults.AlreadyLogIn)  as Bool?, alredyLogIn == true  {
        completion(true)
      }else {
       completion(false)
      }
    }else{
      completion(false)
    }

  }
  
  //MARK: - Convert Lat Long to String
//  class func getStringFromLatLong(_ lat: Double, long: Double) -> String {
//
//    let latitude = String(format: "%.05f", lat)
//
//    let longitude = String(format: "%.05f", long)
//
//    return "\(latitude), \(longitude)"
//
//  }
  
  //MARK:- Get Stored City Details
//  class func getStoredCityDetails() -> CityDetails? {
//    let defaults = UserDefaults.standard
//    if let cityDetail = defaults.object(forKey: Constants.UserDefaults.CityDetails) as? Data {
//      let decoder = JSONDecoder()
//      if let cityDetails = try? decoder.decode(CityDetails.self, from: cityDetail) {
//        return cityDetails
//      }else {
//        return nil
//      }
//    }else {
//      return nil
//    }
//  }
//
  //MARK:- Add Site Details
//  class func addSiteDetails(_ siteDetails: BuyersiteDetails) -> CityDetails {
//
//    let cityDetail =  CityDetails(_id: siteDetails.cityId, location: siteDetails.location, cityName: siteDetails.cityName, stateId: siteDetails.stateId, createdAt: siteDetails.createdAt, countryCode: siteDetails.countryCode, countryId: siteDetails.countryId, countryName: siteDetails.countryName, stateName: siteDetails.stateName, popularity: nil, siteId: siteDetails._id, siteName: siteDetails.siteName, formattedAddress: nil)
//
//    let encoder = JSONEncoder()
//
//    if let encoded = try? encoder.encode(cityDetail) {
//      let defaults = UserDefaults.standard
//      defaults.set(encoded, forKey: Constants.UserDefaults.CityDetails)
//    }else {
//      Helper.DLog(message: "Not Found City Details")
//    }
//
//    return cityDetail
//  }
//
  //MARK:- Add Delivery Date
//  class func addDeliveryDate(_ date: Date) -> Bool {
//
//    let deliveryDateDetails = DeliveryDateDetails(deliverydate: date)
//
//    let encoder = JSONEncoder()
//
//    if let encoded = try? encoder.encode(deliveryDateDetails) {
//      let defaults = UserDefaults.standard
//      defaults.set(encoded, forKey: Constants.UserDefaults.DeliveryDateDetails)
//      return true
//    }else {
//      return false
//    }
//
//  }
  
  //MARK:- Get Delivery Date
//  class func getDeliveryDate() -> DeliveryDateDetails? {
//
//    let defaults = UserDefaults.standard
//
//    if let dataResponse = defaults.object(forKey: Constants.UserDefaults.DeliveryDateDetails) as? Data {
//      let decoder = JSONDecoder()
//      if let deliveryDateDetails = try? decoder.decode(DeliveryDateDetails.self, from: dataResponse) {
//        return deliveryDateDetails
//      }else {
//        return nil
//      }
//    }else {
//      return nil
//    }
//  }
  
  //MARK:- Add Site Details
//  class func addCustomMixInfo(_ addCustomMix: AddCustomMix) {
//    
//    let encoder = JSONEncoder()
//    
//    if let encoded = try? encoder.encode(addCustomMix) {
//      let defaults = UserDefaults.standard
//      defaults.set(encoded, forKey: Constants.UserDefaults.AddCustomMixDetail)
//    }else {
//    }
//    
//    
//  }
  
  //MARK:- Get Delivery Date
//  class func getSaveCustoMixInfo() -> AddCustomMix? {
//    
//    let defaults = UserDefaults.standard
//   
//    if let dataResponse = defaults.object(forKey: Constants.UserDefaults.AddCustomMixDetail) as? Data {
//      let decoder = JSONDecoder()
//      if let cusotmMixDetails = try? decoder.decode(AddCustomMix.self, from: dataResponse) {
//        return cusotmMixDetails
//      }else {
//        return nil
//      }
//    }else {
//      return nil
//    }
//  }
//  
//  
//  //MARK:- Add Buyer TM Detail
//  class func addBuyerTMDetail(_ buyerTMDetail: BuyerTMDetail) -> Bool {
//    
//    let encoder = JSONEncoder()
//    
//    if let encoded = try? encoder.encode(buyerTMDetail) {
//      let defaults = UserDefaults.standard
//      defaults.set(encoded, forKey: Constants.UserDefaults.BuyerTMDetail)
//      return true
//    }else {
//      return false
//    }
//  
//  }
//  
//  //MARK:- Get Buyer TM Detail
//  class func getBuyerTMDetail() -> BuyerTMDetail? {
//    
//    let defaults = UserDefaults.standard
//   
//    if let dataResponse = defaults.object(forKey: Constants.UserDefaults.BuyerTMDetail) as? Data {
//      let decoder = JSONDecoder()
//      if let buyerTMDetails = try? decoder.decode(BuyerTMDetail.self, from: dataResponse) {
//        return buyerTMDetails
//      }else {
//        return nil
//      }
//    }else {
//      return nil
//    }
//  }
  
}
