//
//  Constants.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 06/05/21.
//

import Foundation
import UIKit

struct Constants {
  
  static let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
  static let kMainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
  static let kAcceptableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  static let googleApiKey = "AIzaSyDgc0AfDO-QPVxfx03eXhLBphZ3CKwYp5o"
  static let OTPSeconds = 120
  
  struct Application {
    static let name = "ConmixTrackingApp"
  }
  
  struct APIConstants {
    static let accessToken = "IGVhY2ggcGFuZWwgYWZ0ZQ"
    static let baseDevAPIURL = "http://192.168.1.154:3033/v1/"
    static let baseProdAPIURL = "http://192.168.1.154:3033/v1/"
  }
  
  struct APIPaths {
    
   
    static let sessions = "sessions"
    static let loginMobile = "vendor/auth/\(Placeholders.identifier)"
    static let requestMobileOTP = "vendor/order_track/requestOTP/\(Placeholders.mobileNumber)"
    static let mobileOTPVerification = "vendor/order_track/mobile/\(Placeholders.mobileNumber)/code/\(Placeholders.code)"
    
    /********** Transit Mixer (TM) **********/
    static let getVendorTMList = "Vendor/TM"
    static let getVendorTMDetails = "Vendor/TM/\(Placeholders.tmID)"
    static let tmAssignedToOrder = "vendor/order_track/\(Placeholders.orderID)/\(Placeholders.orderItemID)"
    static let orderByTMID = "vendor/order_track/\(Placeholders.tmID)"
    static let changeOrderQty = "vendor/order_track/\(Placeholders.trackID)/\(Placeholders.tmID)/\(Placeholders.orderID)"
    static let changeOrderStatus = "vendor/order_track/\(Placeholders.trackID)/\(Placeholders.tmID)/\(Placeholders.orderID)/pickup"
    static let uploadDocument = "vendor/order_track/\(Placeholders.trackID)/\(Placeholders.tmID)/\(Placeholders.orderID)/uploadDoc"
    static let deliverOrder = "vendor/order_track/\(Placeholders.tmID)/\(Placeholders.orderdID)/\(Placeholders.trackID)/deliver"
    static let delayOrder = "vendor/order_track/\(Placeholders.tmID)/\(Placeholders.orderdID)/\(Placeholders.trackID)/delay"
    static let orderDetails = "vendor/order_track/OrderDetails/\(Placeholders.orderID)/\(Placeholders.trackID)"
    static let orderVendorOrderTrackDetails = "vendor/order_track/\(Placeholders.orderItemID)/\(Placeholders.trackingID)"
    
    /********** Concrete Pump (CP) **********/
    static let getVendorCPList = "vendor/concrete_pump"
    static let orderByCPID = "vendor/order_track/CP/\(Placeholders.cpID)"
    static let cpChangeOrderQty = "vendor/order_track/CP/\(Placeholders.trackID)/\(Placeholders.cpID)/\(Placeholders.orderID)"
    static let cpOrderDetails = "vendor/order_track/CP/OrderDetails/\(Placeholders.orderID)/\(Placeholders.trackID)"
    static let cpUploadDocument = "vendor/order_track/CP/\(Placeholders.trackID)/\(Placeholders.cpID)/\(Placeholders.orderID)/uploadDoc"
    static let cpChangeOrderStatus = "vendor/order_track/CP/\(Placeholders.trackID)/\(Placeholders.cpID)/\(Placeholders.orderID)/pickup"
    static let cpDeliverOrder = "vendor/order_track/CP/\(Placeholders.cpID)/\(Placeholders.orderdID)/\(Placeholders.trackID)/deliver"
    static let cpDelayOrder = "vendor/order_track/CP/\(Placeholders.cpID)/\(Placeholders.orderdID)/\(Placeholders.trackID)/delay"
    static let checkCPStatus = "vendor/order_track/checkCPStatus/\(Placeholders.orderID)"
    static let uploadInvoice = "vendor/order_track/uploadInvoice"
    static let uploadVendorInvoice = "vendor/order_track/uploadVendorInvoice"
  }
  
  struct Placeholders {
    static let identifier = "{identifier}"
    static let mobileNumber = "{mobile_number}"
    static let code = "{code}"
    static let orderID = "{order_id}"
    static let orderdID = "{orderId}"
    static let orderItemID = "{order_item_id}"
    static let tmID = "{TMId}"
    static let trackID = "{track_id}"
    static let trackingID = "{tracking_id}"
    static let cpID = "{CPId}"
  }
  
  struct UserDefaults {
    static let CurrentLocation = "CurrentLocation"
    static let CustomToken = "CustomToken"
    static let UserType = "UserType"
    static let AlreadyLogIn = "AlreadyLogIn"
    static let AppLanguage = "AppLanguage"
    static let OptionForDelivery = "OptionForDelivery"
    static let CPID = "CPID"
  }
  
  struct Colors {
    static let appThemeColor = UIColor(named: "AppThemeColor")
    static let appRedColor = UIColor(named: "AppRedColor")
    static let appBlackColor = UIColor(named: "AppBlackColor")
    static let blackColor = UIColor(hexString: "#1A1A1A")
    static let appBorderColor = UIColor(named: "AppBorderColor")
    static let lineColor = UIColor(named: "LineColor")
    static let lightGrayColor = UIColor(named: "LightGrayColor")
    static let delayedColor = UIColor(named: "DelayedColor")
    static let pickupColor = UIColor(named: "PickupColor")
    static let assignedColor = UIColor(named: "AssignedColor")
    
  }

  
  struct TableViewCell {
  }
  
  struct TableViewCellIdentifier {
    static let vehicleListCellID = "VehicleListCellID"
    static let vehicleDetailCellID = "VehicleDetailCellID"
    static let loadOrderCellID = "LoadOrderCellID"
    static let orderInfoCellID = "OrderInfoCellID"
    static let uploadDocumentListCellID = "UploadDocumentListCellID"
    static let orderDetailCellID = "OrderDetailCellID"
    static let orderListCellID = "OrderListCellID"
    static let CPDetailsCellID = "CPDetailsCellID"
    static let CPOrderDetailReceiptCellID = "CPOrderDetailReceiptCellID"
  }
  
  struct CollectionViewCell {
  }
  
  struct CollectionViewCellIdentifier {
    static let imageViewCollectionCellID = "ImageViewCollectionCellID"
  }
  
  struct NotificationName {
  }
  
  struct  CommonAlert {
    static let Logout = "Are you sure you want to logout?"
  }
  
  struct SuccessMessage {
    static let docuploadsuccess = "UploadSuccess"
    static let signuploadsuccess = "SignSuccess"
    static let pickupstatus = "OrderPickupSuccess"
    static let updateorderstatus =  "OrderStatusSuccess"
    static let orderdeliver = "OrderDeliveredSuccess"
    static let codesuccess = "OTPSent"
  }
  
  struct ErrorMessage {
    static let invalidcred = "Invalidcred"
    static let invalidmobileregioncode = "Invalidmobileregioncode"
    static let mobileemail = "Mobileemail"
    static let password = "Password"
    static let PasswordBlank = "PasswordBlank"
    static let passwordlength = "Passwordlength"
    static let invalidemail = "Invalidemail"
    static let invalidemailmobile = "Invalidemailmobile"
    static let noNetwork =  "NoNetwork"
    static let PickupQtyZero = "PickupQtyZero"
    static let RoyaltyPassZero = "RoyaltyPassZero"
    static let Royalynotgreaterpickup = "Royalynotgreaterpickup"
    static let noChallan = "NoChallan"
    static let noroyaltypass = "NoRoyaltypass"
    static let noewaybill = "NoEwaybill"
    static let noInvoice = "NoInvoice"
    static let NoBuyerSign = "NoBuyerSign"
    static let NoDriverSign = "NoDriverSign"
    static let NoSupplierSign = "NoSupplierSign"
    static let noreason = "NoReason"
    static let docuploaderror = "UploadError"
    static let sessionExpire = "SessionExpire"
    static let NoOperatorSign = "NoOperatorSign"
  }
  
  struct Alerts {
    static let GoForPickupMsg = "GoForPickupMsg"
    static let SetRoyaltyPassMsg = "SetRoyaltyPassMsg"
    static let RejectorderMsg = "RejectorderMsg"
    static let AcceptorderMsg = "AcceptorderMsg"
    static let LogoutMsg = "LogoutMsg"
  }
  
  struct Text {
    static let Welcome = "Welcome"
    static let Mobile = "Mobile"
    static let Password = "Password"
    static let Login = "Login"
    static let ChooseVehicle = "ChooseVehicle"
    static let NoVehicle = "NoVehicle"
    static let TruckDetail = "TruckDetail"
    static let VehicleSubCategory = "VehicleSubCategory"
    static let ManufacturerName = "ManufacturerName"
    static let ManufacturingYear = "ManufacturingYear"
    static let VehicleModel = "VehicleModel"
    static let Driver1Name = "Driver1Name"
    static let Driver1Contact = "Driver1Contact"
    static let Driver2Name = "Driver2Name"
    static let Driver2Contact = "Driver2Contact"
    static let VehicleLocation = "VehicleLocation"
    static let PlantLocation = "PlantLocation"
    static let CurrentLocation = "CurrentLocation"
    static let Next = "Next"
    static let OrderList = "OrderList"
    static let NoOrder = "NoOrder"
    static let OrderID = "OrderID"
    static let AssignedOn = "AssignedOn"
    static let Status = "Status"
    static let TruckAssign = "TruckAssigned"
    static let PickUp = "Pickup"
    static let Delay = "Delay"
    static let Accept = "Accept"
    static let NoAssignOrder = "NoAssignOrder"
    static let SupplierInfo = "SupplierInfo"
    static let PickupQty = "PickupQty"
    static let CompanyName = "CompanyName"
    static let SupplierName = "SupplierName"
    static let SupplierContact = "SupplierContact"
    static let PickupLocation = "PickupLocation"
    static let DeliveryLocation = "DeliveryLocation"
    static let BuyerInfo = "BuyerInfo"
    static let BuyerName = "BuyerName"
    static let BuyerContact = "BuyerContact"
    static let OrderDelivered = "OrderDelivered"
    static let LoadOrder = "LoadOrder"
    static let ChangeOrderQty = "ChangeOrderQty"
    static let RoyaltyPass = "RoyaltyPass"
    static let OrderInfo = "OrderInfo"
    static let NoOrderInfo = "NoOrderInfo"
    static let RejectOrder = "RejectOrder"
    static let Complaint = "Complaint"
    static let AcceptOrder = "AcceptOrder"
    static let UploadDocument = "UploadDocument"
    static let Done = "Done"
    static let Document = "Document"
    static let UploadRoyaltyPass = "UploadRoyaltyPass"
    static let UploadEWayBill = "UploadEWayBill"
    static let UploadChallan = "UploadChallan"
    static let UploadInvoice = "UploadInvoice"
    static let TakePoto = "TakePoto"
    static let DrawSignature = "DrawSignature"
    static let DrawBuyerSign = "DrawBuyerSign"
    static let DrawSupplierSign = "DrawSupplierSign"
    static let DrawDriverSign = "DrawDriverSign"
    static let Clear = "Clear"
    static let NoOrderFound = "NoOrderFound"
    static let ORDERID = "ORDERID"
    static let PickUpQty = "PickUpQty"
    static let BalanceQty = "BalanceQty"
    static let OrderQty = "OrderQty"
    static let RejectReason = "RejectReason"
    static let WriteReason = "WriteReason"
    static let WriteComplaint = "WriteComplaint"
    static let Yes = "Yes"
    static let No = "No"
    static let NoOrderReceipt = "NoOrderReceipt"
    static let RoyaltyImage = "RoyaltyImage"
    static let WaySlipImage = "WaySlipImage"
    static let ChallanImage = "ChallanImage"
    static let InvoiceImage = "InvoiceImage"
    static let BuyerInvoice = "BuyerInvoice"
    static let BuyerSign = "BuyerSign"
    static let DriverSign = "DriverSign"
    static let HeavyTraffic = "HeavyTraffic"
    static let WeatherConditions = "WeatherConditions"
    static let VehicleBreakdowns = "VehicleBreakdowns"
    static let Puncture = "Puncture"
    static let Accident = "Accident"
    static let DelayReason = "DelayReason"
    static let DelayTime = "DelayTime"
    static let ChooseReason = "ChooseReason"
    static let OTP = "OTP"
    static let OTPConfirmCode = "OTPConfirmCode"
    static let OTPText = "OTPText"
    static let OTPEmailText = "OTPEmailText"
    static let ResendCode = "ResendCode"
    static let Verify = "Verify"
    static let SelectLanguage = "SelectLanguage"
    static let Apply = "Apply"
    static let LogOut = "LogOut"
    
    static let ChooseTracking = "ChooseTracking"
    static let OR = "OR"
    static let ChooseDeliveryFor = "ChooseDeliveryFor"
    static let TransitMixer = "TransitMixer"
    static let ConcretePump = "ConcretePump"
    static let ChooseCP = "ChooseCP"
    static let NoCP = "NoCP"
    static let CPModel = "CPModel"
    static let OperatorName = "OperatorName"
    static let OperatorContact = "OperatorContact"
    static let SerialNumber = "SerialNumber"
    static let CPDetail = "CPDetail"
    static let CPAssigned = "CPAssigned"
    static let NoAssignCPOrder = "NoAssignCPOrder"
    static let DrawOperatorSign = "DrawOperatorSign"
    static let OperatorSign = "OperatorSign"
    static let DelayAlert = "DelayAlert"
    static let SupplierTitle = "SupplierTitle"
    static let BuyerTitle = "BuyerTitle"
    static let DriverName = "DriverName"
  }
  
  struct Toast {
    static let noCameraAvailable = "NoCamera"
    static let cameraAccessTitle = "CameraAccessTitle"
    static let cameraAccessMessage = "CameraAccess"
    static let cancel = "Cancel"
    static let enableCameraAccess = "EnableCamera"
  }
  
  
  struct DateFormats {
    static let iso = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
    static let delivery = "dd MMM, yyyy"
    static let deliveryDate = "dd MMM yyyy"  //"yyyy-MM-dd"
    static let orderDateTime = "dd MMM yyyy, hh:mm:ss a"  //23 Mar 2021, 12:09:06 pm //"dd MMM, yyyy hh:mm a"
    static let current = "yyyy-MM-dd"
  }
  
  struct EventStatus {
    static let TMASSIGNED = "TM_ASSIGNED"
    static let PICKUP = "PICKUP"
    static let DELAYED = "DELAYED"
    static let CPASSIGNED = "CP_ASSIGNED"
  }
}
