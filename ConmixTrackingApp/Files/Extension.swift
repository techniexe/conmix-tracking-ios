//
//  Extension.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 08/03/21.
//

import Foundation
import UIKit
import SDWebImage
import CoreLocation

extension UIScreen {

  var width: CGFloat {
    return UIScreen.main.bounds.size.width
  }
  var height: CGFloat {
    return UIScreen.main.bounds.size.height
  }
}

public extension UIWindow {
  
  /// Transition Options
   struct TransitionOptions {
    
    /// Curve of animation
    ///
    /// - linear: linear
    /// - easeIn: ease in
    /// - easeOut: ease out
    /// - easeInOut: ease in - ease out
    public enum Curve {
      case linear
      case easeIn
      case easeOut
      case easeInOut
      
      /// Return the media timing function associated with curve
      internal var function: CAMediaTimingFunction {
        let key: String!
        switch self {
        case .linear:    key = CAMediaTimingFunctionName.linear.rawValue
        case .easeIn:    key = CAMediaTimingFunctionName.easeIn.rawValue
        case .easeOut:    key = CAMediaTimingFunctionName.easeOut.rawValue
        case .easeInOut:  key = CAMediaTimingFunctionName.easeInEaseOut.rawValue
        }
        return CAMediaTimingFunction(name: CAMediaTimingFunctionName(rawValue: key))
      }
    }
    
    /// Direction of the animation
    ///
    /// - fade: fade to new controller
    /// - toTop: slide from bottom to top
    /// - toBottom: slide from top to bottom
    /// - toLeft: pop to left
    /// - toRight: push to right
    public enum Direction {
      case fade
      case toTop
      case toBottom
      case toLeft
      case toRight
      
      /// Return the associated transition
      ///
      /// - Returns: transition
      internal func transition() -> CATransition {
        let transition = CATransition()
        transition.type = CATransitionType.push
        switch self {
        case .fade:
          transition.type = CATransitionType.fade
          transition.subtype = nil
        case .toLeft:
          transition.subtype = CATransitionSubtype.fromLeft
        case .toRight:
          transition.subtype = CATransitionSubtype.fromRight
        case .toTop:
          transition.subtype = CATransitionSubtype.fromTop
        case .toBottom:
          transition.subtype = CATransitionSubtype.fromBottom
        }
        return transition
      }
    }
    
    /// Background of the transition
    ///
    /// - solidColor: solid color
    /// - customView: custom view
    public enum Background {
      case solidColor(_: UIColor)
      case customView(_: UIView)
    }
    
    /// Duration of the animation (default is 0.20s)
    public var duration: TimeInterval = 0.20
    
    /// Direction of the transition (default is `toRight`)
    public var direction: TransitionOptions.Direction = .toRight
    
    /// Style of the transition (default is `linear`)
    public var style: TransitionOptions.Curve = .linear
    
    /// Background of the transition (default is `nil`)
    public var background: TransitionOptions.Background? = nil
    
    /// Initialize a new options object with given direction and curve
    ///
    /// - Parameters:
    ///   - direction: direction
    ///   - style: style
    public init(direction: TransitionOptions.Direction = .toRight, style: TransitionOptions.Curve = .linear) {
      self.direction = direction
      self.style = style
    }
    
    public init() { }
    
    /// Return the animation to perform for given options object
    internal var animation: CATransition {
      let transition = self.direction.transition()
      transition.duration = self.duration
      transition.timingFunction = self.style.function
      return transition
    }
  }
  
  
  /// Change the root view controller of the window
  ///
  /// - Parameters:
  ///   - controller: controller to set
  ///   - options: options of the transition
  func setRootViewController(_ controller: UIViewController, options: TransitionOptions = TransitionOptions()) {
    
    var transitionWnd: UIWindow? = nil
    
    if let background = options.background {
      transitionWnd = UIWindow(frame: UIScreen.main.bounds)
      switch background {
      case .customView(let view):
        transitionWnd?.rootViewController = UIViewController.newController(withView: view, frame: transitionWnd!.bounds)
      case .solidColor(let color):
        transitionWnd?.backgroundColor = color
      }
      transitionWnd?.makeKeyAndVisible()
    }
    
    // Make animation
    self.layer.add(options.animation, forKey: kCATransition)
    self.rootViewController = controller
    self.makeKeyAndVisible()
    
    if let wnd = transitionWnd {
      DispatchQueue.main.asyncAfter(deadline: (.now() + 1 + options.duration), execute: {
        wnd.removeFromSuperview()
      })
    }
  }
}

internal extension UIViewController {
  
  /// Create a new empty controller instance with given view
  ///
  /// - Parameters:
  ///   - view: view
  ///   - frame: frame
  /// - Returns: instance
  static func newController(withView view: UIView, frame: CGRect) -> UIViewController {
    view.frame = frame
    let controller = UIViewController()
    controller.view = view
    return controller
  }
  
}


extension UIViewController {
  
  @objc func popViewController() {
    self.navigationController?.popViewController(animated: true)
  }
  
  @objc func dismissViewController() {
    self.dismiss(animated: true, completion: nil)
  }
  
  func multiply(lhs: Int, rhs: Double) -> Double {
      return Double(lhs) * rhs
  }
  
  func enableUserInteraction() {
    self.view.isUserInteractionEnabled = true
  }
  
  func disableUserInteraction() {
    self.view.isUserInteractionEnabled = false
  }
}

extension String {
 
  func removingWhitespaces() -> String {
    return components(separatedBy: .whitespaces).joined()
  }
  
  var dateFromISO8601: Date? {
      return Formatter.iso8601.date(from: self)
  }
  
  func trim()->String{
      return self.trimmingCharacters(in: .whitespacesAndNewlines)
  }

  public var initials: String {
      
      let words = components(separatedBy: .whitespacesAndNewlines)
      
      //to identify letters
      let letters = CharacterSet.letters
      var firstChar : String = ""
      var secondChar : String = ""
      var firstCharFoundIndex : Int = -1
      var firstCharFound : Bool = false
      var secondCharFound : Bool = false
      
      for (index, item) in words.enumerated() {
          
          if item.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
              continue
          }
          
          //browse through the rest of the word
          for (_, char) in item.unicodeScalars.enumerated() {
              
              //check if its a aplha
              if letters.contains(char) {
                  
                  if !firstCharFound {
                      firstChar = String(char)
                      firstCharFound = true
                      firstCharFoundIndex = index
                      
                  } else if !secondCharFound {
                      
                      secondChar = String(char)
                      if firstCharFoundIndex != index {
                          secondCharFound = true
                      }
                      
                      break
                  } else {
                      break
                  }
              }
          }
      }
      
      if firstChar.isEmpty && secondChar.isEmpty {
          firstChar = "D"
          secondChar = "P"
      }
      
      return firstChar + secondChar
  }
}


extension UIView{
  
  @IBInspectable var Corneredius:CGFloat{
    
    get{
      return layer.cornerRadius
    }
    set{
      self.layer.cornerRadius = newValue
      self.layer.masksToBounds = newValue > 0
    }
  }
  @IBInspectable var TWBorderWidth:CGFloat{
    
    get{
      return layer.borderWidth
    }
    set{
      self.layer.borderWidth = newValue
      self.layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable var TWBorderColor:UIColor{
    
    get{
      return self.TWBorderColor
    }
    set{
      self.layer.borderColor = newValue.cgColor
      
    }
  }
  @IBInspectable var TWRoundDynamic:Bool{
    
    get{
      return false
    }
    set{
      if newValue == true {
        
        self.perform(#selector(UIView.afterDelay), with: nil, afterDelay: 0.1)
      }
      
    }
    
  }
  @objc func afterDelay(){
    
    let  Height =  self.frame.size.height
    self.layer.cornerRadius = Height/2;
    self.layer.masksToBounds = true;
    
    
  }
  @IBInspectable var TWRound:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = true;
      }
      
    }
  }
  @IBInspectable var TWShadow:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0.2;
        
      }
      
    }
    
  }
  @IBInspectable var TWclipsToBounds:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        
        self.clipsToBounds = true;
      }else{
        self.clipsToBounds = false
      }
      
    }
    
  }
  
  
  func shadowWith(alph:Float){
    self.layer.masksToBounds = false
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    self.layer.shadowOpacity = alph
  }
  
  
  func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
    UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
      self.alpha = 1.0
    }, completion: completion)  }
  
  func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 1.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
    UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
      self.alpha = 0.3
    }, completion: completion)
  }
  
  func createDottedLine(x: CGFloat, width: CGFloat, color: CGColor) {
    let caShapeLayer = CAShapeLayer()
    caShapeLayer.strokeColor = color
    caShapeLayer.lineWidth = width
    caShapeLayer.lineDashPattern = [2,3]
    let cgPath = CGMutablePath()
    let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: x , y: 0)]
    cgPath.addLines(between: cgPoint)
    caShapeLayer.path = cgPath
    layer.addSublayer(caShapeLayer)
  }
  
  func roundCorners(corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
    layer.masksToBounds = false
  }
  
  func lock() {
    if let _ = viewWithTag(10) {
      //View is already locked
    }
    else {
      let lockView = UIView(frame: bounds)
      lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
      lockView.tag = 10
      lockView.alpha = 0.0
      let activity = UIActivityIndicatorView(style: .white)
      activity.color = Constants.Colors.appRedColor
      activity.hidesWhenStopped = true
      activity.center = lockView.center
      lockView.addSubview(activity)
      activity.startAnimating()
      addSubview(lockView)
      
      UIView.animate(withDuration: 0.2, animations: {
        lockView.alpha = 1.0
      })
    }
  }
  
  func unlock() {
    if let lockView = viewWithTag(10) {
      UIView.animate(withDuration: 0.2, animations: {
        lockView.alpha = 0.0
      }, completion: { finished in
        lockView.removeFromSuperview()
      })
    }
  }
  
  func fadeOut(_ duration: TimeInterval) {
    UIView.animate(withDuration: duration, animations: {
      self.alpha = 0.0
    })
  }
  
  func fadeIn(_ duration: TimeInterval) {
    UIView.animate(withDuration: duration, animations: {
      self.alpha = 1.0
    })
  }
  
  class func viewFromNibName(_ name: String) -> UIView? {
    let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
    return views?.first as? UIView
  }
  
  
  // MARK: - Config
  /// The default duration for fading-animations, measured in seconds.
  public static let defaultFadingAnimationDuration: TimeInterval = 1.0
  
  // MARK: - Public methods
  /// Updates the view visiblity.
  ///
  /// - Parameters:
  ///   - isHidden: The new view visibility.
  ///   - duration: The duration of the animation, measured in seconds.
  ///   - completion: Closure to be executed when the animation sequence ends. This block has no return value and takes a single Boolean
  ///                 argument that indicates whether or not the animations actually finished before the completion handler was called.
  ///
  /// - SeeAlso: https://developer.apple.com/documentation/uikit/uiview/1622515-animatewithduration
  public func animate(isHidden: Bool, duration: TimeInterval = UIView.defaultFadingAnimationDuration, completion: ((Bool) -> Void)? = nil) {
    if isHidden {
      fadeOut(duration: duration,
              completion: completion)
    } else {
      fadeIn(duration: duration,
             completion: completion)
    }
  }
  
  /// Fade out the current view by animating the `alpha` to zero and update the `isHidden` flag accordingly.
  ///
  /// - Parameters:
  ///   - duration: The duration of the animation, measured in seconds.
  ///   - completion: Closure to be executed when the animation sequence ends. This block has no return value and takes a single Boolean
  ///                 argument that indicates whether or not the animations actually finished before the completion handler was called.
  ///
  /// - SeeAlso: https://developer.apple.com/documentation/uikit/uiview/1622515-animatewithduration
  public func fadeOut(duration: TimeInterval = UIView.defaultFadingAnimationDuration, completion: ((Bool) -> Void)? = nil) {
    UIView.animate(withDuration: duration,
                   animations: {
                    self.alpha = 0.0
                   },
                   completion: { isFinished in
                    // Update `isHidden` flag accordingly:
                    //  - set to `true` in case animation was completely finished.
                    //  - set to `false` in case animation was interrupted, e.g. due to starting of another animation.
                    self.isHidden = isFinished
                    
                    completion?(isFinished)
                   })
  }
  
  /// Fade in the current view by setting the `isHidden` flag to `false` and animating the `alpha` to one.
  ///
  /// - Parameters:
  ///   - duration: The duration of the animation, measured in seconds.
  ///   - completion: Closure to be executed when the animation sequence ends. This block has no return value and takes a single Boolean
  ///                 argument that indicates whether or not the animations actually finished before the completion handler was called.
  ///
  /// - SeeAlso: https://developer.apple.com/documentation/uikit/uiview/1622515-animatewithduration
  public func fadeIn(duration: TimeInterval = UIView.defaultFadingAnimationDuration, completion: ((Bool) -> Void)? = nil) {
    if isHidden {
      // Make sure our animation is visible.
      isHidden = false
    }
    
    UIView.animate(withDuration: duration,
                   animations: {
                    self.alpha = 1.0
                   },
                   completion: completion)
  }
  
  
  func addCornerRadiusWithShadow(color: UIColor, borderColor: UIColor, cornerRadius: CGFloat) {
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOffset = CGSize(width: 0, height: 3)
    self.layer.shadowOpacity = 1.0
    self.layer.shadowRadius = 6.0
    self.layer.cornerRadius = cornerRadius
    self.layer.borderColor = borderColor.cgColor
    self.layer.borderWidth = 1.0
    self.layer.masksToBounds = true
  }
  
  func setCornerRadiusWith(radius: Float, borderWidth: Float, borderColor: UIColor) {
    self.layer.cornerRadius = CGFloat(radius)
    self.layer.borderWidth = CGFloat(borderWidth)
    self.layer.borderColor = borderColor.cgColor
    self.layer.masksToBounds = true
  }
}

extension Formatter {
  
  static let iso8601: DateFormatter = {
    let formatter = DateFormatter()
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
    return formatter
  }()
}

extension AppCodable {
  func getJSONData() -> Any? {
    
    guard let bodyData = try? JSONEncoder().encode(self) else {
      return nil
    }
    
    return try? JSONSerialization.jsonObject(with: bodyData, options: [.allowFragments])
  }
}



extension UIImageView {
    func setImage(url: String,
                  style: ImageStyle = .squared,
                  completion: ((_ result:Bool,_ error:Error?)->Void)?=nil) {
        
        image = nil
        
        if url.count < 1 {
            return
        }
        
        if(style == .rounded) {
            layer.cornerRadius = frame.height/2
          backgroundColor = Constants.Colors.lightGrayColor
            //backgroundColor = UIColor.rgb(from: 0xEDF0F1)
        }else if(style == .squared){
            layer.cornerRadius = 0.0
            backgroundColor =  Constants.Colors.lightGrayColor
            //backgroundColor = UIColor.clear
        }
        
        sd_imageIndicator = SDWebImageActivityIndicator.gray
    
        self.sd_setImage(with: URL.init(string: url), placeholderImage:nil, options: [.avoidAutoSetImage,.highPriority,.retryFailed,.delayPlaceholder,.continueInBackground], completed: { (image, error, cacheType, url) in
            if error == nil {
                DispatchQueue.main.async {
                    /*self.backgroundColor = .clear*/
                    //self.alpha = 0;
                   
//                    if  (image?.size.width)! > (image?.size.height)! {
//                        self.contentMode = UIView.ContentMode.scaleAspectFit
//                    }else {
//                         self.contentMode = UIView.ContentMode.scaleAspectFill
//                    }
                   
                    self.image = image
                    self.clipsToBounds = true
                    UIView.animate(withDuration: 0.5, animations: {
                        self.alpha = 1
                    }, completion: { (done) in
                        if let completion = completion {
                            completion(true,error)
                        }
                    })
                }
            }else {
                if let completion = completion {
                    completion(false,error)
                }
            }
        })
    }
  
  /// Sets the image property of the view based on initial text, a specified background color, custom text attributes, and a circular clipping
  ///
  /// - Parameters:
  ///   - string: The string used to generate the initials. This should be a user's full name if available.
  ///   - color: This optional paramter sets the background of the image. By default, a random color will be generated.
  ///   - circular: This boolean will determine if the image view will be clipped to a circular shape.
  ///   - textAttributes: This dictionary allows you to specify font, text color, shadow properties, etc.
  open func settingImage(string: String?,
                     color: UIColor? = nil,
                     circular: Bool = false,
                     stroke: Bool = false,
                     textAttributes: [NSAttributedString.Key: Any]? = nil) {
      
      let image = imageSnap(text: string != nil ? string?.initials : "",
                            color: color ?? .random,
                            circular: circular,
                            stroke: stroke,
                            textAttributes:textAttributes)
      
      if let newImage = image {
          self.image = newImage
      }
  }
  
  private func imageSnap(text: String?,
                         color: UIColor,
                         circular: Bool,
                         stroke: Bool,
                         textAttributes: [NSAttributedString.Key: Any]?) -> UIImage? {
      
      let scale = Float(UIScreen.main.scale)
      var size = bounds.size
      if contentMode == .scaleToFill || contentMode == .scaleAspectFill || contentMode == .scaleAspectFit || contentMode == .redraw {
          size.width = CGFloat(floorf((Float(size.width) * scale) / scale))
          size.height = CGFloat(floorf((Float(size.height) * scale) / scale))
      }
      
      UIGraphicsBeginImageContextWithOptions(size, false, CGFloat(scale))
      let context = UIGraphicsGetCurrentContext()
      if circular {
          let path = CGPath(ellipseIn: bounds, transform: nil)
          context?.addPath(path)
          context?.clip()
      }
      
      // Fill
      
      context?.setFillColor(color.cgColor)
      context?.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
      
    let attributes = textAttributes ?? [NSAttributedString.Key.foregroundColor: UIColor.white,
                                        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 55.0)]

      
      //stroke color
      if stroke {
          
          //outer circle
        context?.setStrokeColor((attributes[NSAttributedString.Key.foregroundColor] as! UIColor).cgColor)
          context?.setLineWidth(4)
          var rectangle : CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
          context?.addEllipse(in: rectangle)
          context?.drawPath(using: .fillStroke)
          
          //inner circle
          context?.setLineWidth(1)
          rectangle = CGRect(x: 4, y: 4, width: size.width - 8, height: size.height - 8)
          context?.addEllipse(in: rectangle)
          context?.drawPath(using: .fillStroke)
      }
      
      // Text
      if let text = text {
          let textSize = text.size(withAttributes: attributes)
          let bounds = self.bounds
          let rect = CGRect(x: bounds.size.width/2 - textSize.width/2, y: bounds.size.height/2 - textSize.height/2, width: textSize.width, height: textSize.height)
          
          text.draw(in: rect, withAttributes: attributes)
      }
      
      let image = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
      return image
  }
  
}

extension UIAlertController {
    override open func viewDidLoad() {
        super.viewDidLoad()
        pruneNegativeWidthConstraints()
    }

    func pruneNegativeWidthConstraints() {
        for subView in self.view.subviews {
            for constraint in subView.constraints where constraint.debugDescription.contains("width == - 16") {
                subView.removeConstraint(constraint)
            }
        }
    }
}

extension Array {
    func sortedArrayByPosition() -> [Element] {
        return sorted(by: { (obj1 : Element, obj2 : Element) -> Bool in
            
            let view1 = obj1 as! UIView
            let view2 = obj2 as! UIView
            
            let x1 = view1.frame.minX
            let y1 = view1.frame.minY
            let x2 = view2.frame.minX
            let y2 = view2.frame.minY
            
            if y1 != y2 {
                return y1 < y2
            } else {
                return x1 < x2
            }
        })
    }
    
    /// - parameter indexes: Get elements from these indexes
    /// - returns: New array with elements from the indexes specified.
    func at(indexes: Int...) -> [Element] {
        return Dollar.at(self, indexes: indexes)
    }
    
    /// Cycles through the array n times passing each element into the callback function
    ///
    /// - parameter times: Number of times to cycle through the array
    /// - parameter callback: function to call with the element
    func cycle<U>(times: Int, callback: (Element) -> U) {
        Dollar.cycle(self, times, callback: callback)
    }
    
    /// Cycles through the array indefinetly passing each element into the callback function
    ///
    /// - parameter callback: function to call with the element
    func cycle<U>(callback: (Element) -> U) {
        Dollar.cycle(self, callback: callback)
    }
    
    /// For each item in the array invoke the callback by passing the elem
    ///
    /// - parameter callback: The callback function to invoke that take an element
    /// - returns: array itself
    @discardableResult func eachWithIndex(callback: (Int, Element) -> ()) -> [Element] {
        for (index, elem) in self.enumerated() {
            callback(index, elem)
        }
        return self
    }
    
    /// For each item in the array invoke the callback by passing the elem along with the index
    ///
    /// - parameter callback: The callback function to invoke
    /// - returns: The array itself
    @discardableResult func each(callback: (Element) -> ()) -> [Element] {
        self.eachWithIndex { (index, elem) -> () in
            callback(elem)
        }
        return self
    }
    
    /// For each item in the array that meets the when conditon, invoke the callback by passing the elem
    ///
    /// - parameter when: The condition to check each element against
    /// - parameter callback: The callback function to invoke
    /// - returns: The array itself
    @discardableResult func each(when: (Element) -> Bool, callback: (Element) -> ()) -> [Element] {
        return Dollar.each(self, when: when, callback: callback)
    }
    
    /// Checks if the given callback returns true value for all items in the array.
    ///
    /// - parameter callback: Check whether element value is true or false.
    /// - returns: First element from the array.
    func every(callback: (Element) -> Bool) -> Bool {
        return Dollar.every(self, callback: callback)
    }
    
    /// Get element from an array at the given index which can be negative
    /// to find elements from the end of the array
    ///
    /// - parameter index: Can be positive or negative to find from end of the array
    /// - parameter orElse: Default value to use if index is out of bounds
    /// - returns: Element fetched from the array or the default value passed in orElse
    func fetch(index: Int, orElse: Element? = .none) -> Element! {
        return Dollar.fetch(self, index, orElse: orElse)
    }
    
    /// This method is like find except that it returns the index of the first element
    /// that passes the callback check.
    ///
    /// - parameter callback: Function used to figure out whether element is the same.
    /// - returns: First element's index from the array found using the callback.
    func findIndex(callback: (Element) -> Bool) -> Int? {
        return Dollar.findIndex(self, callback: callback)
    }
    
    /// This method is like findIndex except that it iterates over elements of the array
    /// from right to left.
    ///
    /// - parameter callback: Function used to figure out whether element is the same.
    /// - returns: Last element's index from the array found using the callback.
    func findLastIndex(callback: (Element) -> Bool) -> Int? {
        return Dollar.findLastIndex(self, callback: callback)
    }
    
    /// Gets the first element in the array.
    ///
    /// - returns: First element from the array.
    func first() -> Element? {
        return Dollar.first(self)
    }
    
    /// Flattens the array
    ///
    /// - returns: Flatten array of elements
    func flatten() -> [Element] {
        return Dollar.flatten(self)
    }
    
    /// Get element at index
    ///
    /// - parameter index: The index in the array
    /// - returns: Element at that index
    func get(index: Int) -> Element! {
        return self.fetch(index: index)
    }
    
    /// Gets all but the last element or last n elements of an array.
    ///
    /// - parameter numElements: The number of elements to ignore in the end.
    /// - returns: Array of initial values.
    func initial(numElements: Int? = 1) -> [Element] {
        return Dollar.initial(self, numElements: numElements!)
    }
    
    /// Gets the last element from the array.
    ///
    /// - returns: Last element from the array.
    func last() -> Element? {
        return Dollar.last(self)
    }
    
    /// The opposite of initial this method gets all but the first element or first n elements of an array.
    ///
    /// - parameter numElements: The number of elements to exclude from the beginning.
    /// - returns: The rest of the elements.
    func rest(numElements: Int? = 1) -> [Element] {
        return Dollar.rest(self, numElements: numElements!)
    }
    
    /// Retrieves the minimum value in an array.
    ///
    /// - returns: Minimum value from array.
    func min<T: Comparable>() -> T? {
        return Dollar.min(map { $0 as! T })
    }
    
    /// Retrieves the maximum value in an array.
    ///
    /// - returns: Maximum element in array.
    func max<T: Comparable>() -> T? {
        return Dollar.max(map { $0 as! T })
    }
    
    /// Gets the index at which the first occurrence of value is found.
    ///
    /// - parameter value: Value whose index needs to be found.
    /// - returns: Index of the element otherwise returns nil if not found.
    func indexOf<T: Equatable>(value: T) -> Int? {
        return Dollar.indexOf(map { $0 as! T }, value: value)
    }
    
    /// Remove element from array
    ///
    /// - parameter value: Value that is to be removed from array
    /// - returns: Element at that index
    mutating func remove<T: Equatable>(value: T) -> T? {
        if let index = Dollar.indexOf(map { $0 as! T }, value: value) {
            return (remove(at: index) as? T)
        } else {
            return .none
        }
    }
    
    /// Checks if a given value is present in the array.
    ///
    /// - parameter value: The value to check.
    /// - returns: Whether value is in the array.
    func contains<T: Equatable>(value: T) -> Bool {
        return Dollar.contains(map { $0 as! T }, value: value)
    }
    
    /// Return the result of repeatedly calling `combine` with an accumulated value initialized
    /// to `initial` on each element of `self`, in turn with a corresponding index.
    ///
    /// - parameter initial: the value to be accumulated
    /// - parameter combine: the combiner with the result, index, and current element
    /// - throws: Rethrows the error generated from combine
    /// - returns: combined result
    func reduceWithIndex<T>(initial: T, combine: (T, Int, Array.Iterator.Element) throws -> T) rethrows -> T {
        var result = initial
        for (index, element) in self.enumerated() {
            result = try combine(result, index, element)
        }
        return result
    }
    
    /// Checks if the array has one or more elements.
    ///
    /// - returns: true if the array is not empty, or false if it is empty.
    public var isNotEmpty: Bool {
        get {
            return !self.isEmpty
        }
    }
    
    /// Move item in array from old index to new index
    ///
    /// - parameter oldIndex: index of item to be moved
    /// - parameter newIndex: index to move item to
    mutating func moveElement(at oldIndex: Int, to newIndex: Int) {
        let element = self[newIndex]
        self[newIndex] = self[oldIndex]
        self[oldIndex] = element
    }
}

extension Array where Element: Hashable {
    
    /// Creates an object composed from arrays of keys and values.
    ///
    /// - parameter values: The array of values.
    /// - returns: Dictionary based on the keys and values passed in order.
    public func zipObject<T>(values: [T]) -> [Element:T] {
        return Dollar.zipObject(self, values: values)
    }
}

/// Overloaded operator to appends another array to an array
///
/// - parameter left: array to insert elements into
/// - parameter right: array to source elements from
/// - returns: array with the element appended in the end
public func<<<T>(left: inout [T], right: [T]) -> [T] {
    left += right
    return left
}

/// Overloaded operator to append element to an array
///
/// - parameter array: array to insert element into
/// - parameter elem: element to insert
/// - returns: array with the element appended in the end
public func<<<T>( array: inout [T], elem: T) -> [T] {
    array.append(elem)
    return array
}

/// Overloaded operator to remove elements from first array
///
/// - parameter left: array from which elements will be removed
/// - parameter right: array containing elements to remove
/// - returns: array with the elements from second array removed
public func -<T: Hashable>(left: [T], right: [T]) -> [T] {
    return Dollar.difference(left, right)}

//MARK:- UIColor Helper
extension UIColor {
  
  /// Returns random generated color.
  public static var random: UIColor {
    srandom(arc4random())
    var red: Double = 0
    
    while (red < 0.1 || red > 0.84) {
      red = drand48()
    }
    
    var green: Double = 0
    while (green < 0.1 || green > 0.84) {
      green = drand48()
    }
    
    var blue: Double = 0
    while (blue < 0.1 || blue > 0.84) {
      blue = drand48()
    }
    
    return .init(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1.0)
  }
  
  public static func colorHash(name: String?) -> UIColor {
    guard let name = name else {
      return .red
    }
    
    var nameValue = 0
    for character in name {
      let characterString = String(character)
      let scalars = characterString.unicodeScalars
      nameValue += Int(scalars[scalars.startIndex].value)
    }
    
    var r = Float((nameValue * 123) % 51) / 51
    var g = Float((nameValue * 321) % 73) / 73
    var b = Float((nameValue * 213) % 91) / 91
    
    let defaultValue: Float = 0.84
    r = min(max(r, 0.1), defaultValue)
    g = min(max(g, 0.1), defaultValue)
    b = min(max(b, 0.1), defaultValue)
    
    return .init(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
  }
  
  // MARK: - Initialization
  convenience init(hexString: String) {
      
      let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
      var int = UInt32()
      Scanner(string: hex).scanHexInt32(&int)
      let a, r, g, b: UInt32
      switch hex.count {
          
      case 3: // RGB (12-bit)
          (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
      case 6: // RGB (24-bit)
          (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
      case 8: // ARGB (32-bit)
          (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
      default:
          (a, r, g, b) = (255, 0, 0, 0)
      }
      self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
  }
  // MARK: - Computed Properties
  
  var toHex: String? {
      return toHex()
  }
  
  // MARK: - From UIColor to String
  
  func toHex(alpha: Bool = false) -> String? {
      guard let components = cgColor.components, components.count >= 3 else {
          return nil
      }
      
      let r = Float(components[0])
      let g = Float(components[1])
      let b = Float(components[2])
      var a = Float(1.0)
      
      if components.count >= 4 {
          a = Float(components[3])
      }
      
      if alpha {
          return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
      } else {
          return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
      }
  }
  
  class func rgb(from hex: Int,alpha :CGFloat=1.0) -> UIColor {
      let red = CGFloat((hex & 0xFF0000) >> 16) / 0xFF
      let green = CGFloat((hex & 0x00FF00) >> 8) / 0xFF
      let blue = CGFloat(hex & 0x0000FF) / 0xFF
      return UIColor(red: red, green: green, blue: blue, alpha: alpha)
  }
}

extension UILabel {
  
  func setText(_ name: String) {
    self.text = name
  }
  
  func setOrderID(_ strDisplayId: String) {
    self.text = String(format: "Order ID : #%@",strDisplayId)
  }
  
  func setOrderDate(_ strDate: String) {
    self.text = String(format: "Order Date : %@", strDate)
  }
  
  func setPhoneNumber(_ strPhone: String) {
    self.text = String(format: "Phone No : %@", strPhone)
  }
  
  func setDistance(_ calculatedDistance: NSNumber) {

    let double = Double(exactly: calculatedDistance)
    
    self.text = String(format: "%.2f km", double!)
    
  }
  
  func setAvailability(_ strQty: String, strUnit: String, fontsize: CGFloat = 12.0) {
  
    var strStock: String = ""
    var color: UIColor!
    var strTitle: NSString!
   
    if strQty == "0" {
      strStock = "Out of stock"
      color = UIColor.lightGray
      strTitle = "Availability : \(strStock) " as NSString
    }else {
      strStock = "In stock"
      color = UIColor.init(hexString: "#259221")
      strTitle = "Availability : \(strStock) (\(strQty) \(strUnit))" as NSString
    }
    
    
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "Availability : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.bold.with(size: fontsize),NSAttributedString.Key.foregroundColor: color!], range: strTitle.range(of: "\(strStock) "))
    
    if strStock == "In stock"{
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "(\(strQty) \(strUnit))"))
    }
    
    self.attributedText = strMutableAttributed
    
  }
  
  func setSalePrice(_ sellingprice: Double, fontsize: CGFloat = 12.0) {
    
    let price = Helper.getStringFromNumberFormatter(NSNumber(value: sellingprice))
    
    var strTitle: NSString!
    
    strTitle = "Price : \(price) / Cu.Mtr (Excl. GST)" as NSString
    

    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "Price : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.medium.with(size: fontsize),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(price) / Cu.Mtr "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize-4.0),NSAttributedString.Key.foregroundColor: Constants.Colors.appRedColor ?? UIColor.black], range: strTitle.range(of: "(Excl. GST)"))
    
    self.attributedText = strMutableAttributed
    
  }
  
  
  func setUnitPrice(_ unitprice: Double, fontsize: CGFloat = 12.0) {
    
    let price = Helper.getStringFromNumberFormatter(NSNumber(value: unitprice))
    
    var strTitle: NSString!
    
    strTitle = "Unit Price : \(price)" as NSString
    
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "Unit Price : \(price)"))
    
    self.attributedText = strMutableAttributed
    
  }
  
  func setTMPrice(tmPrice: Double, fontsize: CGFloat = 14.0) {
    
    let price = Helper.getStringFromNumberFormatter(NSNumber(value: tmPrice))
    
    var strTitle: NSString!
    
    strTitle = "With Transit Mixer : \(price) Per Cu.Mtr / Km" as NSString //₹ 54.92 Per Cu. mtr / Km
    
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.bold.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "With Transit Mixer : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(price) Per Cu.Mtr / Km"))
  
    self.attributedText = strMutableAttributed
  }
  
  func setCPPrice(tmPrice: Double, fontsize: CGFloat = 14.0) {
    
    let price = Helper.getStringFromNumberFormatter(NSNumber(value: tmPrice))
    
    var strTitle: NSString!
    
    strTitle = "With Conceret Pump : \(price) Per day" as NSString
    
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.bold.with(size: fontsize), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "With Conceret Pump : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(price) Per day"))
  
    self.attributedText = strMutableAttributed
  }
  
  func setTotalPrice(_ quantity: Int, unitPrice: Double) {
    
    var strTotalPrice: NSString = ""
    
    var totalPrice: Double = 0.0
    
    totalPrice = Double(quantity) * unitPrice
    
    var items: String = ""
    
    if quantity == 1 {
      
      items = "Item"
      strTotalPrice = "\(quantity) \(items)\nTotal Price : \(Helper.getStringFromDoubleWithNumberFormatter(totalPrice))\nInc.of all taxes" as NSString
      
    }else {
      items = "Items"
      strTotalPrice = "\(quantity) \(items)\nTotal Price : \(Helper.getStringFromDoubleWithNumberFormatter(totalPrice))\nInc.of all taxes" as NSString
    }
  
    let strMutableAttributed =  NSMutableAttributedString(string:strTotalPrice as String)

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.medium.with(size: 10.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTotalPrice.range(of: "\(quantity) \(items)\n"))

    strMutableAttributed.addAttributes([NSAttributedString.Key.font:MainFont.medium.with(size: 15.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTotalPrice.range(of: "Total Price : \(Helper.getStringFromDoubleWithNumberFormatter(totalPrice))\n"))

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: 10.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTotalPrice.range(of: "Inc.of all taxes"))

    self.attributedText = strMutableAttributed
    
  }
  
  
  func setAvailableDate(_ strText: String, strColorcode: String, date: String, fontsize: CGFloat = 12.0) {
    
    let textColor: UIColor = UIColor.init(hexString: strColorcode)
    
//    if withTM == true && withCP == true {
//
//      strAvailable = "With TM & CP Available Date"
//
//      textColor = UIColor.init(hexString: "#3A87AD")
//
//    }else if withTM == true && withCP == false {
//
//      strAvailable = "With TM Available Date"
//
//      textColor = UIColor.init(hexString: "#3A87AD")
//
//    }else if withTM == false && withCP == true {
//
//      strAvailable = "With CP Available Date"
//
//      textColor = UIColor.init(hexString: "#3A87AD")
//
//    }else{
//
//      strAvailable = "Without TM & CP Available Date"
//
//      textColor = UIColor.init(hexString: "#B94A48")
//    }
    
    
    let objDate = Date.convertStringToDate(strDate: date, dateFormate: Constants.DateFormats.iso)
    
    let strDate = objDate.convertDateToString(strDateFormate: Constants.DateFormats.deliveryDate)
    
   
    var strTitle: NSString!
    
    strTitle = "\(strText) : \(strDate)" as NSString
    
    
    
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.medium.with(size: fontsize), NSAttributedString.Key.foregroundColor: textColor], range: strTitle.range(of: "\(strText) : "))

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor: textColor], range: strTitle.range(of: "\(strDate)"))
    
    self.attributedText = strMutableAttributed
    
  }
  
  func termAndConditionTextColorChange (fullText : String , changeText : String ) {
    let strNumber: NSString = fullText as NSString
    let range = (strNumber).range(of: changeText)
    let attribute = NSMutableAttributedString.init(string: fullText)
    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: Constants.Colors.appRedColor ?? UIColor.red , range: range)
    self.attributedText = attribute
  }

  func setBuyerTMDetail(_ title: String, description: String, fontsize: CGFloat = 14.0) {
    
    var strTitle: NSString = ""
    
    strTitle = "\(title) : \(description)" as NSString
  
    let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.regular.with(size: fontsize), NSAttributedString.Key.foregroundColor:  Constants.Colors.appThemeColor ?? UIColor.black ], range: strTitle.range(of: "\(title) : "))

    strMutableAttributed.addAttributes([NSAttributedString.Key.font: MainFont.medium.with(size: fontsize), NSAttributedString.Key.foregroundColor:  Constants.Colors.appThemeColor ?? UIColor.black ], range: strTitle.range(of: "\(description)"))

    self.attributedText = strMutableAttributed
    
  }
  
}

internal extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }

    convenience init(hexValue: Int, a: CGFloat = 1.0) {
        self.init(
            red: (hexValue >> 16) & 0xFF,
            green: (hexValue >> 8) & 0xFF,
            blue: hexValue & 0xFF,
            a: a
        )
    }

    static func adaptive(dark: UIColor, light: UIColor) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor { $0.userInterfaceStyle == .dark ? dark : light }
        } else {
            return light
        }
    }
}

extension UITextField {
  
  func setPlaceholderText(_ strText: String) {

    let attributedPlaceholder = NSAttributedString(string: strText, attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "#7E7E7F") ?? UIColor.black , NSAttributedString.Key.font: MainFont.regular.with(size: 16.0)])
    self.attributedPlaceholder = attributedPlaceholder
    
  }
  
}


extension Date {
  
  //convert string to date
  static func convertStringToDate(strDate:String, dateFormate strFormate:String) -> Date{
    let dateFormate = DateFormatter()
    dateFormate.dateFormat = strFormate
    dateFormate.timeZone = TimeZone.init(abbreviation: "UTC")
    
    if let dateResult:Date = dateFormate.date(from: strDate) {
      return dateResult
    }
    
    dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    return dateFormate.date(from: strDate)!
  }
  
  //Function for old date format to new format from UTC to local
  static func convertDateUTCToLocal(strDate:String, oldFormate strOldFormate:String, newFormate strNewFormate:String) -> String{
    let dateFormatterUTC:DateFormatter = DateFormatter()
    dateFormatterUTC.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?//set UTC timeZone
    dateFormatterUTC.dateFormat = strOldFormate //set old Format
    if let oldDate:Date = dateFormatterUTC.date(from: strDate)  as Date?//convert date from input string
    {
      dateFormatterUTC.timeZone = NSTimeZone.local//set localtimeZone
      dateFormatterUTC.dateFormat = strNewFormate //make new dateformatter for output format
      dateFormatterUTC.amSymbol = "AM"
      dateFormatterUTC.pmSymbol = "PM"
      if let strNewDate:String = dateFormatterUTC.string(from: oldDate as Date) as String?//convert dateInUTC into string and set into output
      {
        return strNewDate
      }
      return strDate
    }
    return strDate
  }
  
  //Convert without UTC to local
  static func convertDateToLocal(strDate:String, oldFormate strOldFormate:String, newFormate strNewFormate:String) -> String{
    let dateFormatterUTC:DateFormatter = DateFormatter()
    //set local timeZone
    dateFormatterUTC.dateFormat = strOldFormate //set old Format
    if let oldDate:Date = dateFormatterUTC.date(from: strDate) as Date?//convert date from input string
    {
      dateFormatterUTC.timeZone = NSTimeZone.local
      dateFormatterUTC.dateFormat = strNewFormate //make new dateformatter for output format
      dateFormatterUTC.amSymbol = "AM"
      dateFormatterUTC.pmSymbol = "PM"
      if let strNewDate = dateFormatterUTC.string(from: oldDate as Date) as String?//convert dateInUTC into string and set into output
      {
        return strNewDate
      }
      return strDate
    }
    return strDate
  }
  
  //Convert Date to String
  func convertDateToString(strDateFormate:String) -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = strDateFormate
    let strDate = dateFormatter.string(from: self)
    //      dateFormatter = nil
    return strDate
  }
  
  
  //Convert local to utc
  static func convertLocalToUTC(strDate:String, oldFormate strOldFormate:String, newFormate strNewFormate:String) -> String{
    let dateFormatterUTC:DateFormatter = DateFormatter()
    dateFormatterUTC.timeZone = NSTimeZone.local as TimeZone?//set UTC timeZone
    dateFormatterUTC.dateFormat = strOldFormate //set old Format
    if let oldDate:Date = dateFormatterUTC.date(from: strDate)  as Date?//convert date from input string
    {
      dateFormatterUTC.timeZone = NSTimeZone.init(abbreviation: "UTC")! as TimeZone//set localtimeZone
      dateFormatterUTC.dateFormat = strNewFormate //make new dateformatter for output format
      dateFormatterUTC.amSymbol = "AM"
      dateFormatterUTC.pmSymbol = "PM"
      if let strNewDate:String = dateFormatterUTC.string(from: oldDate as Date) as String?//convert dateInUTC into string and set into output
      {
        return strNewDate
      }
      return strDate
    }
    return strDate
  }
  
  //Comparison two date
  static func compare(date:Date, compareDate:Date) -> String{
    var strDateMessage:String = ""
    let result:ComparisonResult = date.compare(compareDate)
    switch result {
    case .orderedAscending:
      strDateMessage = "Future Date"
      break
    case .orderedDescending:
      strDateMessage = "Past Date"
      break
    case .orderedSame:
      strDateMessage = "Same Date"
      break
      
    }
    return strDateMessage
  }
 
  
  static func getElapsedInterval(_ aSourceDate: Date) -> String {
    
    let year = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date())
    
    if year.year! > 0 {
      return year.year! == 1 ? "\(year.year!)" + " " + "year ago" :
        "\(year.year!)" + " " + "years ago"
    }
    
    let aMonth = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).month
    if aMonth! > 0 {
      return aMonth == 1 ? "\(aMonth!)" + " " + "month ago" :
        "\(aMonth!)" + " " + "months ago"
    }
    
    let aDay = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).day
    if aDay! > 0 {
     
      return aDay! == 1 ? "Yesterday" :
            "\(aDay!)" + " " + "days ago"
    }
    
    let aHour = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).hour
    if (aHour != nil) {
      
      if aHour! > 0 {
        return aHour == 1 ? "\(aHour!)" + " " + "hour ago" :
          "\(aHour!)" + " " + "hours ago"
      }
    }
    
    let aMinute = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute], from: aSourceDate, to: Date()).minute
    
    if (aMinute != nil){
      if aMinute! > 0 {
        return aMinute == 1 ? "\(aMinute!)" + " " + "minute ago" :
          "\(aMinute!)" + " " + "minutes ago"
      }
    }
    
    let aSecond = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute,.second], from: aSourceDate, to: Date()).second
    
    if (aSecond != nil){
      
      if aSecond! > 0 {
        
        return aSecond == 1 ? "\(aSecond!)" + " " + "second ago" :
          "\(aSecond!)" + " " + "seconds ago"
        
      } else {
        
        return "a moment ago"
      }
      
    }
    
    return "a moment ago"
  }
  
  static func getCurrentTimeStamp() -> Float {
    return Float(NSTimeIntervalSince1970 * 1000)
  }
  
  var iso8601: String {
    return Formatter.iso8601.string(from: self)
  }
  
  func dayOfWeek() -> String? {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "EEEE"
    return dateFormatter.string(from: self).capitalized

  }
}


extension Encodable {
  func asDictionary() throws -> [String: Any] {
    let data = try JSONEncoder().encode(self)
    guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
      throw NSError()
    }
    return dictionary
  }

  
  
}


public extension Collection {
  /// Convert self to JSON String.
  /// Returns: the pretty printed JSON string or an empty string if any error occur.
  func json() -> String {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
      return String(data: jsonData, encoding: .utf8) ?? "{}"
    } catch {
      print("json serialization error: \(error)")
      return "{}"
    }
  }
}


extension UserDefaults {
  
  func set(location:CLLocation, forKey key: String){
    let locationLat = NSNumber(value:location.coordinate.latitude)
    let locationLon = NSNumber(value:location.coordinate.longitude)
    self.set(["lat": locationLat, "lon": locationLon], forKey:key)
  }
  
  func location(forKey key: String) -> CLLocation?
  {
    if let locationDictionary = self.object(forKey: key) as? Dictionary<String,NSNumber> {
      let locationLat = locationDictionary["lat"]!.doubleValue
      let locationLon = locationDictionary["lon"]!.doubleValue
      return CLLocation(latitude: locationLat, longitude: locationLon)
    }
    return nil
  }
}
