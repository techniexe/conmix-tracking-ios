//
//  Enums.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 08/03/21.
//

import Foundation
import UIKit

enum LogInType: Int {
  case `default` = 0
  case mobile
  case email
  case facebook
  case google
  
  func name() -> String {
    switch self {
    case .mobile:
      return "Mobile"
    case .email:
      return "Email"
    case .facebook:
      return "Facebook"
    case .google:
      return "Google"
    default:
      return "none"
    }
  }
}

typealias MainFont = Font.Roboto

enum Font {
  
  enum Roboto: String {
    case regular = "Regular"
    case boldItalic = "BoldItalic"
    case black = "Black"
    case medium = "Medium"
    case bold = "Bold"
    case light = "Light"
    case lightItalic = "LightItalic"
    case thinItalic = "ThinItalic"
    case thin = "Thin"
    case blackItalic = "BlackItalic"
    case italic = "Italic"
    case mediumItalic = "MediumItalic"
    
    func with(size: CGFloat) -> UIFont {
      return UIFont(name: "Roboto-\(rawValue)", size: size)!
    }
  }
}

enum MinLength: Int {
  
  case personname = 3
  case password = 6
}

enum MaxLength: Int {
  
  case personname = 64
  case mobile = 13
  case password = 20
  case pan = 10
  case gst = 15
  case review = 250
  case complain = 249
  case pincode = 6
  case address = 500
}

enum ImageStyle: Int {
  case squared,rounded
}

public enum BarButtonItemType {
    case back
    case forward
    case reload
    case stop
    case activity
    case done
    case flexibleSpace
}

public enum NavigationBarPosition {
    case none
    case left
    case right
}

public enum NavigationWay {
    case browser
    case push
}

@objc public enum NavigationType: Int {
    case linkActivated
    case formSubmitted
    case backForward
    case reload
    case formResubmitted
    case other
}

enum CommonAlert {
  case pickUp
  case changeQty
  case acceptOrder
  case cancelOrder
  case logout
}

enum CartAction {
  case removeCartItem
  case choosetimeSlot
}

enum FromScreen {
  case truckdetails
  case truckLoadOrder
  case changeOrderQty
  case delayReason
}


internal enum Colors {

    static var gradientBackground: [CGColor] {
        if #available(iOS 13.0, *) {
            return [UIColor.systemGray4.cgColor, UIColor.systemGray5.cgColor, UIColor.systemGray5.cgColor]
        } else {
            return [UIColor(hexValue: 0xDADADE).cgColor, UIColor(hexValue: 0xEAEAEE).cgColor, UIColor(hexValue: 0xDADADE).cgColor]
        }
    }

    static var separator: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray3
        } else {
            return UIColor(hexValue: 0xD1D1D6)
        }
    }

    static var text: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.label
        } else {
            return UIColor(hexValue: 0x3993F8)
        }
    }

    static var accent: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemBlue
        } else {
            return UIColor(hexValue: 0x3993F8)
        }
    }

}

enum ReviewType {
  case add
  case edit
}
