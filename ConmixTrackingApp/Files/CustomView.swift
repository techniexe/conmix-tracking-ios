//
//  CustomView.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 18/03/21.
//

import UIKit

@IBDesignable
class CustomView: UIView {

  @IBInspectable var cornerRadius: CGFloat = 0 {
      didSet {
          layer.cornerRadius = cornerRadius
          layer.masksToBounds = cornerRadius > 0
      }
  }
  @IBInspectable var borderWidth: CGFloat = 0.0 {
      didSet {
          layer.borderWidth = borderWidth
      }
  }
  @IBInspectable var borderColor: UIColor? {
      didSet {
          layer.borderColor = borderColor?.cgColor
      }
  }

}

@IBDesignable
class CustomShadowView: UIView {
  
  @IBInspectable var cornerRadius: CGFloat {
    get{
      return layer.cornerRadius
    }
    set{
      self.layer.cornerRadius = newValue
      self.layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable var shadow:Bool{
    get{
      return false
    }
    set{
      if newValue == true {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero//CGSize(width:1.0, height: 5.0)
        self.layer.shadowOpacity = 0.2 //0.1
        self.layer.maskedCorners = CACornerMask(arrayLiteral: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner])
      }
      
    }
    
  }
  
}
