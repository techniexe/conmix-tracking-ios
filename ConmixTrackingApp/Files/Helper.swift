//
//  Helper.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 08/03/21.
//

import Foundation
import libPhoneNumber_iOS
import Photos

class Helper {
 
  //MARK:- Print
  static func DLog(message: String, function: String = #function) {
    #if DEBUG
    print("\(function): \(message)")
    #endif
  }
  
  //MARK:- Check Key in UserDefaults
  static func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
  }
  
  //MARK:- Check field is epmpty or not
  static func isEmpty(strText:String) -> Bool {
    
    if strText.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
      
      return true
    }
    return false
  }
  
  //MARK: - Check Valid Phone Number
  static func isValidPhoneNumber(_ strText: String, completion: @escaping(Bool, String, LogInType)-> Void) {
    
    var strMobileOrEmail : String = ""
    
    let phoneUtil = NBPhoneNumberUtil()
    
    do {
      
      let phoneNumber: NBPhoneNumber = try phoneUtil.parse(strText, defaultRegion: "IN")
      try phoneUtil.format(phoneNumber, numberFormat: .E164)
      
      if phoneUtil.getNumberType(phoneNumber) == .MOBILE || phoneUtil.getNumberType(phoneNumber) == .FIXED_LINE_OR_MOBILE   {
        
        let isNumberValid = phoneUtil.isValidNumber(phoneNumber)
        
        if isNumberValid == true {
          strMobileOrEmail = "+\(phoneNumber.countryCode!)\(phoneNumber.nationalNumber!)"
          
        }else {
          Helper.showToast(_strMessage: Constants.ErrorMessage.invalidmobileregioncode.localized())
        }
        
        completion(isNumberValid, strMobileOrEmail, .mobile)
        
      }else {
        Helper.showToast(_strMessage: Constants.ErrorMessage.invalidmobileregioncode.localized())
        completion(false, strMobileOrEmail, .mobile)
      }
      
    }
    catch let error as NSError {
      Helper.DLog(message: "\(error.localizedDescription)")
      
      if strText.trim().contains("@") == true {
        
        if Helper.isValidEmail(strText.trim()) == false {
          Helper.showToast(_strMessage: Constants.ErrorMessage.invalidemail.localized())
          completion(false, strMobileOrEmail, .email)
          return
        }else {
          strMobileOrEmail = strText.trim()
          completion(true, strMobileOrEmail, .email)
        }
        
        
      }else {
        Helper.showToast(_strMessage: Constants.ErrorMessage.invalidemailmobile.localized())
        completion(false, strMobileOrEmail, .email)
        return
      }
      
      
      
    }
  
  }
  
  
  
  //MARK:- Check Valid Phone Number
  static func checkPhoneNumber(_ strMobile: String) -> Bool {
    
    let phoneUtil = NBPhoneNumberUtil()
    
    do {
      
      let phoneNumber: NBPhoneNumber = try phoneUtil.parse(strMobile, defaultRegion: "IN")
      try phoneUtil.format(phoneNumber, numberFormat: .E164)
      
      if phoneUtil.getNumberType(phoneNumber) == .MOBILE || phoneUtil.getNumberType(phoneNumber) == .FIXED_LINE_OR_MOBILE   {
        
        let isNumberValid = phoneUtil.isValidNumber(phoneNumber)
        
        if isNumberValid == true {
          
          return true
          
        }else {
          return false
        }
        
      }else {
        return false
      }
      
    }
    catch let error as NSError {
      Helper.DLog(message: "\(error.localizedDescription)")
      return false
    }
  
  }
  
  
  
  //MARK:- Check Valid Email Address
  static func isValidEmail(_ email: String) -> Bool {
    
      let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
      let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
      return emailPred.evaluate(with: email)
  }
  
  //MARK:- Check Validate Pan Card
  static func isValidPAN(_ pannumber: String) -> Bool {
   
    let pancardregEx = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
    let pancardPred = NSPredicate(format:"SELF MATCHES %@", pancardregEx)
    return pancardPred.evaluate(with: pannumber)
    
  }
  
  //MARK:- Check Validate GST Number
  static func isValidGST(_ gstnumber: String) -> Bool {
    
    let gstcardregEx = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$"
    let gstcardPred = NSPredicate(format:"SELF MATCHES %@", gstcardregEx)
    return gstcardPred.evaluate(with: gstnumber)
    
  }
  
  
  
  //MARK:- Show Toast Message
  static func showToast(_strMessage: String?) {
    
    let toast = Toast(text: _strMessage)
    toast.show()
  }

  //MARK:- Show NVActivityIndicatorView
  static func showLoading(_ activityIndicator: NVActivityIndicatorView? = nil) {
    if let activity = activityIndicator {
      activity.startAnimating()
    }
    
  }
  
  //MARK:- Hide NVActivityIndicatorView
  static func hideLoading(_ activityIndicator: NVActivityIndicatorView? = nil) {
    if let activity = activityIndicator {
      activity.stopAnimating()
    }
  }
  
  // MARK: - Get Document Directory
  static func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
  }
  
  // MARK: - Clear Document Directory
  static func clearDocumentDirectory() {
    
    let dirPath = getDocumentsDirectory()
    
    do {
      let filePaths = try FileManager.default.contentsOfDirectory(atPath: dirPath.path)
      for filePath in filePaths {
        let path = dirPath.appendingPathComponent(filePath).path
        try FileManager.default.removeItem(atPath: path as String)
      }
    } catch {
      Helper.DLog(message: "Could not clear documents directory: \(error.localizedDescription)")
    }
    
  }

  //MARK:- PhotoLibraryAuthorizationStatus
  static func photoLibraryAuthorizationStatus(_ status: @escaping (Bool) -> Void) {
    
    switch PHPhotoLibrary.authorizationStatus() {
    case .denied:
      status(false)
    case .authorized:
      status(true)
    case .restricted:
      status(false)
    case .notDetermined:
      PHPhotoLibrary.requestAuthorization { granted in
        if granted == .authorized {
          status(true)
        } else {
          status(false)
        }
      }
    case .limited:
      status(false)
    @unknown default:
      fatalError()
    }
    
  }
  
  //MARK:- CameraAuthorizationStatus
  static func cameraAuthorizationStatus(_ status: @escaping (Bool) -> Void) {
    switch AVCaptureDevice.authorizationStatus(for:.video) {
    case .denied:
      status(false)
    case .authorized:
      status(true)
    case .restricted:
      status(false)
    case .notDetermined:
      AVCaptureDevice.requestAccess(for:.video) { (granted) in
        status(granted)
      }
    @unknown default:
      fatalError()
    }
  }
  
  //MARK:- Helper function inserted by Swift 4.2 migrator.
  static func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
  }
  
  //MARK:- Helper function inserted by Swift 4.2 migrator.
  static func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
  }
  
  //MARK:- Number Of Lines
  static func numberOfLines(textView: UITextView) -> Int {
      let layoutManager = textView.layoutManager
      let numberOfGlyphs = layoutManager.numberOfGlyphs
      var lineRange: NSRange = NSMakeRange(0, 1)
      var index = 0
      var numberOfLines = 0
      
      while index < numberOfGlyphs {
          layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
          index = NSMaxRange(lineRange)
          numberOfLines += 1
      }
      return numberOfLines
  }
  
  static func getQuantity(_ quantity: NSNumber) -> String {
  
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.locale = Locale.current
    formatter.minimumFractionDigits = 1
    formatter.maximumFractionDigits = 2
    return "\(formatter.string(from: quantity) ?? "") Cu.Mtr"
  }
  
  static func getOrderQuantity(_ quantity: NSNumber) -> String {
  
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.locale = Locale.current
    formatter.minimumFractionDigits = 1
    formatter.maximumFractionDigits = 2
    return "\(formatter.string(from: quantity) ?? "")"
  }
  
  static func getPincode(_ pincode: NSNumber) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .none
    currencyFormatter.locale = Locale.current
    if let strPincode = currencyFormatter.string(from: pincode) {
      return strPincode
    }
    
    return ""
  }
  
  static func getStringFromDoubleWithNumberFormatter(_ number: Double) -> String {
  
    let price = NSNumber(value: number)
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
    if let strPrice = currencyFormatter.string(from: price) {
      return strPrice.removingWhitespaces()
    }
    
    return ""
    
  }
  
  static func getStringFromNumberFormatter(_ price: NSNumber) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    //currencyFormatter.locale = Locale.current
    currencyFormatter.locale = Locale(identifier: "en_IN")
    currencyFormatter.maximumFractionDigits = 2
    if let strPrice = currencyFormatter.string(from: price) {
      return strPrice
    }
    
    return ""
  }


  static func getTMPumpText(withTM: Bool, withCP: Bool) -> String {
    
    var strWithTMPumpText: String = ""
    
    if withTM == true && withCP == true {
      strWithTMPumpText = "With TM & CP"
    }else if withTM == true {
      strWithTMPumpText = "With TM"
    }else if withCP == true {
      strWithTMPumpText = "With CP Available Date"
    }else {
      strWithTMPumpText = "Without TM & CP"
    }
    
    return strWithTMPumpText
  }
  
  //MARK:- Set Mobile OTP Text
  static func setMobileOTPText(_ strMobile: String) -> String {
    
    var strFinal: String = ""
    
    strFinal = "\(Constants.Text.OTPText.localized()) \(strMobile.prefix(2))XXXXXX\(strMobile.suffix(2))"
    
    return strFinal
    
  }
  
  //MARK:- Set Email OTP Text
  static func setEmailOTPText(_ strEmail: String) -> String {
    
    var strFinal: String = ""
    
    strFinal = "\(Constants.Text.OTPEmailText.localized()) \(strEmail)"
    
    return strFinal
    
  }
  
  static func convertNumberToString(_ number: NSNumber) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .decimal
    currencyFormatter.locale = Locale.current
    currencyFormatter.minimumFractionDigits = 0
    currencyFormatter.maximumFractionDigits = 2
    if let str = currencyFormatter.string(from: number) {
      return str
    }
    
    return ""
  }
  
  static func convertNumberToYear(_ number: NSNumber) -> String {
    
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .none
    currencyFormatter.locale = Locale.current
    if let str = currencyFormatter.string(from: number) {
      return str
    }
    
    return ""
  }

  
  static func getStoryBorad() -> UIStoryboard {
    let objStroyBoard = UIStoryboard(name: "NoDataVC", bundle: nil)
    return objStroyBoard
  }
  
  //MARK:- Get Address
  static func getAddress(_ strLine1: String?, strLine2: String?, strCityName: String?, strStateName: String?, pincode: Double?) -> String {
   
    var strAddress: String = ""
    
    if let line1 = strLine1, strLine1 != "" {
      strAddress = String(format: "%@", line1)
    }
    
    if let line2 = strLine2, line2 != "" {
      strAddress += String(format: ",%@", line2)
    }
    
    if let cityName = strCityName, cityName != "" {
      strAddress += String(format: ",%@", cityName)
    }
    
    if let statename = strStateName, statename != "" {
      strAddress += String(format: ",%@", statename)
    }
    
    if let pincodes = pincode {
      let code = Helper.getPincode(NSNumber(value: pincodes))
      strAddress += String(format: "-%@", code)
    }
    
    return strAddress.trim()
  }
  
  //MARK:- Seconds To Hours Minutes Seconds
  static func secondsToHoursMinutesSeconds(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {

    completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)

  }
  
  //MARK:- Get String From Hours Minutes Seconds
  static func getStringFrom(seconds: Int) -> String {

      return seconds < 10 ? "0\(seconds)" : "\(seconds)"
  }
  
  static func isTransitMixer() -> Bool {
    
    if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.OptionForDelivery) == true {
      
      let isTransitMixer = UserDefaults.standard.value(forKey: Constants.UserDefaults.OptionForDelivery) as! Bool
      
      return isTransitMixer
      
    }else {
      return false
    }
  }
  
  static func getCapacity(_ quantity: NSNumber) -> String {
  
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.locale = Locale.current
    formatter.minimumFractionDigits = 1
    formatter.maximumFractionDigits = 2
    return "\(formatter.string(from: quantity) ?? "") Cu.Mtr"
  }
  
  static func getCurrentDate(_ date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Constants.DateFormats.current
    let strCurrentDate = dateFormatter.string(from: date)
    return strCurrentDate
  }
  
  static func getCPID() -> String {
    
    if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.CPID) == true {
      
      let strCPID = UserDefaults.standard.value(forKey: Constants.UserDefaults.CPID) as! String
      
      return strCPID
      
    }else {
      return ""
    }
  }
  
  
  static func setSignatureTitle(_ title: String, subTitle: String) -> NSMutableAttributedString {
    
    let strTitle = String(format: "\(title) : %@", subTitle) as NSString
    
    let strMutableAttributed =  NSMutableAttributedString(string: strTitle as String)
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(title) : "))
    
    strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(subTitle)"))
    
    return strMutableAttributed
  }
  
  static func tapOnMobileNumber(_ phoneNumber: String) {
    
    guard let number = URL(string: "tel://" + phoneNumber) else { return }
    UIApplication.shared.open(number)
  }
}
