//
//  AppDelegate.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 06/05/21.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    return true
  }
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    configureIQKeyboard()
    configureGoogleAPIKey()
    GlobalFunction.setRootViewController(direction: UIWindow.TransitionOptions.Direction.toRight)
    return true
  }

  //MARK:- configure IQKeyboard
  func configureIQKeyboard() {
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.enableAutoToolbar = true
    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
  }
  
  //MARK:- configure Google API Key
  func configureGoogleAPIKey() {
    GMSPlacesClient.provideAPIKey(Constants.googleApiKey)
    GMSServices.provideAPIKey(Constants.googleApiKey)
    GoogleApi.shared.initialiseWithKey(Constants.googleApiKey)
  }
}

