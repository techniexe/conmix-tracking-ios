//
//  Auth.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 06/05/21.
//

import Foundation


extension APIHandler {
  
  //MARK:- LogIn with Mobile Number
  func login(_ strIdentifier: String, strPassword: String, completion: @escaping(Int?, AppData<CustomToken>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.loginMobile
      
      let pathData = [Constants.Placeholders.identifier : strIdentifier]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getResponse(method: .post, url: url, bodyData: ["password": strPassword], completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Request OTP With Mobile Number
  func requestOTPWithMobile(_ strMobileNumber: String, completion: @escaping(Int?, Code?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.requestMobileOTP
      
      let pathData = [Constants.Placeholders.mobileNumber : strMobileNumber]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- OTP Verification With Mobile Number
  func OTPVerificationWithMobile(_ strMobileNumber: String, strCode: String, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.mobileOTPVerification
      
      let pathData = [Constants.Placeholders.mobileNumber : strMobileNumber, Constants.Placeholders.code : strCode]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Get Vendor TM List
  func getListOfVendorTM(_ queryParams: [String:Any]?, completion: @escaping(Int?, AppData<[TMDetails]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.getVendorTMList
      
      let url =  getURL(path:path, pathVariables: nil, queryParams:queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Get Vendor TM Details
  func getVendorTMDetails(_ strTMID: String, completion: @escaping(Int?, AppData<TMDetails>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.getVendorTMDetails
    
      let pathData = [Constants.Placeholders.tmID : strTMID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- TM Assigned to Order
  func assignedToOrder(_ strOrderID: String, strOrderItemID: String, bodyParams: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.tmAssignedToOrder
      
      let pathData = [Constants.Placeholders.orderID : strOrderID, Constants.Placeholders.orderItemID : strOrderItemID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: bodyParams, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Get Order By TM
  func getOrderByTM(_ strTMID: String, queryParams: [String:Any]?, completion: @escaping(Int?, OrderList?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.orderByTMID
      
      let pathData = [Constants.Placeholders.tmID : strTMID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Change Order Qty
  func changeOrderQty(_ strTMID: String, strOrderID: String, strTrackID: String, bodyParams: [String: Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.changeOrderQty
      
      let pathData = [Constants.Placeholders.trackID : strTrackID, Constants.Placeholders.tmID: strTMID, Constants.Placeholders.orderID: strOrderID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .patch, bodyData: bodyParams, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Change Order Status
  func changeOrderStatus(_ strTMID: String, strOrderID: String, strTrackID: String, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.changeOrderStatus
      
      let pathData = [Constants.Placeholders.trackID : strTrackID, Constants.Placeholders.tmID: strTMID, Constants.Placeholders.orderID: strOrderID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Upload Document
  func uploadDocument(_ strTrackingID: String, strTMID: String, strOrderID: String, arrUploadDoc: [UploadDocument], completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.uploadDocument
      
      let pathData = [Constants.Placeholders.trackID: strTrackingID, Constants.Placeholders.tmID: strTMID, Constants.Placeholders.orderID : strOrderID]
      
      let url = getURL(path:path, pathVariables:pathData, queryParams:nil)
      
      uploadImageAsMultipartformData(url: url, method: .post, bodyData: nil, arrUploadDoc: arrUploadDoc, completion: completion)
      
    }else{
      GlobalFunction.noNetworkConnection()
    }
  }
  
  
  //MARK:- Order Deliver
  func deliverOrder(_ strTMID: String, strTrackingID: String,  strOrderID: String, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.deliverOrder
      
      let pathData = [Constants.Placeholders.tmID: strTMID, Constants.Placeholders.trackID: strTrackingID, Constants.Placeholders.orderdID : strOrderID]
      
      let url =  getURL(path:path, pathVariables:pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: nil, completion: completion)
      
    }else{
      GlobalFunction.noNetworkConnection()
    }
  }
  
  
  //MARK:- Order Deliver
  func delayOrder(_ strTMID: String, strTrackingID: String,  strOrderID: String, bodyParams: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.delayOrder
      
      let pathData = [Constants.Placeholders.tmID: strTMID, Constants.Placeholders.trackID: strTrackingID, Constants.Placeholders.orderdID : strOrderID]
      
      let url = getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: bodyParams, completion: completion)
      
    }else{
      GlobalFunction.noNetworkConnection()
    }
  }
 
  
  //MARK:- Get Order Details
  func getOrderDetails(_ strOrderID: String, strTrackID: String, completion: @escaping(Int?, OrderDetails?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.orderDetails
      
      let pathData = [Constants.Placeholders.orderID : strOrderID, Constants.Placeholders.trackID: strTrackID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Get Vendor Order Track Details
  func getVendorOrderTrackDetails(_ strOrderItemID: String, strTrackingID: String, completion: @escaping(Int?, AppData<VendorOrderDetailsForTracking>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.orderVendorOrderTrackDetails
      
      let pathData = [Constants.Placeholders.orderItemID : strOrderItemID, Constants.Placeholders.trackingID: strTrackingID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  /************************Concrete Pump****************************************/
  
  //MARK:- Get Vendor CP List
  func getListOfVendorCP(completion: @escaping(Int?, AppData<[ConcretePumpDetails]>?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.getVendorCPList
      
      let url =  getURL(path:path, pathVariables: nil, queryParams:nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Get Order List By CP
  func getOrderListByCPID(_ strCPID: String, queryParams: [String:Any]?, completion: @escaping(Int?, OrderList?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.orderByCPID
      
      let pathData = [Constants.Placeholders.cpID : strCPID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:queryParams)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Get Order Details
  func getCPOrderDetails(_ strOrderID: String, strTrackID: String, completion: @escaping(Int?, OrderDetails?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.cpOrderDetails
      
      let pathData = [Constants.Placeholders.orderID : strOrderID, Constants.Placeholders.trackID: strTrackID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getResponse(method: .get, url: url, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Upload Document FOR CP
  func uploadDocumentForCP(_ strTrackingID: String, strCPID: String, strOrderID: String, arrUploadDoc: [UploadDocument], completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.cpUploadDocument
      
      let pathData = [Constants.Placeholders.trackID: strTrackingID, Constants.Placeholders.cpID: strCPID, Constants.Placeholders.orderID : strOrderID]
      
      let url = getURL(path:path, pathVariables:pathData, queryParams:nil)
      
      uploadImageAsMultipartformData(url: url, method: .post, bodyData: nil, arrUploadDoc: arrUploadDoc, completion: completion)
      
    }else{
      GlobalFunction.noNetworkConnection()
    }
  }
  
  

  //MARK:- Change Order Status For CP
  func changeOrderStatusForCP(_ strCPID: String, strOrderID: String, strTrackID: String, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.cpChangeOrderStatus
      
      let pathData = [Constants.Placeholders.trackID : strTrackID, Constants.Placeholders.cpID: strCPID, Constants.Placeholders.orderID: strOrderID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Order Deliver For CP
  func deliverOrderForCP(_ strCPID: String, strTrackingID: String,  strOrderID: String, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.cpDeliverOrder
      
      let pathData = [Constants.Placeholders.cpID: strCPID, Constants.Placeholders.trackID: strTrackingID, Constants.Placeholders.orderdID : strOrderID]
      
      let url =  getURL(path:path, pathVariables:pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: nil, completion: completion)
      
    }else{
      GlobalFunction.noNetworkConnection()
    }
  }
  
  
  //MARK:- Order Deliver For CP
  func delayOrderForCP(_ strCPID: String, strTrackingID: String,  strOrderID: String, bodyParams: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.cpDelayOrder
      
      let pathData = [Constants.Placeholders.cpID: strCPID, Constants.Placeholders.trackID: strTrackingID, Constants.Placeholders.orderdID : strOrderID]
      
      let url = getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: bodyParams, completion: completion)
      
    }else{
      GlobalFunction.noNetworkConnection()
    }
  }
  
  //MARK:- Change Order Qty For CP
  func changeOrderQtyForCP(_ strCPID: String, strOrderID: String, strTrackID: String, bodyParams: [String: Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.cpChangeOrderQty
      
      let pathData = [Constants.Placeholders.trackID : strTrackID, Constants.Placeholders.cpID: strCPID, Constants.Placeholders.orderID: strOrderID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .patch, bodyData: bodyParams, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Check CP Status
  func checkCPStatus(_ strOrderID: String, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.checkCPStatus
      
      let pathData = [ Constants.Placeholders.orderID: strOrderID]
      
      let url =  getURL(path:path, pathVariables: pathData, queryParams:nil)
      
      getStatusCode(url: url, method: .get, bodyData: nil, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Upload Invoice
  func uploadInvoice(_ bodyParams: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.uploadInvoice
      
      let url =  getURL(path:path, pathVariables: nil, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: bodyParams, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  //MARK:- Upload Invoice
  func uploadVendorInvoice(_ bodyParams: [String:Any]?, completion: @escaping(Int?, String?) -> Void) {
    
    if Reachability.isConnectedToNetwork() {
      
      let path = Constants.APIPaths.uploadVendorInvoice
      
      let url =  getURL(path:path, pathVariables: nil, queryParams:nil)
      
      getStatusCode(url: url, method: .post, bodyData: bodyParams, completion: completion)
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
}
