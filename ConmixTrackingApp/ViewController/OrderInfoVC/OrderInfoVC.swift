//
//  OrderInfoVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class OrderInfoVC: UIViewController {
  
  //MARK:- @IBOutlets
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var btnMenu: UIButton!
  @IBOutlet weak var lblHeader: UILabel!
  
  //MARK:- @vars
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var fromScreen: FromScreen = .changeOrderQty
  var strOrderID: String? = nil
  var strTrackingID: String? = nil
  var alertIndex: Int!
  var isTM: Bool = false
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    self.isTM = Helper.isTransitMixer()
    self.lblHeader.text = Constants.Text.OrderInfo.localized()
    self.tblView.register(UINib(nibName: "OrderInfoCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.orderInfoCellID)
    if fromScreen == .delayReason {
      self.btnMenu.isHidden = false
    }else{
      self.btnMenu.isHidden = true
    }
    self.callGetOrderDetailsAPI()
  }
  
  
  // MARK: - Call Get Order API
  func callGetOrderDetailsAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
      
      if self.isTM == true {
        
        APIHandler.sharedInstance.getOrderDetails(strOrderID ?? "", strTrackID: strTrackingID ?? "") { statuscode, response, error in
          
          Helper.hideLoading(self.activityIndicator)
          
          if statuscode == 200 {
            
            if let orderResponse  = response?.order {
              
              self.dicOrderDetails = orderResponse
              
            }
            
          }else {
            Helper.showToast(_strMessage: error)
            
          }
          
          DispatchQueue.main.async {
            self.updateUI()
          }
          
        }
        
      }else {
        
        if let orderID = self.strOrderID, let trackingID = self.strTrackingID {
          
          APIHandler.sharedInstance.getCPOrderDetails(orderID, strTrackID: trackingID) { (statuscode, response, error) in
            
            Helper.hideLoading(self.activityIndicator)
            
            if statuscode == 200 {
              
              if let orderResponse  = response?.order {
                
                self.dicOrderDetails = orderResponse
                
              }
              
            }else {
              Helper.showToast(_strMessage: error)
              
            }
            
            DispatchQueue.main.async {
              self.updateUI()
            }
            
          }
          
        }
        
        
        
      }
      
    }
    else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  // MARK: - Update UI
  func updateUI() {
    
    guard self.dicOrderDetails != nil else {
      let storyBoard = Helper.getStoryBorad()
      if let objNoDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC {
        objNoDataVC.view.frame = CGRect(x: 0.0, y: 44.0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 44)
        objNoDataVC.lblTitle.text = Constants.Text.NoOrderInfo.localized()
        self.contentView.addSubview(objNoDataVC.view)
      }
      return
    }
    
    //dicOrderDetails?.pickupQuantity = royaltyQty
    
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.isHidden = false
    self.tblView.reloadData()
    
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionRightMenu(_ sender: Any) {
    self.openAlertController()
  }
  
  // MARK: - openAlertController
  func openAlertController() {
    
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
    
    let cancelOrderAction = UIAlertAction(title: Constants.Text.RejectOrder.localized(), style: .default, handler: {
      (alert: UIAlertAction!) -> Void in
      
      self.openCustomAlert(0, alert: .cancelOrder)
      
    })
    
    cancelOrderAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
    
    let cancelAction = UIAlertAction(title: Constants.Toast.cancel.localized(), style: .cancel, handler: {
      (alert: UIAlertAction!) -> Void in
      //self.dismiss(animated: true, completion: nil)
      //self.isImageUpdate = false
    })
    
    cancelAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
    
    alertController.addAction(cancelOrderAction)
    alertController.addAction(cancelAction)
    
    if #available(iOS 13.0, *) {
      alertController.overrideUserInterfaceStyle = .light
    } else {
      // Fallback on earlier versions
    }
    
    alertController.pruneNegativeWidthConstraints()
    self.present(alertController, animated: true, completion: nil)
  }
  
  //MARK:- Open Custom Alert Screen
  func openCustomAlert(_ index: Int, alert: CommonAlert = .cancelOrder) {
    
    self.alertIndex = index
    CommonAlertVC.showPopup(parentVC: self, options: alert)
    
  }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension OrderInfoVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.orderInfoCellID, for: indexPath) as? OrderInfoCell else {
      return UITableViewCell()
    }
    
    objCell.delegate = self
    
    
    if fromScreen == .delayReason {
      objCell.btnProceed.setTitle(Constants.Text.AcceptOrder.localized(), for: .normal)
    }else {
      objCell.btnProceed.setTitle(Constants.Text.Next.localized(), for: .normal) //PROCEED
    }
    
    if let details = self.dicOrderDetails {
      objCell.configureCell(details)
    }
    return objCell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
   /* let header = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: 40.0))
    header.backgroundColor = UIColor.white
  
    
    let label = UILabel(frame: CGRect(x: 15.0, y: 10, width: header.bounds.size.width - 30, height: header.bounds.size.height-10))
    label.text = "Order Summary"
    label.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
    label.backgroundColor = UIColor.clear
    label.textColor = UIColor.black
    header.addSubview(label)
  
    return header*/
    
    let header = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: 20.0))
    header.backgroundColor = UIColor.white
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 20
  }
  
}


// MARK: -  LoadOrderViewDelegate
extension OrderInfoVC: OrderInfoViewDelegate {
  
  func actionProceedOrder() {
    
    if fromScreen == .delayReason {
      self.openCustomAlert(1, alert: .acceptOrder)
    }else {
      
      /*if Reachability.isConnectedToNetwork() {
        
        APIHandler.shared.updateOrderStatus(orderDetailsViewModel.orderDetails?._id ?? "", strVehicleID: strVehicleID ?? "", strOrderID: orderDetailsViewModel.orderDetails?.logistics_order_info?._id ?? "") { (statusCode, error) in
          
          if statusCode == 202 {
            
            Helper.showToast(_strMessage: SuccessMessage.pickupstatus.rawValue)
            
            if let objUploadDocumentVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"UploadDocumentVC") as? UploadDocumentVC {
              objUploadDocumentVC.orderDetailsViewModel = self.orderDetailsViewModel
              self.navigationController?.pushViewController(objUploadDocumentVC, animated: true)
            }
            
          } else {
            Helper.showToast(_strMessage: error)
            return
          }
         
          
        }
      }else {
        Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
      }*/
      
      let storyBoard = UIStoryboard(name: "UploadDocumentVC", bundle: nil)
      if let objUploadDocumentVC = storyBoard.instantiateViewController(withIdentifier:"UploadDocumentVC") as? UploadDocumentVC {
        objUploadDocumentVC.dicOrderDetails = self.dicOrderDetails
        self.navigationController?.pushViewController(objUploadDocumentVC, animated: true)
      }
    }
  }
  
}


//MARK:- PopUpProtocol
extension OrderInfoVC: PopUpProtocol {
  
  func handleAction(action: Bool) {
    if (action) {
      if self.alertIndex == 0 {
        
        let storyBoard = UIStoryboard(name: "RejectReasonVC", bundle: nil)
        if let objRejectReasonVC = storyBoard.instantiateViewController(withIdentifier:"RejectReasonVC") as? RejectReasonVC {
          objRejectReasonVC.delegate = self
          objRejectReasonVC.strDeliveryStatus = "REJECTED"
          objRejectReasonVC.dicOrderDetails = self.dicOrderDetails
          objRejectReasonVC.modalPresentationStyle = .fullScreen
          self.present(objRejectReasonVC, animated: true, completion: nil)
          
        }
        
      }else {
        self.callSendOTPAPI("ACCEPTED")
        //self.callUpdateOrderStatusAPI()
      }
    }
  }
  
}

extension OrderInfoVC {
  
  // MARK: - Call Update Order Status API
 /* func callUpdateOrderStatusAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      var strDeliverStatus: String! = ""
      
      if self.alertIndex == 0 {
        strDeliverStatus = "REJECTED"
      }else {
        strDeliverStatus = "ACCEPTED"
      }
      
      let params: [String: Any]?  = ["delivery_status": strDeliverStatus ?? ""]
      
      APIHandler.shared.updateOrderQty(orderDetailsViewModel.orderDetails?._id ?? "",strVehcileID: strVehicleID ?? "", strOrderID: orderDetailsViewModel.orderDetails?.logistics_order_info?._id ?? "", bodyParams: params) { (statusCode, error) in
        
        if statusCode == 202 {
          //Helper.showToast(_strMessage: "order status updated successfully.")
          
          if strDeliverStatus == "REJECTED" {
            self.navigationController?.popToRootViewController(animated: true)
          }else {
            
            self.callSendOTPAPI(strDeliverStatus)
            
            /*if let objDrawSignatureVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
              objDrawSignatureVC.isForDropOrder = true
              objDrawSignatureVC.orderDetailsViewModel = self.orderDetailsViewModel
              self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
            }*/
          }
        } else {
          Helper.showToast(_strMessage: error)
          return
        }
      
      }
    }else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
   
  }*/
  
  // MARK: - CALL SEND OTP
  func callSendOTPAPI(_ deliveryStatus: String) {
    
    if let buyerInfo = self.dicOrderDetails?.buyerInfo {
      
      if let mobileNumber = buyerInfo.mobileNumber {
        
        APIHandler.sharedInstance.requestOTPWithMobile(mobileNumber) { (statuscode, response, error) in
          
          if statuscode == 200 {
            if let code = response?.code, code != "" {
              
              Helper.showToast(_strMessage: Constants.SuccessMessage.codesuccess.localized())
              
              let storyBoard = UIStoryboard(name: "MobileOTPVC", bundle: nil)
             
              if let objMobileOTPVC = storyBoard.instantiateViewController(withIdentifier:"MobileOTPVC") as? MobileOTPVC {
                objMobileOTPVC.strDeliveryStatus = deliveryStatus
                objMobileOTPVC.dicOrderDetails = self.dicOrderDetails
                self.navigationController?.pushViewController(objMobileOTPVC, animated: true)
              }
              
            }
          }
          else {
            Helper.showToast(_strMessage: error)
          }
          
        }
        
      }
      
    }
  }
}


// MARK: - RejectReasonDelegate
extension OrderInfoVC: RejectReasonDelegate {
 
  func actionButton(_ strText: String, viewController: UIViewController) {
    
    let stroyBoard = UIStoryboard(name: "MobileOTPVC", bundle: nil)
    if let objMobileOTPVC = stroyBoard.instantiateViewController(withIdentifier:"MobileOTPVC") as? MobileOTPVC {
      objMobileOTPVC.strRejectReason = strText
      objMobileOTPVC.strDeliveryStatus = "REJECTED"
      objMobileOTPVC.dicOrderDetails = self.dicOrderDetails
      self.navigationController?.pushViewController(objMobileOTPVC, animated: true)
      
    }
  }

}
