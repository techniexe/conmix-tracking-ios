//
//  MobileOTPVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 01/02/21.
//  Copyright © 2021 Techniexe Infolabs. All rights reserved.
//

import UIKit

class MobileOTPVC: UIViewController {
  
  @IBOutlet weak var lblHeader: UILabel!
  
  @IBOutlet weak var lblConfirmCode: UILabel!
  
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  @IBOutlet weak var lblTitle: UILabel!
  
  @IBOutlet weak var lblTimer: UILabel!
  
  @IBOutlet weak var btnResend: UIButton!
  
  @IBOutlet weak var btnVerify: UIButton!
  
  @IBOutlet weak var otpTextFieldView: OTPFieldView!
  
  // MARK: - @vars
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var strDeliveryStatus: String = ""
  var strRejectReason: String = ""
  var timer: Timer? = nil
  var seconds: Int = Constants.OTPSeconds
  var mobileNumber: String = ""
  var isTM: Bool = false
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    initializeOnce()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let textFiled = self.otpTextFieldView.viewWithTag(1) as? UITextField
    textFiled?.becomeFirstResponder()
    setupTimer()
  }
  
  // MARK: - initializeOnce
  func initializeOnce() {
    
    self.isTM = Helper.isTransitMixer()
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    
    self.lblHeader.text = Constants.Text.OTP.localized()
    self.lblConfirmCode.text = Constants.Text.OTPConfirmCode.localized()
    self.btnResend.setTitle(Constants.Text.ResendCode.localized(), for: .normal)
    self.btnVerify.setTitle(Constants.Text.Verify.localized(), for: .normal)
    self.btnVerify.isEnabled = false
    self.btnVerify.isUserInteractionEnabled = false
    self.btnVerify.alpha = 0.5
    self.lblTitle.textAlignment = .justified
   
    
    
    if let buyerInfo = self.dicOrderDetails?.buyerInfo {
      
      if let mobileNumber = buyerInfo.mobileNumber {
        
        self.mobileNumber = mobileNumber
      
        if self.mobileNumber.count > 10 {
      
          let strMobile: String = self.mobileNumber
          
          let newString = strMobile.dropFirst(self.mobileNumber.count - 10)
          
          self.lblTitle.text = Helper.setMobileOTPText(String(newString))
          
        }else if self.mobileNumber.count == 10 {
          
          let strMobile: String = self.mobileNumber
          
          self.lblTitle.text = Helper.setMobileOTPText(strMobile)

        }else {
          
          self.lblTitle.text = ""
        }
        
        
        //let title = Constants.Text.OTPText.localized()
        
        //self.lblTitle.text = "\(title) \(self.mobileNumber)."
      }else {
       
        self.lblTitle.text = ""
        
      }
    }
    
    setupOtpView()
  }
  
  // MARK: - Setup OTP View
  func setupOtpView(){
    self.otpTextFieldView.fieldsCount = 6
    self.otpTextFieldView.fieldBorderWidth = 2
    self.otpTextFieldView.defaultBorderColor = Constants.Colors.lineColor ?? UIColor.black
    self.otpTextFieldView.filledBorderColor = Constants.Colors.appRedColor ?? UIColor.blue
    self.otpTextFieldView.cursorColor = UIColor.blue
    self.otpTextFieldView.displayType = .underlinedBottom
    self.otpTextFieldView.fieldSize = 40
    self.otpTextFieldView.layoutIfNeeded()
    self.otpTextFieldView.separatorSpace = ((self.otpTextFieldView.bounds.width - (self.otpTextFieldView.fieldSize * CGFloat(self.otpTextFieldView.fieldsCount))) / CGFloat((self.otpTextFieldView.fieldsCount - 1)))
    self.otpTextFieldView.shouldAllowIntermediateEditing = false
    self.otpTextFieldView.delegate = self
    self.otpTextFieldView.initializeUI()
  }
  
  // MARK: - setup Timer
  func setupTimer() {
    
    if timer == nil {
      
      timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
        if self.seconds > 0 {
          self.setTimerText(seconds: self.seconds)
          self.seconds -= 1
        } else {
          self.btnResend.isHidden = false
          self.lblTimer.isHidden = true
          Timer.invalidate()
          self.invalidateTimer()
        }
      }
      
    }
    
  }
  
  // MARK: - Invalid Timer
  func invalidateTimer() {
    self.timer?.invalidate()
    self.timer = nil
  }
  
  
  //MARK:- Set Timer Text
  func setTimerText(seconds: Int) {
  
    Helper.secondsToHoursMinutesSeconds(seconds: seconds) { hours, minutes, seconds in
      
      let minutes = Helper.getStringFrom(seconds: minutes)
      let seconds = Helper.getStringFrom(seconds: seconds)
      self.lblTimer.text = "\(minutes):\(seconds)"
    }
    
  }
  
  // MARK: - Call Verify Mobile Number API
  func callVerifyMobileNumberAPI(otp: String ) {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.sharedInstance.OTPVerificationWithMobile(self.mobileNumber, strCode: otp) { (statuscode, error) in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 200 {
          self.callUpdateOrderStatusAPI()
        }else{
          Helper.showToast(_strMessage: error)
        }
        
      }
      
    }
    
    else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
  
  
  // MARK: - Call GET OTP API
  func callResendOTPAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.sharedInstance.requestOTPWithMobile(self.mobileNumber)  { (statuscode, response, error) in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        guard error == nil else {
          Helper.showToast(_strMessage: error)
          return
        }
        
        if statuscode == 200 {
          
          if let code = response?.code, code != "" {
            
            Helper.showToast(_strMessage: Constants.SuccessMessage.codesuccess.localized())
            self.btnResend.isHidden = true
            self.lblTimer.isHidden = false
            self.setupTimer()
          }
          
        }else {
          Helper.showToast(_strMessage: error)
        }
        
      }
      
    }else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
  }
  
  
  
  // MARK: - Call Update Order Status API
  func callUpdateOrderStatusAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      var params: [String: Any]?  = ["order_status": strDeliveryStatus]
      
      if self.strDeliveryStatus == "REJECTED" {
        params!["reject_reason"] = self.strRejectReason
      }
   
      if self.isTM == true {
        
        APIHandler.sharedInstance.changeOrderQty(strVehicleID ?? "", strOrderID: self.dicOrderDetails?.vendorOrderInfo?._id ?? "", strTrackID: self.dicOrderDetails?._id ?? "", bodyParams: params) { (statusCode, error) in
          
          if statusCode == 202 {
            
            if self.strDeliveryStatus == "REJECTED" {
              
              self.invalidateTimer()
              self.navigationController?.popToRootViewController(animated: true)
              
            }else {
              
              let storyBoard = UIStoryboard(name: "DrawSignatureVC", bundle: nil)
              if let objDrawSignatureVC = storyBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
                objDrawSignatureVC.isForDropOrder = true
                objDrawSignatureVC.dicOrderDetails = self.dicOrderDetails
                self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
                self.invalidateTimer()
              }
              
            }
            
          } else {
            Helper.showToast(_strMessage: error)
            return
          }
          
        }
        
      }else {
        
        let strCPId = Helper.getCPID()
        
        APIHandler.sharedInstance.changeOrderQtyForCP(strCPId, strOrderID: self.dicOrderDetails?.vendorOrderInfo?._id ?? "", strTrackID:  self.dicOrderDetails?._id ?? "", bodyParams: params) { (statusCode, error) in
          
          if statusCode == 202 {
            
            if self.strDeliveryStatus == "REJECTED" {
              
              self.invalidateTimer()
              self.navigationController?.popToRootViewController(animated: true)
              
            }else {
              
              let storyBoard = UIStoryboard(name: "DrawSignatureVC", bundle: nil)
              if let objDrawSignatureVC = storyBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
                objDrawSignatureVC.isForDropOrder = true
                objDrawSignatureVC.dicOrderDetails = self.dicOrderDetails
                self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
                self.invalidateTimer()
              }
              
            }
            
          } else {
            Helper.showToast(_strMessage: error)
            return
          }
        }
        
      }
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
   
  }
  
  // MARK: - @IBActions
  @IBAction func actionResendCode(_ sender: Any) {
    
    self.seconds = Constants.OTPSeconds
    
    setTimerText(seconds: seconds)
    
    let otpTextFields = self.otpTextFieldView.subviews as! [OTPTextField]
    
    for aTextField in otpTextFields {
      self.otpTextFieldView.deleteText(in: aTextField)
    }
    
    callResendOTPAPI()
  }
  
  @IBAction func actionVerify(_ sender: Any) {
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.invalidateTimer()
    self.navigationController?.popViewController(animated: true)
  }
  
}


// MARK: - UITextFieldDelegate
extension MobileOTPVC: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    return true
  }
  
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    guard let text = textField.text else { return true }
    
    let newLength = text.count + string.count - range.length
    
    return newLength <= 1
  }
  
}

// MARK: - OTPFieldViewDelegate
extension MobileOTPVC: OTPFieldViewDelegate {
  func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
    return true
  }
  
  func enteredOTP(otp: String) {
    //self.btnVerify.isUserInteractionEnabled = true
    //self.btnVerify.alpha = 1.0
    callVerifyMobileNumberAPI(otp: otp)
  }
  
  func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
    return true
  }

  
}
