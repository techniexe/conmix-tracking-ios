//
//  GalleryVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 21/10/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class GalleryVC: UIViewController {

  // MARK: - @@IBOutlet
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  // MARK: - @vars
  var strUrl: String = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
   
  // MARK: - setupView
  func setupView() {
    
    Helper.showLoading(self.activityIndicator)
   
    self.imgView.setImage(url: strUrl, style: .squared) { (result, error) in
      Helper.hideLoading(self.activityIndicator)
      if result == true {
      }else {
        Helper.showToast(_strMessage: error?.localizedDescription)
      }
    }
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
}
