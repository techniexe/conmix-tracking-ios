//
//  DrawSignatureVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class DrawSignatureVC: UIViewController {
  
  @IBOutlet weak var lblHeader: UILabel!
  @IBOutlet weak var imgUpload: UIImageView!
  @IBOutlet weak var btnDone: UIButton!
  @IBOutlet var lblTitleCollection: [UILabel]!
  @IBOutlet var lblPlaceholderCollection: [UILabel]!
  @IBOutlet var btnClearCollection: [UIButton]!
  @IBOutlet var signatureView: [YPDrawSignatureView]!
  
  var isForDropOrder: Bool = false
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var arrDocumentList: [UploadDocument] = []
  var isTM: Bool = false
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    self.isTM = Helper.isTransitMixer()
    
    self.lblHeader.text = Constants.Text.DrawSignature.localized()
    self.btnDone.setTitle(Constants.Text.Done.localized(), for: .normal)
    for i in 0..<self.btnClearCollection.count {
      self.btnClearCollection[i].setTitle(Constants.Text.Clear.localized(), for: .normal)
    }
    
    //self.btnDone.isUserInteractionEnabled = true
    //self.btnDone.alpha = 1.0
    self.btnDone.isHidden = true
    self.imgUpload.loadGif(asset: "ic_upload")
    self.imgUpload.isHidden = true
    
    for i in 0..<2 {
      self.signatureView[i].delegate = self
    }
    
    if isForDropOrder == true {
     
      let buyerTitle = Constants.Text.BuyerName.localized()
      
      if let buyerInfo = self.dicOrderDetails?.buyerInfo {
        
        if let fullName = buyerInfo.fullName, fullName != "" {
        
          self.lblTitleCollection[0].attributedText = Helper.setSignatureTitle(buyerTitle, subTitle: fullName)
        }
        
      }
      
      self.lblPlaceholderCollection[0].text = Constants.Text.DrawBuyerSign.localized()
      self.arrDocumentList.append(UploadDocument(title: "", img: nil, data: nil, name: "buyer_drop_signature", fileName: "buyerdrop.jpeg"))
      self.arrDocumentList.append(UploadDocument(title: "", img: nil, data: nil, name: "driver_drop_signature", fileName: "driverdrop.jpeg"))
      
    }else {
      
      let supplierTitle = Constants.Text.SupplierName.localized()
      
      if let vendorInfo = self.dicOrderDetails?.vendorInfo {
        
        if let fullName = vendorInfo.fullName, fullName != "" {
        
          self.lblTitleCollection[0].attributedText = Helper.setSignatureTitle(supplierTitle, subTitle: fullName)
        }
        
      }
      
      self.lblPlaceholderCollection[0].text = Constants.Text.DrawSupplierSign.localized()
      self.arrDocumentList.append(UploadDocument(title: "", img: nil, data: nil, name: "supplier_pickup_signature", fileName: "supplier.jpeg"))
      self.arrDocumentList.append(UploadDocument(title: "", img: nil, data: nil, name: "driver_pickup_signature", fileName: "driverpickup.jpeg"))
    }
    
    if self.isTM == true {
      
      let driverTitle = Constants.Text.DriverName.localized()
      
      if let driver1Info = self.dicOrderDetails?.driver1Info {
        
        if let driverName = driver1Info.driverName, driverName != "" {
        
          self.lblTitleCollection[1].attributedText = Helper.setSignatureTitle(driverTitle, subTitle: driverName)
        }
        
      }
      
      self.lblPlaceholderCollection[1].text = Constants.Text.DrawDriverSign.localized()
      
    }else {
      
      let operatorTitle = Constants.Text.OperatorName.localized()
      
      if let operatorInfo = self.dicOrderDetails?.operatorInfo {
        
        if let operatorName = operatorInfo.operatorName, operatorName != "" {
        
          self.lblTitleCollection[1].attributedText = Helper.setSignatureTitle(operatorTitle, subTitle: operatorName)
        }
        
      }
      
      self.lblPlaceholderCollection[1].text = Constants.Text.DrawOperatorSign.localized()
    }
    
    
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionDone(_ sender: Any) {
    
    if isForDropOrder == true {
      
      if self.arrDocumentList[0].img == nil {
        Helper.showToast(_strMessage: Constants.ErrorMessage.NoBuyerSign.localized())
      }else if self.arrDocumentList[1].img == nil {
        
        if self.isTM == true {
          
          Helper.showToast(_strMessage: Constants.ErrorMessage.NoDriverSign.localized())
          
        }else {
          
          Helper.showToast(_strMessage: Constants.ErrorMessage.NoOperatorSign.localized())
        }
        
        
      }else {
        self.btnDone.isHidden = true
        self.imgUpload.isHidden = false
        self.callUploadDocumentAPI()
      }
      
    }else {
      
      if self.isTM == true {
       
        if self.arrDocumentList[0].img == nil {
          Helper.showToast(_strMessage: Constants.ErrorMessage.NoSupplierSign.localized())
        }else if self.arrDocumentList[1].img == nil {
          
          Helper.showToast(_strMessage: Constants.ErrorMessage.NoDriverSign.localized())
          
        }else {
          self.btnDone.isHidden = true
          self.imgUpload.isHidden = false
          self.callUploadDocumentAPI()
        }
      }else {
        
        if self.arrDocumentList[1].img == nil {
          
          Helper.showToast(_strMessage: Constants.ErrorMessage.NoOperatorSign.localized())
          
        }else {
          self.btnDone.isHidden = true
          self.imgUpload.isHidden = false
          self.callUploadDocumentAPI()
        }
      }
      
    }
    
    
    
    //    if let img = self.signatureView[0].getSignature() {
    //
    //      uploadDocViewModel.documentList[0].img = img
    //
    //      // Saving signatureImage from the line above to the Photo Roll.
    //      // The first time you do this, the app asks for access to your pictures.
    //      //UIImageWriteToSavedPhotosAlbum(signatureImage, nil, nil, nil)
    //
    //      // Since the Signature is now saved to the Photo Roll, the View can be cleared anyway.
    //      //      var strData = ""
    //      //      let currentTimeStamp = String(Int(NSDate().timeIntervalSince1970))
    //      //      let formatter = DateFormatter()
    //      //      formatter.dateFormat = Constants.DateFormats.created
    //      //      let strDate = formatter.string(from:Date())
    //      //      let strImageName = "\(strDate)-\(currentTimeStamp)-\("App")"
    //      //      strData = signatureImage.storedFileIntoLocal(strImageName: strImageName)
    //      //      print(strData)
    //      //      self.signatureView[0].clear()
    //    }
    
    
    
  }
  
  @IBAction func actionClear(_ sender: Any) {
    let btn = sender as! UIButton
    self.signatureView[btn.tag].clear()
    self.arrDocumentList[btn.tag].img = nil
    self.arrDocumentList[btn.tag].data = nil
    self.lblPlaceholderCollection[btn.tag].isHidden = false
    self.btnClearCollection[btn.tag].isHidden = true
    
    if self.isTM == true {
      
      if let _ = self.arrDocumentList.findIndex(callback: {$0.data == nil}) {
        self.btnDone.isHidden = true
      }else {
        self.btnDone.isHidden = false
      }
      
    }else {
      
      if self.isForDropOrder == true {
        
        if let _ = self.arrDocumentList.findIndex(callback: {$0.data == nil}) {
          self.btnDone.isHidden = true
        }else {
          self.btnDone.isHidden = false
        }
        
      }else {
        
        if let _ = self.arrDocumentList.findIndex(callback: {$0.data != nil}) {
          self.btnDone.isHidden = false
        }else {
          self.btnDone.isHidden = true
        }
        
      }
      
      
    }
    
    
  }
  
  // MARK:- Call Upload Document API
  func callUploadDocumentAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      if self.isTM == true {
        
        APIHandler.sharedInstance.uploadDocument(self.dicOrderDetails?._id ?? "", strTMID: strVehicleID ?? "", strOrderID: self.dicOrderDetails?.vendorOrderInfo?._id ?? "", arrUploadDoc: self.arrDocumentList) { (statusCode, error) in
          
          
          if statusCode == 202 {
            //self.imgUpload.isHidden = true
            //self.btnDone.isHidden = false
            Helper.showToast(_strMessage: Constants.SuccessMessage.signuploadsuccess.localized())
            
            if self.isForDropOrder == true {
              
              self.imgUpload.isHidden = true
              self.btnDone.isHidden = false
              let storyBoard = UIStoryboard(name: "OrderDetailReceiptVC", bundle: nil)
              if let objOrderDetailReceiptVC = storyBoard.instantiateViewController(withIdentifier:"OrderDetailReceiptVC") as? OrderDetailReceiptVC {
                objOrderDetailReceiptVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                objOrderDetailReceiptVC.strTrackingID = self.dicOrderDetails?._id
                self.navigationController?.pushViewController(objOrderDetailReceiptVC, animated: true)
              }
              
            }else {
              
              
              if Reachability.isConnectedToNetwork() {
                
                APIHandler.sharedInstance.changeOrderStatus(strVehicleID ?? "", strOrderID: self.dicOrderDetails?.vendorOrderInfo?._id ?? "", strTrackID: self.dicOrderDetails?._id ?? "") { statusCode, error in
                  
                  if statusCode == 202 {
                    
                    if self.isForDropOrder == true {
                      
                      self.imgUpload.isHidden = true
                      self.btnDone.isHidden = false
                      let storyBoard = UIStoryboard(name: "OrderDetailReceiptVC", bundle: nil)
                      if let objOrderDetailReceiptVC = storyBoard.instantiateViewController(withIdentifier:"OrderDetailReceiptVC") as? OrderDetailReceiptVC {
                        objOrderDetailReceiptVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                        self.navigationController?.pushViewController(objOrderDetailReceiptVC, animated: true)
                      }
                      
                    }else {
                      
                      Helper.showToast(_strMessage: Constants.SuccessMessage.pickupstatus.localized())
                      
                      self.uploadInvoiceAfterPickup { (completed) in
                        
                        if completed == true {
                        
                          
                          self.uploadVendorInvoice { (completed) in
                            
                            if completed == true {
                              
                              self.imgUpload.isHidden = true
                              self.btnDone.isHidden = false
                              
                              let storyBoard = UIStoryboard(name: "VehicleDetailOrderVC", bundle: nil)
                              if let objVehicleDetailOrderVC = storyBoard.instantiateViewController(withIdentifier:"VehicleDetailOrderVC") as? VehicleDetailOrderVC {
                                objVehicleDetailOrderVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                                objVehicleDetailOrderVC.strTrackingID = self.dicOrderDetails?._id
                                self.navigationController?.pushViewController(objVehicleDetailOrderVC, animated: true)
                              }
                              
                            }
                            
                          }
                          
                        }
                        
                      }
                      
                    }
                    
                  } else {
                    self.imgUpload.isHidden = true
                    self.btnDone.isHidden = false
                    Helper.showToast(_strMessage: error)
                    return
                  }
                  
                }
                
                
              }else {
                GlobalFunction.noNetworkConnection()
              }
              
            }
            
          } else {
            self.imgUpload.isHidden = true
            self.btnDone.isHidden = false
            Helper.showToast(_strMessage: error)
            return
          }
          
        }
      }else {
        
        
        if let strTrackingID = self.dicOrderDetails?._id ,let strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id, let strCPID = self.dicOrderDetails?.CPInfo?._id {
          
          var arrList: [UploadDocument] = []
          
          for (index, element) in self.arrDocumentList.enumerated() {
            arrList.insert(element, at: index)
          }
          
          if let index = arrList.findIndex(callback: {$0.data == nil}) {
            arrList.remove(at: index)
          }
          
          APIHandler.sharedInstance.uploadDocumentForCP(strTrackingID, strCPID: strCPID, strOrderID: strOrderID, arrUploadDoc: arrList) { (statusCode, error) in
            
            
            if statusCode == 202 {
              
              self.imgUpload.isHidden = true
              self.btnDone.isHidden = false
              Helper.showToast(_strMessage: Constants.SuccessMessage.signuploadsuccess.localized())
              
              if self.isForDropOrder == true {
                
                let storyBoard = UIStoryboard(name: "OrderDetailReceiptVC", bundle: nil)
                if let objOrderDetailReceiptVC = storyBoard.instantiateViewController(withIdentifier:"OrderDetailReceiptVC") as? OrderDetailReceiptVC {
                  objOrderDetailReceiptVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                  objOrderDetailReceiptVC.strTrackingID = self.dicOrderDetails?._id
                  self.navigationController?.pushViewController(objOrderDetailReceiptVC, animated: true)
                }
                
              }else {
                
                
                if Reachability.isConnectedToNetwork() {
                  
                  APIHandler.sharedInstance.changeOrderStatusForCP(strCPID, strOrderID: strOrderID, strTrackID: strTrackingID) { statusCode, error in
                    
                    if statusCode == 202 {
                      
                      if self.isForDropOrder == true {
                        
                        let storyBoard = UIStoryboard(name: "OrderDetailReceiptVC", bundle: nil)
                        if let objOrderDetailReceiptVC = storyBoard.instantiateViewController(withIdentifier:"OrderDetailReceiptVC") as? OrderDetailReceiptVC {
                          objOrderDetailReceiptVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                          self.navigationController?.pushViewController(objOrderDetailReceiptVC, animated: true)
                        }
                        
                      }else {
                        
                        Helper.showToast(_strMessage: Constants.SuccessMessage.pickupstatus.localized())
                        
                        let storyBoard = UIStoryboard(name: "VehicleDetailOrderVC", bundle: nil)
                        if let objVehicleDetailOrderVC = storyBoard.instantiateViewController(withIdentifier:"VehicleDetailOrderVC") as? VehicleDetailOrderVC {
                          objVehicleDetailOrderVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                          objVehicleDetailOrderVC.strTrackingID = self.dicOrderDetails?._id
                          self.navigationController?.pushViewController(objVehicleDetailOrderVC, animated: true)
                        }
                      }
                      
                    } else {
                      Helper.showToast(_strMessage: error)
                      return
                    }
                    
                  }
                  
                  
                }else {
                  GlobalFunction.noNetworkConnection()
                }
                
              }
              
            } else {
              self.imgUpload.isHidden = true
              self.btnDone.isHidden = false
              Helper.showToast(_strMessage: error)
              return
            }
            
          }
          
        }
        
      }
    }
    else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
}

// MARK: - YPSignatureDelegate
extension DrawSignatureVC: YPSignatureDelegate {
  
  func didStart(_ view: YPDrawSignatureView) {
    print(view.tag)
    self.lblPlaceholderCollection[view.tag].isHidden = true
    //print("Started Drawing")
  }
  
  func didFinish(_ view: YPDrawSignatureView) {
    //print("Finished Drawing")
    
    //print(view.tag)
    
    if let img = self.signatureView[view.tag].getSignature() {
      
      self.arrDocumentList[view.tag].img = img
      let imgData = img.jpegData(compressionQuality: 0.6)
      self.arrDocumentList[view.tag].data = imgData
      self.btnClearCollection[view.tag].isHidden = false
    
      if self.isTM == true {
        
        if let _ = self.arrDocumentList.findIndex(callback: {$0.data == nil}) {
        }else {
          self.btnDone.isHidden = false
        }
        
      }else {
        
        if self.isForDropOrder == true {
          
          if let _ = self.arrDocumentList.findIndex(callback: {$0.data == nil}) {
          }else {
            self.btnDone.isHidden = false
          }
          
        }else {
          
          if let _ = self.arrDocumentList.findIndex(callback: {$0.data != nil}) {
            self.btnDone.isHidden = false
          }else {
            //self.btnDone.isHidden = true
          }
          
        }
        
        
      }
      
      
      
    }
  }
  
}


extension DrawSignatureVC {
  
  //MARK:- Upload Buyer Invoice
  func uploadInvoiceAfterPickup(_ completion: @escaping (Bool) -> Void) {
    
  
    self.disableUserInteraction()
  
    if let orderID = self.dicOrderDetails?.vendorOrderInfo?._id, let strTMID = strVehicleID, let trackingID = self.dicOrderDetails?._id   {
      
     
      
      var bodyParams = [String:Any]()
      
      bodyParams["TM_id"] = strTMID
      bodyParams["order_id"] = orderID
      bodyParams["track_id"] = trackingID
    
      print("Request For Upload Buyer Invoice :: \(Date())")
      APIHandler.sharedInstance.uploadInvoice(bodyParams) { (statuscode, error) in
        
        print("Response For Upload Buyer Invoice :: \(Date())")
        
        if statuscode == 200 {
          
          completion(true)
          
        }else {
          self.imgUpload.isHidden = true
          self.btnDone.isHidden = false
          Helper.showToast(_strMessage: error)
        }
        
      }
    }
    
  }
  
  //MARK:- Upload Vendor Invoice
  func uploadVendorInvoice(_ completion: @escaping (Bool) -> Void) {
    
    self.disableUserInteraction()
  
    
    
    if let orderID = self.dicOrderDetails?.vendorOrderInfo?._id, let strTMID = strVehicleID, let trackingID = self.dicOrderDetails?._id   {
      
      var bodyParams = [String:Any]()
      
      bodyParams["TM_id"] = strTMID
      bodyParams["order_id"] = orderID
      bodyParams["track_id"] = trackingID
    
      print("Request For Upload Vendor Invoice :: \(Date())")
      
      APIHandler.sharedInstance.uploadVendorInvoice(bodyParams) { (statuscode, error) in
        
        print("Response For Upload Vendor Invoice :: \(Date())")
        
        self.enableUserInteraction()
        
        //Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 200 {
          
          completion(true)
          
        }else {
          self.imgUpload.isHidden = true
          self.btnDone.isHidden = false
          Helper.showToast(_strMessage: error)
        }
        
      }
    }
    
  }
}
