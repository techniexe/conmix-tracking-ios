//
//  LoginVC.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 10/05/21.
//

import UIKit
import Network

class LoginVC: UIViewController {
  
  //MARK:- @IBOutlet
  @IBOutlet weak var txtMobileNo: FloatingTextField!
  @IBOutlet weak var txtPassword: FloatingTextField!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var lblHeder: UILabel!
  @IBOutlet weak var btnLogin: UIButton!
  
  //MARK:- @vars
  var strMobileorEmail: String! = ""
  
  
  
  //MARK:- View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  //MARK:- setupView
  func setupView() {
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    setText()
  }
  
  // MARK:- Set Text
  func setText() {
    //self.lblHeder.text = Constants.Text.Welcome.localized()
    self.txtMobileNo.placeholder = Constants.Text.Mobile.localized()
    self.txtPassword.placeholder = Constants.Text.Password.localized()
    self.btnLogin.setTitle(Constants.Text.Login.localized(), for: .normal)
  }

  //MARK:- @IBActions
  @IBAction func actionPwdOnOff(_ sender: UIButton) {
    
    sender.isSelected = !sender.isSelected
    
    if sender.isSelected {
      self.txtPassword.isSecureTextEntry = false
      
    }else {
      self.txtPassword.isSecureTextEntry = true
    }
  }
  
  @IBAction func actionLogin(_ sender: UIButton) {
    
    self.view.endEditing(true)
    
    if checkValidation() {
      
      
      LocalNetworkAuthorization.init().requestAuthorization { (completed) in
        
        self.disableUserInteraction()
        Helper.showLoading(self.activityIndicator)
        
        if completed == true {
          
          if Reachability.isConnectedToNetwork() == true {
            
            Helper.isValidPhoneNumber(self.txtMobileNo.text?.trim() ?? "") { (isValid, mobileorEmail, loginType) in
              
              if isValid == true {
                
                self.strMobileorEmail = mobileorEmail
                
                if loginType == .mobile {
                  self.callloginAPI("Mobile")
                }else {
                  self.callloginAPI("Email")
                }
                
              }else {
                return
              }
              
            }
            
          }else {
            
            self.enableUserInteraction()
            Helper.hideLoading(self.activityIndicator)
            GlobalFunction.noNetworkConnection()
          }
          
        }else {
          self.enableUserInteraction()
          Helper.hideLoading(self.activityIndicator)
          GlobalFunction.noNetworkConnection()
        }
        
      }
      
      /*Helper.isValidPhoneNumber(self.txtMobileNo.text?.trim() ?? "") { (isValid, mobileorEmail, loginType) in
       
       if isValid == true {
       
       self.strMobileorEmail = mobileorEmail
       
       if loginType == .mobile {
       self.callloginAPI("Mobile")
       }else {
       self.callloginAPI("Email")
       }
       
       }else {
       return
       }
       
       }*/
      
      
      
    }
  }
  
  
  
  //MARK:- Call Login API
  func callloginAPI(_ type: String) {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.sharedInstance.login(self.strMobileorEmail, strPassword: self.txtPassword.text?.trim() ?? "") { (statuscode, response, error) in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 200 {
          
          if let dataResponse = response?.data {
            
            if let customToken = dataResponse.customToken {
              
              UserDefaults.standard.set(true, forKey:Constants.UserDefaults.AlreadyLogIn)
              UserDefaults.standard.set(type, forKey:Constants.UserDefaults.UserType)
              UserDefaults.standard.set(customToken, forKey:Constants.UserDefaults.CustomToken)
              UserDefaults.standard.synchronize()
              
              GlobalFunction.setRootViewController(direction: UIWindow.TransitionOptions.Direction.toRight)
            }
            
          }
          
        }else {
          if error == "" {
            Helper.showToast(_strMessage: Constants.ErrorMessage.invalidcred.localized())
          }else {
            Helper.showToast(_strMessage: error)
          }
        }
        
      }
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
  }
  
  
  //MARK:- Check Validation
  func checkValidation() -> Bool {
    
    if Helper.isEmpty(strText: self.txtMobileNo.text ?? "") {
    
      Helper.showToast(_strMessage: Constants.ErrorMessage.mobileemail.localized())
      return false
    
    }else if Helper.isEmpty(strText: self.txtPassword.text ?? "") {
      
      Helper.showToast(_strMessage: Constants.ErrorMessage.PasswordBlank.localized())
      return false
      
    }else if (self.txtPassword.text?.trim().count)! < MinLength.password.rawValue {
      
      Helper.showToast(_strMessage: Constants.ErrorMessage.passwordlength.localized())
      return false
      
    }
    
    return true
  }
  
}


//MARK:- UITextFieldDelegate
extension LoginVC : UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {

  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    
    if range.location == 0 && string == " " {
      return false
    }
    
    let currentString: NSString = textField.text! as NSString
    let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
    
    if textField.tag == 1 {
      return newString.length <= MaxLength.password.rawValue
    }
    
    return true
  }
  
 
}

@available(iOS 14.0, *)
public class LocalNetworkAuthorization: NSObject {
    private var browser: NWBrowser?
    private var netService: NetService?
    private var completion: ((Bool) -> Void)?
    
    public func requestAuthorization(completion: @escaping (Bool) -> Void) {
        self.completion = completion
        
        // Create parameters, and allow browsing over peer-to-peer link.
        let parameters = NWParameters()
        parameters.includePeerToPeer = true
        
        // Browse for a custom service type.
        let browser = NWBrowser(for: .bonjour(type: "_bonjour._tcp", domain: nil), using: parameters)
        self.browser = browser
        browser.stateUpdateHandler = { newState in
            switch newState {
            case .failed(let error):
                print(error.localizedDescription)
            case .ready, .cancelled:
                break
            case let .waiting(error):
                print("Local network permission has been denied: \(error)")
                self.reset()
                self.completion?(false)
            default:
                break
            }
        }
        
        self.netService = NetService(domain: "local.", type:"_lnp._tcp.", name: "LocalNetworkPrivacy", port: 8083)
        self.netService?.delegate = self
        
        self.browser?.start(queue: .main)
        self.netService?.publish()
    }
    
    private func reset() {
        self.browser?.cancel()
        self.browser = nil
        self.netService?.stop()
        self.netService = nil
    }
}

@available(iOS 14.0, *)
extension LocalNetworkAuthorization : NetServiceDelegate {
    public func netServiceDidPublish(_ sender: NetService) {
        self.reset()
        print("Local network permission has been granted")
        completion?(true)
    }
}
