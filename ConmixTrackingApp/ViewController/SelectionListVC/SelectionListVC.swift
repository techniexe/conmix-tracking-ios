//
//  SelectionListVC.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 27/07/21.
//

import UIKit

class SelectionListVC: UIViewController {
  
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblSubTitle: UILabel!
  @IBOutlet weak var btnTM: UIButton!
  @IBOutlet weak var lblOR: UILabel!
  @IBOutlet weak var btnCP: UIButton!
  @IBOutlet weak var btnOption: UIButton!
  @IBOutlet weak var lblTM: UILabel!
  @IBOutlet weak var lblCP: UILabel!
  
  //MARK:- @lets
  let manager = PopMenuManager.default
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setText()
    createPopupMenu()
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
  }
  
  // Remove the LCLLanguageChangeNotification on viewWillDisappear
  override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
  }
  
  
  // MARK: Localized Text
  @objc func setText(){
    
    self.lblTitle.text = Constants.Text.ChooseTracking.localized()
    //self.lblSubTitle.text = Constants.Text.ChooseDeliveryFor.localized()
    //self.btnTM.setTitle(Constants.Text.TransitMixer.localized(), for: .normal)
    //self.lblOR.text = Constants.Text.OR.localized()
    //self.btnCP.setTitle(Constants.Text.ConcretePump.localized(), for: .normal)
    self.lblTM.text = Constants.Text.TransitMixer.localized()
    self.lblCP.text = Constants.Text.ConcretePump.localized()
   
  }
  
  // MARK: - Create Popup Menu
  func createPopupMenu() {
  
    // Customize appearance
    manager.popMenuAppearance.popMenuFont = UIFont.systemFont(ofSize: 16.0) //UIFont(name: "AvenirNext-DemiBold", size: 16)!
    manager.popMenuAppearance.popMenuBackgroundStyle = .none()
    manager.popMenuShouldDismissOnSelection = false
    manager.popMenuDelegate = self
  }
  
  
  @IBAction func actionHeaderbtn(_ sender: Any) {

    let arrOptionList = [Constants.Text.SelectLanguage.localized()]
    manager.actions.removeAll()
    
    for i in 0..<arrOptionList.count {
      
      let popMenuDefaultAction = PopMenuDefaultAction(title: arrOptionList[i], image: nil, color: nil)
      manager.actions.insert(popMenuDefaultAction, at: i)
    }
    
    manager.present(sourceView: self.btnOption)
  }
  
  // MARK: - Open Select Language Popup
  func openSelectLanguagePopup() {
    let selectLanguageVC = SelectLanguageVC.init(nibName: "SelectLanguageVC", bundle: nil)
    selectLanguageVC.modalTransitionStyle = .crossDissolve
    selectLanguageVC.modalPresentationStyle = .overCurrentContext
    self.parent?.present(selectLanguageVC, animated: true, completion: nil)
  }
  
  // MARK: - Open Select Language Popup
  func openLogoutPopup() {
    CommonAlertVC.showPopup(parentVC: self, options: .logout)
  }
  
  
  @IBAction func actionButton(_ sender: Any) {
    
    let btn = sender as! UIButton
    
    switch btn.tag {
    case 0:
      self.openScreen(true)
    case 1:
      self.openScreen(false)
    default:
      Helper.DLog(message: "")
    }
    
  }
  
  
  func openScreen(_ isTransitMixer: Bool){
    if let objTMListVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"TransitMixerListVC") as? TransitMixerListVC {
      
      if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.OptionForDelivery) == true {
        UserDefaults.standard.removeObject(forKey: Constants.UserDefaults.OptionForDelivery)
      }
      
      UserDefaults.standard.set(isTransitMixer, forKey:Constants.UserDefaults.OptionForDelivery)
      UserDefaults.standard.synchronize()
      self.navigationController?.pushViewController(objTMListVC, animated: true)
    }
  }
  
}

//MARK:- PopMenuViewControllerDelegate
extension SelectionListVC: PopMenuViewControllerDelegate {
  
  func popMenuDidSelectItem(_ popMenuViewController: PopMenuViewController, at index: Int) {
    
    popMenuViewController.dismiss(animated: true, completion: nil)
    print(index)
    switch index {
    case 0:
      openSelectLanguagePopup()//openLogoutPopup()
    case 1:
      openSelectLanguagePopup()
    default:
      break
    }
    
    
    
  }
  
}
