//
//  UploadDocumentVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import MobileCoreServices

class UploadDocumentVC: UIViewController {
  
  @IBOutlet weak var imgUpload: UIImageView!
  @IBOutlet weak var btnDone: UIButton!
  @IBOutlet weak var lblHeader: UILabel!
  @IBOutlet weak var tblView: UITableView!

  var tapIndex: Int = 0
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var arrDocumentList: [UploadDocument] = []
  var isTM: Bool = false
  //UploadDocument(title: Constants.Text.UploadRoyaltyPass.localized(), img: nil, data: nil, name: "royalty_pass_image", fileName: "royaltypass.jpeg"), UploadDocument(title: Constants.Text.UploadEWayBill.localized(), img: nil, data: nil, name: "way_slip_image", fileName: "wayslip.jpeg"),
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    //self.btnDone.isUserInteractionEnabled = true
   // self.btnDone.alpha = 1.0
   // self.btnDone.isHidden = false
  
    self.isTM = Helper.isTransitMixer()
    self.lblHeader.text = Constants.Text.UploadDocument.localized()
    self.btnDone.setTitle(Constants.Text.Done.localized(), for: .normal)
    self.imgUpload.loadGif(asset: "ic_upload")
    self.imgUpload.isHidden = true
    
    if self.isTM == true {
      
      arrDocumentList = [UploadDocument(title: Constants.Text.UploadChallan.localized(), img: nil, data: nil, name: "challan_image", fileName: "challan.jpeg")] //UploadDocument(title: Constants.Text.UploadInvoice.localized(), img: nil, data: nil, name: "invoice_image", fileName: "invoice.jpeg")
      
    }else {
     
      arrDocumentList = [UploadDocument(title: Constants.Text.UploadChallan.localized(), img: nil, data: nil, name: "challan_image", fileName: "challan.jpeg")]
    }
    
    self.tblView.register(UINib(nibName: "UploadDocumentListCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.uploadDocumentListCellID)
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.reloadData()
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionDone(_ sender: Any) {
    
    if self.isTM == true {
      
      if self.arrDocumentList[0].img == nil {
        Helper.showToast(_strMessage: Constants.ErrorMessage.noChallan.localized())
      }/*else if self.arrDocumentList[1].img == nil {
        Helper.showToast(_strMessage: Constants.ErrorMessage.noInvoice.localized())
      }*/else {
        self.btnDone.isHidden = true
        self.imgUpload.isHidden = false
        for i in 0..<self.arrDocumentList.count {
          
          if let image = self.arrDocumentList[i].img {
            let imgData = image.jpegData(compressionQuality: 0.6)
            self.arrDocumentList[i].data = imgData
          }
        }
        self.callUploadDocumentAPI()
      }
      
    }else {
      
      if self.arrDocumentList[0].img == nil {
        Helper.showToast(_strMessage: Constants.ErrorMessage.noChallan.localized())
      }else {
        self.btnDone.isHidden = true
        self.imgUpload.isHidden = false
        for i in 0..<self.arrDocumentList.count {
          
          if let image = self.arrDocumentList[i].img {
            let imgData = image.jpegData(compressionQuality: 0.6)
            self.arrDocumentList[i].data = imgData
          }
        }
        self.callUploadDocumentAPI()
      }
      
    }
    
   
    
    /*else if self.arrDocumentList[1].img == nil {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noroyaltypass.localized())
    }else if self.arrDocumentList[2].img == nil {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noewaybill.localized())
    }*/
  }
  
  // MARK: - Allow Camera Permission
  func allowCameraPermission() {
      
      let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
    
    let takePhotoAction = UIAlertAction(title: Constants.Text.TakePoto.localized(), style: .default, handler: {
          (alert: UIAlertAction!) -> Void in
          
          Helper.cameraAuthorizationStatus({ (result) in
              
              if result {
                  DispatchQueue.main.async {
                      if UIImagePickerController.isSourceTypeAvailable(.camera) {
                          let imagePicker = UIImagePickerController()
                          imagePicker.delegate = self
                          imagePicker.sourceType = .camera
                          imagePicker.allowsEditing = true
                          self.present(imagePicker, animated: true, completion: nil)
                      }else {
                         
                        Helper.showToast(_strMessage: Constants.Toast.noCameraAvailable.localized())
                      }
                  }
                  
              }else {
                  DispatchQueue.main.async {
                    let alert = UIAlertController(title: Constants.Toast.cameraAccessTitle.localized(), message: Constants.Toast.cameraAccessMessage.localized(), preferredStyle: .alert)
                      
                    alert.addAction(UIAlertAction(title: Constants.Toast.enableCameraAccess.localized(), style: .default) { action in
                          UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                      })
                      
                      alertController.pruneNegativeWidthConstraints()
                      self.present(alert, animated: true, completion: nil)
                  }
              }
          })
      })
      
      takePhotoAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
      
    let cancelAction = UIAlertAction(title: Constants.Toast.cancel.localized(), style: .cancel, handler: {
          (alert: UIAlertAction!) -> Void in
      })
      
      cancelAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
      alertController.addAction(takePhotoAction)
      alertController.addAction(cancelAction)
      if #available(iOS 13.0, *) {
        alertController.overrideUserInterfaceStyle = .light
      } else {
        // Fallback on earlier versions
      }
      alertController.pruneNegativeWidthConstraints()
      self.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: - Call Upload Document API
  func callUploadDocumentAPI() {
    
    if Reachability.isConnectedToNetwork() {
    
      if self.isTM == true {
        
        APIHandler.sharedInstance.uploadDocument(self.dicOrderDetails?._id ?? "", strTMID: strVehicleID ?? "", strOrderID: self.dicOrderDetails?.vendorOrderInfo?._id ?? "", arrUploadDoc: self.arrDocumentList) { (statusCode, error) in
          
          if statusCode == 202 {
            self.imgUpload.isHidden = true
            self.btnDone.isHidden = false
            Helper.showToast(_strMessage: Constants.SuccessMessage.docuploadsuccess.localized())
            let storyBoard = UIStoryboard(name: "DrawSignatureVC", bundle: nil)
            if let objDrawSignatureVC = storyBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
              objDrawSignatureVC.dicOrderDetails = self.dicOrderDetails
              self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
            }
            
          } else {
            self.imgUpload.isHidden = true
            self.btnDone.isHidden = false
            Helper.showToast(_strMessage: error)
            return
          }
          
        }
        
      }else {
        
        let strCPID =  Helper.getCPID()
        
        if let strTrackingID = self.dicOrderDetails?._id, let strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id {
        
          APIHandler.sharedInstance.uploadDocumentForCP(strTrackingID, strCPID: strCPID, strOrderID: strOrderID, arrUploadDoc: self.arrDocumentList) { (statusCode, error) in
            
            
            if statusCode == 202 {
              self.imgUpload.isHidden = true
              self.btnDone.isHidden = false
              Helper.showToast(_strMessage: Constants.SuccessMessage.docuploadsuccess.localized())
              let storyBoard = UIStoryboard(name: "DrawSignatureVC", bundle: nil)
              if let objDrawSignatureVC = storyBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
                objDrawSignatureVC.dicOrderDetails = self.dicOrderDetails
                self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
              }
              
            } else {
              self.imgUpload.isHidden = true
              self.btnDone.isHidden = false
              Helper.showToast(_strMessage: error)
              return
            }
          }
        }
        
      }
      
    }else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
    
  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension UploadDocumentVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.arrDocumentList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.uploadDocumentListCellID, for: indexPath) as? UploadDocumentListCell else {
      return UITableViewCell()
    }
    
    if indexPath.row == self.arrDocumentList.count - 1 {
      objCell.lblLine.isHidden = true
    }
    
    objCell.configureCell(self.arrDocumentList[indexPath.row])
    
    return objCell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //Helper.DLog(message: "section : \(indexPath.section) row: \(indexPath.row)")
    tapIndex = indexPath.row
    self.allowCameraPermission()
    
    /*if let objDrawSignatureVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
      self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
    }*/
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let header = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: 40.0))
    header.backgroundColor = UIColor.white
  
    
    let label = UILabel(frame: CGRect(x: 15.0, y: 15, width: header.bounds.size.width - 30, height: header.bounds.size.height-15))
    label.text = Constants.Text.Document.localized()
    label.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
    label.backgroundColor = UIColor.clear
    label.textColor = UIColor.black
    header.addSubview(label)
  
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }
  
}

 
// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension UploadDocumentVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK::
    //MARK:: Image Picker Delegate Method
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
  
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//
//       guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String else { return }
//
//         if mediaType == kUTTypeImage as String {
//
//           if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//             self.arrUploadDocList[tapIndex].img = pickedImage
//             dismiss(animated: true, completion: nil)
//             self.tblView.reloadData()
//           }
//
//         }
//     }
    
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    // Local variable inserted by Swift 4.2 migrator.
    let info = Helper.convertFromUIImagePickerControllerInfoKeyDictionary(info)
    let image = info[Helper.convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as! UIImage
    self.arrDocumentList[tapIndex].img = image
    
    
    if let index = self.arrDocumentList.findIndex(callback: {$0.img == nil}) {
      Helper.DLog(message: "\(index)")
    }else {
      self.btnDone.isHidden = false
    }
    
    dismiss(animated: true, completion: nil)
    
    self.tblView.reloadData()
    
    dismiss(animated: true, completion: nil)
  }
}

