//
//  OrderDetailReceiptVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class OrderDetailReceiptVC: UIViewController {
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet weak var lblOrderDate: UILabel!
  @IBOutlet weak var btnDone: UIButton!
  @IBOutlet weak var tblView: UITableView!
  
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var strOrderID: String? = nil
  var strTrackingID: String? = nil
  var isTM: Bool = false
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  
  // MARK: - setupView
  func setupView() {
    
    self.isTM = Helper.isTransitMixer()
    
    self.btnDone.setTitle(Constants.Text.Done.localized(), for: .normal)
    
    if self.isTM == true {
      
      self.tblView.register(UINib(nibName: "OrderDetailCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.orderDetailCellID)
      
    }else {
      self.tblView.register(UINib(nibName: "CPOrderDetailReceiptCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.CPOrderDetailReceiptCellID)
      
    }
    
    
    self.callGetOrderDetailsAPI()
  }
  
  // MARK: - Call Get Order API
  func callGetOrderDetailsAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
     
      if self.isTM == true {
        
        APIHandler.sharedInstance.getOrderDetails(strOrderID ?? "", strTrackID: strTrackingID ?? "") { statusCode, response, error in
          
          //Helper.hideLoading(self.activityIndicator)
          
          if statusCode == 200 {
            
            if let orderResponse  = response?.order {
              
              self.dicOrderDetails = orderResponse
              
            }
            
          }else {
            Helper.hideLoading(self.activityIndicator)
            Helper.showToast(_strMessage: error)
          }
          DispatchQueue.main.async {
            self.updateUI()
          }
        }
      }else {
        
        if let orderID = strOrderID, let trackingID = strTrackingID  {
         
          APIHandler.sharedInstance.getCPOrderDetails(orderID, strTrackID: trackingID) { (statusCode, response, error) in
            
            Helper.hideLoading(self.activityIndicator)
            
            if statusCode == 200 {
              
              if let orderResponse  = response?.order {
                
                self.dicOrderDetails = orderResponse
                
              }
              
            }else {
              Helper.showToast(_strMessage: error)
            }
            DispatchQueue.main.async {
              self.updateUI()
            }
            
          }
          
        }
        
      }
    }
    else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
  // MARK: - Call Order Delivered API
  func callOrderDeliveredAPI() {
    
    if self.isTM == true {
      
      APIHandler.sharedInstance.deliverOrder(strVehicleID ?? "", strTrackingID: self.dicOrderDetails?._id ?? "", strOrderID: strOrderID ?? "") { statusCode, error in
        
        if statusCode == 202 {
          
          Helper.showToast(_strMessage: Constants.SuccessMessage.orderdeliver.localized())
          
          //self.uploadInvoice()
          
        } else {
          Helper.showToast(_strMessage: error)
          return
        }
      }
    }else {
      
      if let orderID = strOrderID, let strCPID = self.dicOrderDetails?.CPInfo?._id, let trackingID = self.dicOrderDetails?._id   {
        
        APIHandler.sharedInstance.deliverOrderForCP(strCPID, strTrackingID: trackingID, strOrderID: orderID) { (statusCode, error) in
          
          if statusCode == 202 {
            
            Helper.showToast(_strMessage: Constants.SuccessMessage.orderdeliver.localized())
            
          } else {
            Helper.showToast(_strMessage: error)
            return
          }
        }
      }
      
    }
  }
  
  //MARK:- Upload Invoice
  func uploadInvoice() {
    
    self.disableUserInteraction()
    
    if let orderID = strOrderID, let strTMID = strVehicleID, let trackingID = self.dicOrderDetails?._id   {
      
      var bodyParams = [String:Any]()
      
      bodyParams["TM_id"] = strTMID
      bodyParams["order_id"] = orderID
      bodyParams["track_id"] = trackingID
      
      APIHandler.sharedInstance.uploadInvoice(bodyParams) { (statuscode, error) in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 200 {
          
        }else {
          //Helper.showToast(_strMessage: error)
        }
        
      }
    }
    
  }
  
  // MARK: - Update UI
  func updateUI() {
    
    guard self.dicOrderDetails != nil else {
      Helper.hideLoading(self.activityIndicator)
      let storyBoard = Helper.getStoryBorad()
      if let objNoDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC {
        objNoDataVC.view.frame = CGRect(x: 0.0, y: 44.0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 44)
        objNoDataVC.lblTitle.text = Constants.Text.NoOrderReceipt.localized()
        self.contentView.addSubview(objNoDataVC.view)
      }
      return
    }
    
    if let orderID = self.dicOrderDetails?.vendorOrderInfo?._id {
      let strOrderID = Constants.Text.ORDERID.localized()
      self.lblOrderID.text = String(format: "\(strOrderID) #%@", orderID)
    }
    
    if let createdAt = self.dicOrderDetails?.createdAt {
      self.lblOrderDate.text = Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: Constants.DateFormats.deliveryDate)
    }
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.isHidden = false
    self.tblView.reloadData()
    
    callOrderDeliveredAPI()
  }
  
  @IBAction func actionBack(_ sender: Any) {
    
    //callOrderDeliveredAPI()
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  @IBAction func actionDone(_ sender: Any) {
    
    //self.callOrderDeliveredAPI()
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  //MARK:- Action On Challan For CP
  @objc func actionChallanForCP(_ sender: UIButton) {
    let storyBoard = UIStoryboard(name: "GalleryVC", bundle: nil)
    if let objGalleryVC = storyBoard.instantiateViewController(withIdentifier:"GalleryVC") as? GalleryVC {
      objGalleryVC.strUrl = self.dicOrderDetails?.challanImageUrl ?? ""
      self.navigationController?.pushViewController(objGalleryVC, animated: true)
    }
    
  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension OrderDetailReceiptVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if self.isTM == true {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.orderDetailCellID, for: indexPath) as? OrderDetailCell else {
        return UITableViewCell()
      }
      
      objCell.delegate = self
      
      if let orderDetail = self.dicOrderDetails {
        objCell.configureCell(orderDetail)
      }
      
      return objCell
    
    }else {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.CPOrderDetailReceiptCellID, for: indexPath) as? CPOrderDetailReceiptCell else {
        return UITableViewCell()
      }
      
      
      objCell.btnChallan.addTarget(self, action: #selector(self.actionChallanForCP(_:)), for: .touchUpInside)
      
      if let orderDetail = self.dicOrderDetails {
        objCell.configureCell(orderDetail, isTransitMixer: isTM)
      }
      
      return objCell
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     Helper.DLog(message: "section : \(indexPath.section) row: \(indexPath.row)")
   }
}

// MARK: - OrderDocumentDelegate
extension OrderDetailReceiptVC: OrderDocumentDelegate {
  
  func documentUrl(_ strURL: String, index: Int) {
   
    if index == 5 {
      
      let storyBoard = UIStoryboard(name: "InvoiceVC", bundle: nil)
      if let objInvoiceVCVC = storyBoard.instantiateViewController(withIdentifier:"InvoiceVC") as? InvoiceVC {
        objInvoiceVCVC.strInvoice = strURL
        self.navigationController?.pushViewController(objInvoiceVCVC, animated: true)
      }
      
    }else {
      
      let storyBoard = UIStoryboard(name: "GalleryVC", bundle: nil)
      if let objGalleryVC = storyBoard.instantiateViewController(withIdentifier:"GalleryVC") as? GalleryVC {
        objGalleryVC.strUrl = strURL
        self.navigationController?.pushViewController(objGalleryVC, animated: true)
      }
    }
  }
}
