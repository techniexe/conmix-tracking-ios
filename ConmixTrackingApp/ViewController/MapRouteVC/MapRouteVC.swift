//
//  MapRouteVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 24/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class MapRouteVC: UIViewController, GMSMapViewDelegate {
  
  @IBOutlet weak var googleView: GMSMapView!

  
  var fromScreen: FromScreen = .truckLoadOrder
  var dicPickUpLocation: Location?
  var dicPickupAddInfo: PickUpAddressInfo?
  var dicDeliveryAddInfo: DeliveryAddressInfo?
  
   // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView(){
    
    
    if fromScreen == .truckdetails {
      
      
      /*let camera = GMSCameraPosition.camera(withLatitude: (dicPickUpLocation?.coordinates?[1])!,
                                            longitude: (dicPickUpLocation?.coordinates?[0])!,
                                            zoom: 20.0,
                                            bearing: 30,
                                            viewingAngle: 40)*/
      
      
      if let coordinates = dicPickUpLocation?.coordinates {
        
        CATransaction.begin()
        CATransaction.setValue(1.0, forKey: kCATransactionAnimationDuration)
        let camera = GMSCameraPosition.camera(withTarget:  CLLocationCoordinate2D(latitude: coordinates[1], longitude: coordinates[0]), zoom: 15)
        
        
        self.googleView.camera = camera
        self.googleView.animate(to: camera)
        CATransaction.commit()
        
        self.googleView.delegate = self
        self.googleView?.isMyLocationEnabled = true
        self.googleView.settings.myLocationButton = true
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude:  coordinates[1] , longitude:  coordinates[0])
        marker.map = googleView
      }
      
      
      
      
      //Setting the googleView
      
//      self.googleView.delegate = self
//      self.googleView?.isMyLocationEnabled = true
//      self.googleView.settings.myLocationButton = true
      //self.googleView.settings.compassButton = true
      //self.googleView.settings.zoomGestures = true
      //self.googleView.animate(to: camera)
      
      // Creates a marker in the center of the map.
      
      
      
    }else {
      
      // Create a GMSCameraPosition that tells the map to display the
      
      let camera = GMSCameraPosition.camera(withLatitude: (dicPickupAddInfo?.pickupLocation?.coordinates?[1])!,
                                            longitude: (dicPickupAddInfo?.pickupLocation?.coordinates?[0])!,
                                            zoom: 20.0,
                                            bearing: 30,
                                            viewingAngle: 40)
      //Setting the googleView
      self.googleView.camera = camera
      self.googleView.delegate = self
      self.googleView?.isMyLocationEnabled = true
      self.googleView.settings.myLocationButton = true
      self.googleView.settings.compassButton = true
      self.googleView.settings.zoomGestures = true
      self.googleView.animate(to: camera)
      
      //Setting the start and end location
      let origin = "\((dicPickupAddInfo?.pickupLocation?.coordinates?[1])!),\((dicPickupAddInfo?.pickupLocation?.coordinates?[0])!)"
      let destination = "\((dicDeliveryAddInfo?.deliveryLocation?.coordinates?[1])!),\((dicDeliveryAddInfo?.deliveryLocation?.coordinates?[0])!)"
     
      AF.request("https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(Constants.googleApiKey)").response { response in
        
        /*print(response.request as Any)  // original URL request
         print(response.response as Any) // HTTP URL response
         print(response.data as Any)     // server data
         print(response.result)*/
        
        do {
          let json = try JSON(data: response.data!)
          let routes = json["routes"].arrayValue
          
          for route in routes
          {
            let routeOverviewPolyline = route["overview_polyline"].dictionary
            let points = routeOverviewPolyline?["points"]?.stringValue
            let path = GMSPath.init(fromEncodedPath: points!)
            let polyline = GMSPolyline.init(path: path)
            polyline.strokeColor = UIColor.red
            polyline.strokeWidth = 2
            polyline.map = self.googleView
          }
        }catch {
          print(error)
        }
      }
      
      // Creates a marker in the center of the map.
      let marker = GMSMarker()
      marker.position = CLLocationCoordinate2D(latitude: (dicPickupAddInfo?.pickupLocation?.coordinates?[1])!, longitude: (dicPickupAddInfo?.pickupLocation?.coordinates?[0])!)
      marker.title = dicPickupAddInfo?.cityName
      marker.snippet = dicPickupAddInfo?.stateName
      marker.map = googleView
      
      //28.643091, 77.218280
      let marker1 = GMSMarker()
       marker.position = CLLocationCoordinate2D(latitude: (dicDeliveryAddInfo?.deliveryLocation?.coordinates?[1])!, longitude: (dicDeliveryAddInfo?.deliveryLocation?.coordinates?[0])!)
      marker1.title = dicDeliveryAddInfo?.cityName
      marker.snippet = dicDeliveryAddInfo?.stateName
      marker1.map = googleView
    }
    
    
  }

  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
}
