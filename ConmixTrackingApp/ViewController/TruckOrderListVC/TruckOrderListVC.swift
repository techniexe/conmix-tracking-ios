//
//  TruckOrderListVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 27/01/21.
//  Copyright © 2021 Techniexe Infolabs. All rights reserved.
//

import UIKit

class TruckOrderListVC: UIViewController {
  
  // MARK:  - @IBOutlets
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var lblHeader: UILabel!
  
  // MARK:  - @vars
  var arrOrderList: [VendorOrderDetailsForTracking] = []
  var isInitially: Bool = false
  var isTM: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.isTM =  Helper.isTransitMixer()
    setupView()
    // Do any additional setup after loading the view.
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if self.isTM == true {
      
      callGetOrderListByVehicleIdAPI()
      
    }else {
      
      callGetOrderListByCPAPI()
    }
    
    
  }
  
  // MARK: - setupView
  func setupView() {
    
    
    self.lblHeader.text = Constants.Text.OrderList.localized()
    self.tblView.register(UINib(nibName: "OrderListCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.orderListCellID)
   
    isInitially = true
    
  }
  
 
  
  // MARK: - Call Get Order API
  func callGetOrderListByVehicleIdAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      if let vehicleId = strVehicleID {
        
        if isInitially == true {
          Helper.showLoading(self.activityIndicator)
        }
        
       
        let strCurrentDate = Helper.getCurrentDate(Date())
        
        APIHandler.sharedInstance.getOrderByTM(vehicleId, queryParams: ["current_date": strCurrentDate]) { statuscode, response, error in
      
          self.arrOrderList = []
          
          if statuscode == 200 {
            
            if let orderResposne = response?.order {
              
              for list in orderResposne {
                self.arrOrderList.append(list)
              }
              
              DispatchQueue.main.async {
                self.updateUI()
              }
            }
            
          }else {
            Helper.showToast(_strMessage: error)
            DispatchQueue.main.async {
              self.updateUI()
            }
            return
          }
          
        }
      }else {
        DispatchQueue.main.async {
          self.updateUI()
        }
      }
      
      
      
    }
    else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
 
  
  
   
   // MARK: - Call Get Order CP API
  func callGetOrderListByCPAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      if isInitially == true {
        Helper.showLoading(self.activityIndicator)
      }
      
      
      let strCurrentDate = Helper.getCurrentDate(Date())
      
      let strCPID = Helper.getCPID()
      
      APIHandler.sharedInstance.getOrderListByCPID(strCPID, queryParams: ["current_date": strCurrentDate]) { (statuscode, response, error) in
        
        self.arrOrderList = []
        
        if statuscode == 200 {
          
          if let orderResposne = response?.order {
            
            for list in orderResposne {
              self.arrOrderList.append(list)
            }
            
            DispatchQueue.main.async {
              self.updateUI()
            }
          }
          
        }else {
          Helper.showToast(_strMessage: error)
          DispatchQueue.main.async {
            self.updateUI()
          }
          return
        }
        
      }
      
      
      
    }
    else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  // MARK: - Update UI
  func updateUI() {
    
    isInitially = false
    
    Helper.hideLoading(self.activityIndicator)
    
    guard self.arrOrderList.count != 0 else {
      let storyBoard = Helper.getStoryBorad()
      if let objNoDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC {
        objNoDataVC.view.frame = CGRect(x: 0.0, y: 44.0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 44)
        if self.isTM == true {
          objNoDataVC.lblTitle.text = Constants.Text.NoOrder.localized()
        }else {
          objNoDataVC.lblTitle.text = Constants.Text.NoAssignCPOrder.localized()
        }
        
        self.contentView.addSubview(objNoDataVC.view)
      }
      return
    }
    
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.isHidden = false
    self.tblView.reloadData()
    
  }
 
  // MARK: -@IBAction
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension TruckOrderListVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return self.arrOrderList.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.orderListCellID, for: indexPath) as? OrderListCell else {
      return UITableViewCell()
    }
    
    objCell.delegate = self
    
    objCell.btnList.tag = indexPath.section
    
    objCell.configureCell(self.arrOrderList[indexPath.section], isTransitMixer: self.isTM)
    
    return objCell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let header = UIView()
    header.backgroundColor = UIColor.clear
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 {
      return 10.0
    }
    return 0.0
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let header = UIView()
    header.backgroundColor = UIColor.clear
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if section == self.arrOrderList.count - 1 {
      return 5.0
    }
    return 0.0
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //Helper.DLog(message: "section : \(indexPath.section) row: \(indexPath.row)"
  }
}


extension TruckOrderListVC: OrderListCellDelegate {
  
  func actionButton(_ tag: Int) {
 
    //"TRUCK_ASSIGNED_FOR_PICKUP"
    if self.arrOrderList[tag].eventStatus == Constants.EventStatus.TMASSIGNED || self.arrOrderList[tag].eventStatus == Constants.EventStatus.CPASSIGNED {
      
      let storyBoard = UIStoryboard(name: "VehicleDetailLoadOrderVC", bundle: nil)
      if let objVehicleDetailLoadOrderVC = storyBoard.instantiateViewController(withIdentifier:"VehicleDetailLoadOrderVC") as? VehicleDetailLoadOrderVC {
        
        objVehicleDetailLoadOrderVC.strOrderID = self.arrOrderList[tag].vendorOrderInfo?._id
        objVehicleDetailLoadOrderVC.strTrackingID = self.arrOrderList[tag]._id
        self.navigationController?.pushViewController(objVehicleDetailLoadOrderVC, animated: true)
      }
      
    }else if self.arrOrderList[tag].eventStatus == Constants.EventStatus.PICKUP || self.arrOrderList[tag].eventStatus == Constants.EventStatus.DELAYED {
      let storyBoard = UIStoryboard(name: "VehicleDetailOrderVC", bundle: nil)
      if let objVehicleDetailOrderVC = storyBoard.instantiateViewController(withIdentifier:"VehicleDetailOrderVC") as? VehicleDetailOrderVC {
        
        objVehicleDetailOrderVC.strOrderID = self.arrOrderList[tag].vendorOrderInfo?._id
        objVehicleDetailOrderVC.strTrackingID = self.arrOrderList[tag]._id
        self.navigationController?.pushViewController(objVehicleDetailOrderVC, animated: true)
      }
    }else {
      return
    }
  }
}
