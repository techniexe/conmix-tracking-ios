//
//  TransitMixerListVC.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 10/05/21.
//

import UIKit

class TransitMixerListVC: UIViewController {
  
  //MARK:- @IBOutlets
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var lblHeader: UILabel!
  @IBOutlet weak var btnOption: UIButton!
  
  
  //MARK:- @vars
  var arrTransitMixerList: [TMDetails] = []
  var arrConcretePumpList: [ConcretePumpDetails] = []
  var noDataVC: NoDataVC? = nil
  var isTM: Bool = false
  
  //MARK:- @lets
  let manager = PopMenuManager.default
  
  //MARK:- ViewLife Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    setupView()
  }
  
//  override func viewWillAppear(_ animated: Bool) {
//    super.viewWillAppear(animated)
//    NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
//  }
//  
//  // Remove the LCLLanguageChangeNotification on viewWillDisappear
//  override func viewWillDisappear(_ animated: Bool) {
//      super.viewWillDisappear(animated)
//      NotificationCenter.default.removeObserver(self, name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
//  }
  
  
  // MARK: - setupView
  func setupView() {
    self.isTM = Helper.isTransitMixer()
    setText()
    //createPopupMenu()
    let storyBoard = Helper.getStoryBorad()
    noDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC
    self.tblView.register(UINib(nibName: "VehicleListCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.vehicleListCellID)
    self.tblView.delegate = self
    self.tblView.dataSource = self
    
    
   
    
    if self.isTM  == true{
    
      self.callTMListAPI()
      addFooterView()
      
    }else {
      
      callCPListAPI()
      
    }
    
  }
  
  // MARK: - Create Popup Menu
  func createPopupMenu() {
  
    // Customize appearance
    manager.popMenuAppearance.popMenuFont = UIFont.systemFont(ofSize: 16.0) //UIFont(name: "AvenirNext-DemiBold", size: 16)!
    manager.popMenuAppearance.popMenuBackgroundStyle = .none()
    manager.popMenuShouldDismissOnSelection = false
    manager.popMenuDelegate = self
  }
  
  // MARK: Localized Text
  func setText(){
    
    if self.isTM  == true {
   
      self.lblHeader.text = Constants.Text.ChooseVehicle.localized()
      guard self.arrTransitMixerList.count != 0 else {
        noDataVC?.lblTitle.text = Constants.Text.NoVehicle.localized()
        return
      }
      
      
    }else {
      
      self.lblHeader.text = Constants.Text.ChooseCP.localized()
     
      guard self.arrConcretePumpList.count != 0 else {
        noDataVC?.lblTitle.text = Constants.Text.NoCP.localized()
        return
      }
      
    }
    
    
  }
  
  //MARK:- Call TM List API
  func callTMListAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.sharedInstance.getListOfVendorTM(nil) { statuscode, response, error in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        self.arrTransitMixerList = []
        
        if statuscode == 200 {
          
          if let dataResponse = response?.data {
            
            for element in dataResponse {
              self.arrTransitMixerList.append(element)
            }
          }
          
          DispatchQueue.main.async {
            self.updateUI()
          }
          
        }else {
          Helper.showToast(_strMessage: error)
        }
      }
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  
  //MARK:- Call TM List API
  func callCPListAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      self.disableUserInteraction()
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.sharedInstance.getListOfVendorCP { (statuscode, response, error) in
        
        self.enableUserInteraction()
        
        Helper.hideLoading(self.activityIndicator)
        
        self.arrConcretePumpList = []
        
        if statuscode == 200 {
          
          if let dataResponse = response?.data {
            
            for (_, element) in dataResponse.enumerated() {
              self.arrConcretePumpList.append(element)
            }
          }
          
          DispatchQueue.main.async {
            self.updateUI()
          }
          
        }else {
          Helper.showToast(_strMessage: error)
        }
      }
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
  @IBAction func actionHeaderbtn(_ sender: Any) {

    let arrOptionList = [Constants.Text.SelectLanguage.localized()]
    manager.actions.removeAll()
    
    for i in 0..<arrOptionList.count {
      
      let popMenuDefaultAction = PopMenuDefaultAction(title: arrOptionList[i], image: nil, color: nil)
      manager.actions.insert(popMenuDefaultAction, at: i)
    }
    
    manager.present(sourceView: self.btnOption)
  }
  
  // MARK: - Open Select Language Popup
  func openSelectLanguagePopup() {
    let selectLanguageVC = SelectLanguageVC.init(nibName: "SelectLanguageVC", bundle: nil)
    selectLanguageVC.modalTransitionStyle = .crossDissolve
    selectLanguageVC.modalPresentationStyle = .overCurrentContext
    self.parent?.present(selectLanguageVC, animated: true, completion: nil)
  }
  
  // MARK: - Open Select Language Popup
  func openLogoutPopup() {
    CommonAlertVC.showPopup(parentVC: self, options: .logout)
  }
  
  // MARK: - Update UI
  func updateUI() {
    
    guard self.arrTransitMixerList.count != 0 || self.arrConcretePumpList.count != 0 else {
      let storyBoard = Helper.getStoryBorad()
      if let objNoDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC {
        objNoDataVC.view.frame = CGRect(x: 0.0, y: 44.0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 44)
        if self.isTM == true {
          objNoDataVC.lblTitle.text = Constants.Text.NoVehicle.localized()
        }else {
          objNoDataVC.lblTitle.text = Constants.Text.NoCP.localized()
        }
        noDataVC = objNoDataVC
        self.contentView.addSubview(objNoDataVC.view)
      }
      return
    }
    
    self.tblView.isHidden = false
    self.tblView.reloadData()
    self.tblView.layoutIfNeeded()
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
}


// MARK:- UITableViewDelegate, UITableViewDataSource
extension TransitMixerListVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if self.isTM == true {
      return self.arrTransitMixerList.count
    }else {
      return self.arrConcretePumpList.count
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.vehicleListCellID, for: indexPath) as? VehicleListCell else {
      return UITableViewCell()
    }
    
    objCell.btnList.tag = indexPath.section
    
    objCell.delegate = self
    
    if self.isTM == true {
      objCell.configureCell(self.arrTransitMixerList[indexPath.section])
    }else {
      objCell.configureCell(self.arrConcretePumpList[indexPath.section])
    }
    
    
    
    return objCell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let header = UIView()
    header.backgroundColor = UIColor.clear
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 {
      return 10.0
    }
    return 0.0
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let header = UIView()
    header.backgroundColor = UIColor.clear
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if section == self.arrTransitMixerList.count - 1 {
      return 5.0
    }
    return 0.0
  }
  
  /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     Helper.DLog(message: "section : \(indexPath.section) row: \(indexPath.row)")
   }*/
}

// MARK:-  VehicleListCellDelegatep
extension TransitMixerListVC: VehicleListCellDelegate {
  
  func actionButton(_ tag: Int) {
 
    if self.isTM == true {
      
      let storyBoard = UIStoryboard(name: "VehicleDetailVC", bundle: nil)
      if let objVehicleDetailVC = storyBoard.instantiateViewController(withIdentifier:"VehicleDetailVC") as? VehicleDetailVC {
        objVehicleDetailVC.dicTMdetails = self.arrTransitMixerList[tag]
        objVehicleDetailVC.dicCPDetails  = nil
        self.navigationController?.pushViewController(objVehicleDetailVC, animated: true)
      }
      
    }else {
      let storyBoard = UIStoryboard(name: "VehicleDetailVC", bundle: nil)
      if let objVehicleDetailVC = storyBoard.instantiateViewController(withIdentifier:"VehicleDetailVC") as? VehicleDetailVC {
        objVehicleDetailVC.dicCPDetails = self.arrConcretePumpList[tag]
        self.navigationController?.pushViewController(objVehicleDetailVC, animated: true)
      }
    }
    
    
    
  }
  
  
}


//MARK:- PopMenuViewControllerDelegate
extension TransitMixerListVC: PopMenuViewControllerDelegate {
    
    func popMenuDidSelectItem(_ popMenuViewController: PopMenuViewController, at index: Int) {
      
      popMenuViewController.dismiss(animated: true, completion: nil)
      print(index)
      switch index {
      case 0:
        openSelectLanguagePopup()//openLogoutPopup()
      case 1:
       openSelectLanguagePopup()
      default:
        break
      }
      
    
      
    }
    
}

//MARK:- PopUpProtocol
extension TransitMixerListVC: PopUpProtocol {
  
  func handleAction(action: Bool) {
    if (action) {
      GlobalFunction.removeDefaultSettingWhenLogOut()
    }
  }
  
}

extension TransitMixerListVC {
  
  // MARK:- Hide FooterView
  func hideFooterView() {
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
      self.tblView.ptr.isLoadingFooter = false
    }
  }
  
  // MARK: - Add Circulr View On Footer Of Tableview
  func addFooterView() {
    
    tblView.ptr.footerHeight = 40
    
    //reviewTblView.ptr.footerView = getProgressCircle()
    
    self.tblView.ptr.footerCallback = { [weak self] in
      
      if self?.arrTransitMixerList.count != 0 {
        
        if let strCreatedAt = self?.arrTransitMixerList.last?.createdAt {
          
          if let dateFromString = strCreatedAt.dateFromISO8601 {
            
            let beforeTime = dateFromString.iso8601
            
            DispatchQueue.global(qos: .default).async(execute: {
            
              APIHandler.sharedInstance.getListOfVendorTM(["before": beforeTime]) { (statuscode, response, error)in
                
                
                if statuscode == 200 {
                  
                  if let dataResponse = response?.data {
                    
                    if dataResponse.count == 0 {
                      
                      self?.hideFooterView()
                      
                    }else {
                      
                      var arrSectionIndex: [Int] = []
                      
                      for (index, element) in dataResponse.enumerated() {
                        self?.arrTransitMixerList.append(element)
                        arrSectionIndex.insert((self?.arrTransitMixerList.count)!, at: index)
                      }
                      
                      DispatchQueue.main.async {
                        self?.tblView.performBatchUpdates({
                          self?.tblView.insertSections(IndexSet(arrSectionIndex), with: UITableView.RowAnimation.none)
                        }, completion: { (completed) in
                          
                        })
                      }
                      
                      
                    }
                    
                    
                    
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
                
              }
              
              /*APIHandler.shared.getReviewList(queryParams: ["before": beforeTime]) { [self] (statuscode, dataResponse, error) in
                
                if dataResponse != nil {
                  
                  if let dataArray = dataResponse?.data {
                    
                    if dataArray.count > 0 {
                      
                      self?.reviewTblView.ptr.isLoadingFooter = false
                      
                      var arrIndexPath: [IndexPath] = []
                      
                      for element in dataArray {
                        arrIndexPath.append(IndexPath(row: self?.arrReviewList.count ?? 0, section: 0))
                        self?.arrReviewList.append(element)
                        
                      }
                      
                      DispatchQueue.main.async {
                        self?.reviewTblView.performBatchUpdates({
                          self?.reviewTblView.insertRows(at: arrIndexPath, with: UITableView.RowAnimation.none)
                        }, completion: { (completed) in
                          
                        })
                      }
                      
                
                    }else {
                      
                      self?.hideFooterView()
                    }
                    
                  }else {
                    self?.hideFooterView()
                  }
                  
                }else {
                  self?.hideFooterView()
                }
                
              }*/
              
              
            })
            
          }
          
        }
          
        
      }else{
        self?.hideFooterView()
      }

    }
    
  }
  
}
