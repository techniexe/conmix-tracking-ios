//
//  CommonAlertVC.swift
//  ConmixApp
//
//  Created by Techniexe Infolabs on 16/03/21.
//

import UIKit

protocol PopUpProtocol {
  func handleAction(action: Bool)
}

class CommonAlertVC: UIViewController {
  
  //MARK:- @IBOutlets
  @IBOutlet weak var dialogBoxView: UIView!
  @IBOutlet weak var lblMessage: UILabel!
  @IBOutlet weak var yesButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  
  //MARK:- @vars
  var commonAlert: CommonAlert = .logout
  var delegate: PopUpProtocol?
  
  private let backgroundOpacity = CGFloat(0.6)
  
  //MARK:- View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    setupView()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  //MARK:- setupView
  func setupView() {
    
    //adding an overlay to the view to give focus to the dialog box
    view.backgroundColor = UIColor(white: 0, alpha: self.backgroundOpacity) //UIColor.black.withAlphaComponent(0.50)
    dialogBoxView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 4)
    self.cancelButton.setTitle(Constants.Text.No.localized(), for: .normal)
    self.yesButton.setTitle(Constants.Text.Yes.localized(), for: .normal)
    
    
    switch commonAlert {
    case .pickUp:
      self.lblMessage.text = Constants.Alerts.GoForPickupMsg.localized()
    case .changeQty:
      self.lblMessage.text = Constants.Alerts.SetRoyaltyPassMsg.localized() //"Are you sure you want to change order quantity?"
    case .acceptOrder:
      self.lblMessage.text = Constants.Alerts.AcceptorderMsg.localized()
    case .cancelOrder:
      self.lblMessage.text = Constants.Alerts.RejectorderMsg.localized()
    case .logout:
      self.lblMessage.text = Constants.Alerts.LogoutMsg.localized()
    }
  }
  
  @IBAction func cancelButtonPressed(_ sender: Any) {
    dismissViewController()
  }
  
  @IBAction func yesButtonPressed(_ sender: Any) {
  
    self.dismiss(animated: true) {
      self.delegate?.handleAction(action: true)
    }
  }
  
  
  //MARK:- functions for the viewController
  static func showPopup(parentVC: UIViewController, options: CommonAlert){
    //creating a reference for the dialogView controller
    let storyBoard = UIStoryboard(name: "CommonAlertVC", bundle: nil)
    if let commonAlertVC = storyBoard.instantiateViewController(withIdentifier: "CommonAlertVC") as? CommonAlertVC {
      commonAlertVC.modalPresentationStyle = .custom
      commonAlertVC.modalTransitionStyle = .crossDissolve
      commonAlertVC.commonAlert = options
      //setting the delegate of the dialog box to the parent viewController
      commonAlertVC.delegate = parentVC as? PopUpProtocol
    
      //presenting the pop up viewController from the parent viewController
      parentVC.present(commonAlertVC, animated: true)
    }
  }
}
