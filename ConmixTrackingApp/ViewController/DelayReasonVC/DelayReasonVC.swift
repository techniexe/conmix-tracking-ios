//
//  DelayReasonVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 23/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class DelayReasonVC: UIViewController, UITextFieldDelegate {
  
  @IBOutlet var lblHeader: UILabel!
  
  @IBOutlet var btnSave: UIButton!
  
  @IBOutlet var txtTime: JVFloatLabeledTextField!
  
  @IBOutlet var txtReason: JVFloatLabeledTextField!
  
  @IBOutlet weak var txtView: JVFloatLabeledTextView!
  
  @IBOutlet weak var txtViewheightConstraint: NSLayoutConstraint!
  
  var handler: GrowingTextViewHandler?
  
  var arrSelectTime: [String] = []
  
  var strOrderID: String? = nil
  
  var strTrackingID: String? = nil
  
  var pickerView = UIPickerView()
  
  var reasonPickerView = UIPickerView()
  
  var day: Int = 0
  var hour: Int = 0

  var isTM: Bool = false
  
  var arrReasonList: [String] = [Constants.Text.HeavyTraffic.localized(), Constants.Text.WeatherConditions.localized(), Constants.Text.VehicleBreakdowns.localized(),Constants.Text.Puncture.localized(), Constants.Text.Accident.localized()]
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    self.isTM = Helper.isTransitMixer()
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    self.lblHeader.text = Constants.Text.DelayReason.localized()
    self.txtTime.placeholder = Constants.Text.DelayTime.localized()
    self.txtReason.placeholder = Constants.Text.ChooseReason.localized()
    self.btnSave.setTitle(Constants.Text.Done.localized(), for: .normal)
    //self.txtView.layoutIfNeeded()
    self.btnSave.alpha = 1.0
    self.btnSave.isEnabled = true
//    self.txtView.isScrollEnabled = false
//    handler = GrowingTextViewHandler(textView: self.txtView, heightConstraint: self.txtViewheightConstraint)
//    handler?.minimumNumberOfLines = 1
//    handler?.maximumNumberOfLines = 4
//    handler?.setText("", animated: false)
//    self.textViewDidChange(self.txtView)
    
    pickerView.delegate = self
    pickerView.dataSource = self
    reasonPickerView.delegate = self
    reasonPickerView.dataSource = self

    self.txtTime.inputView = self.pickerView
    self.txtReason.inputView = self.reasonPickerView
    
    self.txtTime.text = "1 hrs"
    self.txtReason.text = self.arrReasonList[0]
    
    for i in 1..<25 {
      self.arrSelectTime.append("\(i) hrs")
    }
    
    for i in 1..<6 {
      self.arrSelectTime.append("\(i) day")
    }
  }

  @IBAction func actionClose(_ sender: Any) {
    self.view.endEditing(true)
    self.dismissViewController()
  }
  
  @IBAction func actionSave(_ sender: Any) {
   
    callOrderDelayAPI()
    
    /*if self.txtTime.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
      Helper.showToast(_strMessage: "Please select delay time")
    }else if self.txtReason.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
      Helper.showToast(_strMessage: "Please choose reason")
    }/*else if self.txtTime.text?.trimmingCharacters(in: .whitespacesAndNewlines) == placeholder {
      Helper.showToast(_strMessage: "Please  select delay time")
    }*//*else if self.txtView.text.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
      Helper.showToast(_strMessage: ErrorMessage.noreason.rawValue)
    }*/else {
      self.callOrderDelayAPI()
    }*/
    
  }
  
  
  // MARK: - Call Order Delay API
  func callOrderDelayAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      if let strTime = self.txtTime.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
        
        if let description = self.txtReason.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
          
          let params: [String: Any]?  = ["reasonForDelay": description, "delayTime": strTime]
          
          if self.isTM == true {
            
            APIHandler.sharedInstance.delayOrder(strVehicleID ?? "", strTrackingID: self.strTrackingID ?? "", strOrderID: strOrderID ?? "", bodyParams: params) {(statusCode, error) in
              
              if statusCode == 202 {
                self.view.endEditing(true)
                Helper.showToast(_strMessage: Constants.SuccessMessage.updateorderstatus.localized())
                
                self.dismissViewController()
              } else {
                Helper.showToast(_strMessage: error)
                return
              }
            }
            
          }else  {
            
            let strCPID = Helper.getCPID()
            
            if let trackingId = self.strTrackingID, let orderID = self.strOrderID {
              
              APIHandler.sharedInstance.delayOrderForCP(strCPID, strTrackingID: trackingId, strOrderID: orderID, bodyParams: params) { (statusCode, error) in
                
                if statusCode == 202 {
                  self.view.endEditing(true)
                  Helper.showToast(_strMessage: Constants.SuccessMessage.updateorderstatus.localized())
                  
                  self.dismissViewController()
                } else {
                  Helper.showToast(_strMessage: error)
                  return
                }
              }
            }
            
          }
          
        }
      }
      
    } else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
}



// MARK: - UITextViewDelegate
extension DelayReasonVC : UITextViewDelegate {
    
 /* func textViewDidChange(_ textView: UITextView) {
    self.handler?.resizeTextView(true)
  }*/
  
  func textViewDidChange(_ textView: UITextView) {
    
    let numLines = Helper.numberOfLines(textView: textView)
    
    //self.handler?.resizeTextView(false)
    //print("lines::::\(numLines) \(textView.bounds.height)")
    
    let sizeThatFitsTextView = textView.sizeThatFits(CGSize(width: textView.bounds.size.width, height: CGFloat(MAXFLOAT)))
    
    print(sizeThatFitsTextView)
    
    if numLines > 4 {
      textView.isScrollEnabled = true
      
    }else {
      
      if textView.text == "" {
        self.txtViewheightConstraint.constant = 37
      }else {
        self.txtViewheightConstraint.constant = sizeThatFitsTextView.height //+ 5.0
      }
    
      
    }
  }
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    txtView.placeholder = nil
    //self.textLineView.backgroundColor = Constants.Colors.themeColor
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    //self.textLineView.backgroundColor = Constants.Colors.lineColor
    if textView.text.count == 0 {
      txtView.placeholder = Constants.Text.WriteReason.localized()
    }else {
       txtView.placeholder = nil
    }
   
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    
    
    if text == "\n" {
      textView.resignFirstResponder()
      return true
    }
    
    
    if range.location == 0 && text == " " {
      return false
    }
 
    
    let newLength = textView.text.utf16.count + text.utf16.count - range.length
    
    if newLength == 0 {

      self.txtViewheightConstraint.constant = 37
      //self.txtView.placeholderLabel.isHidden = false
      return true
    }
      
    else if newLength > 0 && newLength <= 500 {
    
      //self.txtView.placeholderLabel.isHidden = true
      return true
    }
    else {
      return false
    }
    
  }
  
}

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
extension DelayReasonVC: UIPickerViewDelegate, UIPickerViewDataSource {
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    switch component {
    case 0:
      if pickerView == reasonPickerView {
        return self.arrReasonList.count
      }else {
        return self.arrSelectTime.count
      }
    default:
      return 0
    }
  }
  
  /*func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
    return pickerView.frame.size.width/3
  }*/
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    switch component {
    case 0:
      if pickerView == reasonPickerView {
        return self.arrReasonList[row]
      }else {
        return self.arrSelectTime[row]
      }
      
    default:
      return ""
    }
  }
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    switch component {
    case 0:
      if pickerView == reasonPickerView {
        self.txtReason.text = self.arrReasonList[row]
      }else {
        self.txtTime.text = self.arrSelectTime[row]
      }
      
    default:
      break;
    }
    
//    if day == 0 && hour == 0 {
//      self.txtTime.text = placeholder
//    }else if day == 0 && hour != 0 {
//
//      if hour == 1 {
//        self.txtTime.text = "00 days \(hour) hr"
//      }else {
//        self.txtTime.text = "00 days \(hour) hrs"
//      }
//
//    }else if day != 0 && hour == 0 {
//      if day == 1 {
//        self.txtTime.text = "\(day) day 00 hrs"
//      }else {
//        self.txtTime.text = "\(day) days 00 hrs"
//      }
//    }else {
//
//      if day == 1 {
//
//        if hour == 1 {
//          self.txtTime.text = "\(day) day \(hour) hr"
//        }else {
//          self.txtTime.text = "\(day) day \(hour) hrs"
//        }
//
//      }else {
//        if hour == 1 {
//          self.txtTime.text = "\(day) days \(hour) hr"
//        }else {
//          self.txtTime.text = "\(day) days \(hour) hrs"
//        }
//      }
//
//    }
  }
}
