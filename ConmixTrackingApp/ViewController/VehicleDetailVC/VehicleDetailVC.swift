//
//  VehicleDetailVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 20/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class VehicleDetailVC: UIViewController {
  
  //MARK:- @IBOutlets
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var lblHeader: UILabel!
 
  //MARK:- @vars
  var dicTMdetails: TMDetails!
  var dicCPDetails: ConcretePumpDetails? = nil
  var isTM: Bool = false
  var locationManager: CLLocationManager?
  var isLocationEnable: Bool = false
  
  //MARK:- View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  //MARK:- setupView
  func setupView() {
    
    self.isTM = Helper.isTransitMixer()
    
    if self.isTM == true {
      
      self.locationManager = CLLocationManager()
      locationManager?.requestAlwaysAuthorization()
      locationManager?.requestWhenInUseAuthorization()
      if CLLocationManager.locationServicesEnabled() {
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager?.startUpdatingLocation()
      }
      
      //self.locationManager = CLLocationManager()
      //self.locationManager?.delegate = self
      checkLocationServices()
      self.lblHeader.text = Constants.Text.TruckDetail.localized()
      self.tblView.register(UINib(nibName: "VehicleDetailCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.vehicleDetailCellID)
    }else {
      self.lblHeader.text = Constants.Text.CPDetail.localized()
      self.contentView.backgroundColor = UIColor.white
      self.tblView.register(UINib(nibName: "CPDetailsCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.CPDetailsCellID)
    }
    
    self.tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.reloadData()
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  //MARK:- Push To Truck Order List VC
  func pushToTruckOrderListVC() {
    
    if self.isTM == true {
      
      let storyBoard = UIStoryboard(name: "TruckOrderListVC", bundle: nil)
      if let objTruckOrderListVC = storyBoard.instantiateViewController(withIdentifier:"TruckOrderListVC") as? TruckOrderListVC {
        strVehicleID = self.dicTMdetails._id
        self.navigationController?.pushViewController(objTruckOrderListVC, animated: true)
      }
      
    }else {
      
      let storyBoard = UIStoryboard(name: "TruckOrderListVC", bundle: nil)
      if let objTruckOrderListVC = storyBoard.instantiateViewController(withIdentifier:"TruckOrderListVC") as? TruckOrderListVC {
        
        if let strCPID = self.dicCPDetails?._id {
          if Helper.isKeyPresentInUserDefaults(key: Constants.UserDefaults.CPID) == true {
            UserDefaults.standard.removeObject(forKey: Constants.UserDefaults.CPID)
          }
          UserDefaults.standard.set(strCPID, forKey:Constants.UserDefaults.CPID)
          UserDefaults.standard.synchronize()
          self.navigationController?.pushViewController(objTruckOrderListVC, animated: true)
        }
        
       
      }
    }
    
    
  }
  
  //MARK:- @Action For Next
  @objc func actionNext(_ sender: UIButton) {
    
    self.pushToTruckOrderListVC()
  }
  
  @objc func actionMobileNumber(_ tapGestureRecognizer: UITapGestureRecognizer) {
    
    if self.isTM == true {
      
      if let driver1Info = self.dicTMdetails.driver1Info {
       
        if let phoneNumber = driver1Info.driverMobileNumber {
          Helper.tapOnMobileNumber(phoneNumber)
        }
        
      }
      
    }else {
      
      if let operatorInfo = self.dicCPDetails?.operatorInfo {
        
        if let phoneNumber = operatorInfo.operatorMobileNumber {
          
          Helper.tapOnMobileNumber(phoneNumber)
        }
        
      }
      
    
      
    }
    
  }
  
  
  @objc func actionSecondMobileNumber(_ tapGestureRecognizer: UITapGestureRecognizer) {
    
    if self.isTM == true {
      
      if let driver2Info = self.dicTMdetails.driver2Info {
       
        if let phoneNumber = driver2Info.driverMobileNumber {
          
          Helper.tapOnMobileNumber(phoneNumber)
        }
        
      }
      
    }
    
  }
  
  
  //MARK:- Check Location Service
  func checkLocationServices() {
    
    if CLLocationManager.locationServicesEnabled() == true {
      
      switch locationManager?.authorizationStatus {
      case .notDetermined, .restricted, .denied:
        isLocationEnable = false
        locationManager?.requestWhenInUseAuthorization()
      case .authorizedAlways, .authorizedWhenInUse:
        isLocationEnable = true
      default:
        Helper.DLog(message: "")
      }
      
      locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager?.delegate = self
      locationManager?.startUpdatingLocation()
    } else {
      isLocationEnable = false
      locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager?.delegate = self
      locationManager?.startUpdatingLocation()
    }
    
  }
  
  @objc func actionCurrentLocation(_ btn: UIButton) {
    
    let storyBoard = UIStoryboard(name: "MapRouteVC", bundle: nil)
    
    if let objMapRouteVC = storyBoard.instantiateViewController(withIdentifier:"MapRouteVC") as? MapRouteVC {
      
      objMapRouteVC.fromScreen = .truckdetails
     
      if let location = UserDefaults.standard.location(forKey: Constants.UserDefaults.CurrentLocation) {
        
        objMapRouteVC.dicPickUpLocation = Location(type: "Point", coordinates: [location.coordinate.longitude, location.coordinate.latitude])
        self.navigationController?.pushViewController(objMapRouteVC, animated: true)
      }
      
      
      
    }
    
  }
  
}


//MARK:- UITableViewDelegate, UITableViewDataSource
extension VehicleDetailVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if self.isTM == true {
      
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.vehicleDetailCellID, for: indexPath) as? VehicleDetailCell else {
        return UITableViewCell()
      }
      
      objCell.delegate = self
      
      let gesture = UITapGestureRecognizer(target: self, action: #selector(actionMobileNumber(_:)))
      objCell.lblDriver1Contact.isUserInteractionEnabled = true
      objCell.lblDriver1Contact.addGestureRecognizer(gesture)
      
      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(actionSecondMobileNumber(_:)))
      objCell.lblDriver2Contact.isUserInteractionEnabled = true
      objCell.lblDriver2Contact.addGestureRecognizer(tapGesture)
      
      objCell.configureCell(dicTMdetails)
      
      
      if CLLocationManager.locationServicesEnabled() == true {
        
        if let location = UserDefaults.standard.location(forKey: Constants.UserDefaults.CurrentLocation) {
          
          objCell.lineView[1].isHidden = false
          objCell.currentLocationView.isHidden = false
          
          self.reverseGeocodeCoordinate(CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude), objCell: objCell)
      
          objCell.btnCurrentLocation.addTarget(self, action: #selector(self.actionCurrentLocation(_:)), for: .touchUpInside)
         
          
          
        }else {
          objCell.lineView[1].isHidden = true
          objCell.currentLocationView.isHidden = true
        }
        
      }else {
          objCell.lineView[1].isHidden = true
          objCell.currentLocationView.isHidden = true
      }
      
      return objCell
      
    }else {
     
      guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.CPDetailsCellID, for: indexPath) as? CPDetailsCell else {
        return UITableViewCell()
      }
      
      objCell.btnNext.addTarget(self, action: #selector(self.actionNext(_:)), for: .touchUpInside)
      
      let gesture = UITapGestureRecognizer(target: self, action: #selector(actionMobileNumber(_:)))
      objCell.lblSubTitle[5].isUserInteractionEnabled = true
      objCell.lblSubTitle[5].addGestureRecognizer(gesture)
    
      
      if let details = dicCPDetails {
        objCell.configureCell(details)
      }
      
      
      
      return objCell
      
    }
    
    
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
}

//MARK:- VehicleDetailsDelegate
extension VehicleDetailVC: VehicleDetailsDelegate {
  
  func actionVehicleLocation() {
    
    let storyBoard = UIStoryboard(name: "MapRouteVC", bundle: nil)
    if let objMapRouteVC = storyBoard.instantiateViewController(withIdentifier:"MapRouteVC") as? MapRouteVC {
      objMapRouteVC.fromScreen = .truckdetails
     
      if let objPickupLocation = dicTMdetails.pickupLocation {
        objMapRouteVC.dicPickUpLocation = objPickupLocation
        self.navigationController?.pushViewController(objMapRouteVC, animated: true)
      }else {
        Helper.DLog(message: "pickup location not found")
        return
      }
      
    }
    
  }
  
  func actionPickup() {
    
    pushToTruckOrderListVC()
  }
  
}

//MARK:- CLLocationManagerDelegate
extension VehicleDetailVC: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    
    if CLLocationManager.locationServicesEnabled() == true {
      
      switch status {
    
      case .notDetermined:
        Helper.DLog(message: "notDetermined")
        if let _ = UserDefaults.standard.location(forKey: Constants.UserDefaults.CurrentLocation) {
          //self.lblButtonTitle.text = Constants.Settings
        }else {
          //self.lblButtonTitle.text = Constants.AllowLocationPermission
        }
        
        self.tblView.reloadData()
        
      case .restricted:
        Helper.DLog(message: "restricted")
        self.tblView.reloadData()
      case .denied:
        Helper.DLog(message: "denied")
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
        
        /*if let _ = UserDefaults.standard.location(forKey: Constants.UserDefaults.MyLocation) {
        }else {
          self.lblButtonTitle.text = Settings
        }*/
        self.tblView.reloadData()
        
      case .authorizedAlways, .authorizedWhenInUse:
        
        
        self.tblView.reloadData()
        
        
      default:
        Helper.DLog(message: "Not Find Status")
      }
    }
    
  }
  
  //MARK:- CLLocationManager Delegates
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
    if locations.count > 0 {
      
      // guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
      
      let locationArray = locations as NSArray
      
      if let locationObj = locationArray.firstObject as? CLLocation {
        
        UserDefaults.standard.set(location: locationObj, forKey: Constants.UserDefaults.CurrentLocation)
        
        self.locationManager?.stopUpdatingLocation()
        self.locationManager?.delegate = nil
        
        self.tblView.reloadData()
        
        //Location Update on Server
        //self.getCityNameFromCurrentLocation()
        
        isLocationEnable = true
      }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    //Helper.showToast(_strMessage: "Unable to access your current location")
  }
  
  func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
      CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
  }
  
}

extension VehicleDetailVC {
  
  // MARK: - Reverse GeoCoordinates
  private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D, objCell: VehicleDetailCell) {
    
    let geocoder = GMSGeocoder()
    
    geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
      
      //self.addressView.unlock()
      //self.view.isUserInteractionEnabled = true
      
      guard let address = response?.firstResult(), let lines = address.lines   else {
        return
      }
      
      /*self.selectedAddress = address
      
      let strLine = lines[0]
      
      // print(str.split(separator: ","))
      
      let arrSplit = strLine.split(separator: ",")
      
      var strShortAdd: String = ""
      
      if arrSplit.count >= 3 {
        strShortAdd = "\(arrSplit[1]), \(arrSplit[2])"
      }else if arrSplit.count >= 2 {
        strShortAdd = "\(arrSplit[1])"
      }else {
        strShortAdd = "\(arrSplit[0])"
      }*/
      
      //let strCoordinates = GlobalFunction.getStringFromLatLong(address.coordinate.latitude, long: address.coordinate.longitude)
      
      
      
      objCell.lblCurrentLocation.text =   lines.joined(separator: "\n")
      //objCell.btnCurrentLocation.setTitle(lines.joined(separator: "\n"), for: UIControl.State.normal)
  
    }
  }
  
}

////MARK:-  CustomAlertViewDelegate
//extension VehicleDetailVC: CustomAlertViewDelegate {
//    
//    func actionAlertDialog(_ aButton: UIButton) {
//        
//        switch aButton.tag {
//        case 0:
//            self.dismissViewController()
//        case 1:
//            self.dismiss(animated: false) {
//          
//              self.pushToTruckOrderListVC()
//              
//            }
//        default:
//            print(aButton.tag)
//        }
//    }
//}
//

