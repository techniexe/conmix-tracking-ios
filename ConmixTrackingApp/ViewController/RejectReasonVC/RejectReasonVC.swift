//
//  RejectReasonVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 01/02/21.
//  Copyright © 2021 Techniexe Infolabs. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

@objc protocol RejectReasonDelegate: AnyObject {
  
  func actionButton(_ strText: String, viewController: UIViewController)
}

class RejectReasonVC: UIViewController {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var lblHeader: UILabel!
  @IBOutlet weak var txtView: JVFloatLabeledTextView!
  @IBOutlet var btnSave: UIButton!
  @IBOutlet weak var txtViewheightConstraint: NSLayoutConstraint!
  
  // MARK: - @vars
  var handler: GrowingTextViewHandler?
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var strDeliveryStatus : String = ""
  var delegate: RejectReasonDelegate? = nil
  var isTM: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    // Do any additional setup after loading the view.
  }
  
  
  
  // MARK: - setupView
  func setupView() {
    
    self.navigationController?.setNavigationBarHidden(true, animated: false)
    
    self.isTM = Helper.isTransitMixer()
    
    if self.isTM == true {
      self.lblHeader.text = Constants.Text.RejectReason.localized()
    }else {
      self.lblHeader.text = Constants.Text.Complaint.localized()
    }
    
    
    self.btnSave.setTitle(Constants.Text.Done.localized(), for: .normal)
    //self.txtView.layoutIfNeeded()
    self.btnSave.alpha = 1.0
    self.btnSave.isEnabled = true
    self.txtView.isScrollEnabled = false
    handler = GrowingTextViewHandler(textView: self.txtView, heightConstraint: self.txtViewheightConstraint)
    handler?.minimumNumberOfLines = 1
    handler?.maximumNumberOfLines = 4
    handler?.setText("", animated: false)
    self.textViewDidChange(self.txtView)
    
  
  }
  
  @IBAction func actionClose(_ sender: Any) {
    self.view.endEditing(true)
    self.dismissViewController()
  }
  
  @IBAction func actionSave(_ sender: Any) {
   
    if self.txtView.text.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noreason.localized())
    }else {
      callSendOTPAPI(strDeliveryStatus)
    }
    
  }
  
  
  // MARK: - CALL SEND OTP
  func callSendOTPAPI(_ deliveryStatus: String) {
    
    if let buyerInfo = self.dicOrderDetails?.buyerInfo {
      
      if let mobileNumber = buyerInfo.mobileNumber {
        
        
        APIHandler.sharedInstance.requestOTPWithMobile(mobileNumber)  { (statuscode,response, error) in
          
          if statuscode == 200 {
            
            if let code = response?.code, code != "" {
              
              self.dismiss(animated: false) {
               
                Helper.showToast(_strMessage: Constants.SuccessMessage.codesuccess.localized())
                self.delegate?.actionButton(self.txtView.text.trim(), viewController: self)
              }
              
            }
            
          }else {
            Helper.showToast(_strMessage: error)
          }
          
          guard error == nil else {
            Helper.showToast(_strMessage: error)
            return
          }
          
        }
        
      }
      
    }
  }
  
}


// MARK: - UITextViewDelegate
extension RejectReasonVC : UITextViewDelegate {
    
 /* func textViewDidChange(_ textView: UITextView) {
    self.handler?.resizeTextView(true)
  }*/
  
  func textViewDidChange(_ textView: UITextView) {
    
    let numLines = Helper.numberOfLines(textView: textView)
    
    //self.handler?.resizeTextView(false)
    //print("lines::::\(numLines) \(textView.bounds.height)")
    
    let sizeThatFitsTextView = textView.sizeThatFits(CGSize(width: textView.bounds.size.width, height: CGFloat(MAXFLOAT)))
    
    print(sizeThatFitsTextView)
    
    if numLines > 4 {
      textView.isScrollEnabled = true
      
    }else {
      
      if textView.text == "" {
        self.txtViewheightConstraint.constant = 37
      }else {
        self.txtViewheightConstraint.constant = sizeThatFitsTextView.height //+ 5.0
      }
    
      
    }
  }
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    txtView.placeholder = nil
    //self.textLineView.backgroundColor = Constants.Colors.themeColor
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    //self.textLineView.backgroundColor = Constants.Colors.lineColor
    if textView.text.count == 0 {
      if self.isTM == true {
        txtView.placeholder = Constants.Text.WriteReason.localized()
      }else {
        txtView.placeholder = Constants.Text.WriteComplaint.localized()
      }
    }else {
       txtView.placeholder = nil
    }
   
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    
    
//    if text == "\n" {
//      textView.resignFirstResponder()
//      return true
//    }
//
    
    if range.location == 0 && text == " " {
      return false
    }
 
    
    let newLength = textView.text.utf16.count + text.utf16.count - range.length
    
    if newLength == 0 {

      self.txtViewheightConstraint.constant = 37
      //self.txtView.placeholderLabel.isHidden = false
      return true
    }
      
    else if newLength > 0 && newLength <= 500 {
    
      //self.txtView.placeholderLabel.isHidden = true
      return true
    }
    else {
      return false
    }
    
  }
  
}
