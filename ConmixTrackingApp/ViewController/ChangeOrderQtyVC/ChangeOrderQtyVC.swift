//
//  ChangeOrderQtyVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 21/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class ChangeOrderQtyVC: UIViewController {
  
  let ACCEPTABLE_NUMBERS = "0123456789."
  
  @IBOutlet var txtFieldCollection: [JVFloatLabeledTextField]!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet var viewCollection: [UIView]!
  @IBOutlet weak var lblHeader: UILabel!
  @IBOutlet weak var btnNext: UIButton!
 
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
    
    self.lblHeader.text = Constants.Text.ChangeOrderQty.localized()
    self.btnNext.setTitle(Constants.Text.Next.localized(), for: .normal)
  
    self.txtFieldCollection[0].placeholder = Constants.Text.PickupQty.localized()
    self.txtFieldCollection[1].placeholder = Constants.Text.RoyaltyPass.localized()
    
    self.activityIndicator.isHidden = true
    
    if let pickupQty = self.dicOrderDetails?.pickupQuantity {
      let qty = Helper.getOrderQuantity(NSNumber(value: pickupQty))
      self.txtFieldCollection[0].text = String(format: "%@", qty)
      self.txtFieldCollection[1].text = String(format: "%@", qty)
      //self.txtFieldCollection[1].becomeFirstResponder()
    }else{
      self.txtFieldCollection[0].text = "0.0"
    }
    
    self.txtFieldCollection[0].isEnabled = false
    
   /* if let royalityQty = loadOrderViewModel.orderByVehicle?.royality_quantity { //Int((self.txtFieldCollection[1].text?.trimmingCharacters(in: .whitespacesAndNewlines))!) {
      self.txtFieldCollection[1].text = String(format: "%.1f", royalityQty)
    }else {
      self.txtFieldCollection[1].text = "0.0"
    }*/
    
    
  }
  
  @IBAction func actionBack(_ sender: Any) {
    //self.navigationController?.popToRootViewController(animated: true)
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func actionNext(_ sender: Any) {
  
    self.callUpdateQtyAPI()
    
//    let rpQty = Double((self.txtFieldCollection[1].text?.trimmingCharacters(in: .whitespacesAndNewlines))!)
//    //let strRoyaltyQty = Helper.getQuantity(NSNumber(value: rpQty ?? 0.0))
//    var pickupQty: Double! = 0.0
//
//    if let qty = self.dicOrderDetails?.pickupQuantity {
//      pickupQty = qty
//    }
//
////    if self.txtFieldCollection[0].text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
////      Helper.showToast(_strMessage: "Please enter pickup quantity")
////    }else
//   
//    if self.txtFieldCollection[1].text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
//      Helper.showToast(_strMessage: Constants.ErrorMessage.RoyaltyPassZero.localized())
//    }
//    /*else if strRoyaltyQty == "0" {
//       Helper.showToast(_strMessage: "Royalty quantity can not be zero")
//    }*/
//    else if pickupQty < (rpQty)!  {
//      Helper.showToast(_strMessage: Constants.ErrorMessage.Royalynotgreaterpickup.localized())
//    }else {
//      //CommonAlertVC.showPopup(parentVC: self, options: .changeQty)
//      self.callUpdateQtyAPI()
//    }
    
  }
  
  // MARK: - Call Update Qty API
  func callUpdateQtyAPI() {
    
    if Reachability.isConnectedToNetwork() {
    
      self.view.endEditing(true)
      
      Helper.showLoading(self.activityIndicator)
      
      //if let pickupQty = Double((self.txtFieldCollection[0].text?.trimmingCharacters(in: .whitespacesAndNewlines))!) {
        
        if let pickupQty = Double((self.txtFieldCollection[0].text?.trimmingCharacters(in: .whitespacesAndNewlines))!) {
          
          let params: [String: Any]?  = ["pickup_quantity": pickupQty, "royality_quantity": 0.0]
          
          APIHandler.sharedInstance.changeOrderQty(strVehicleID ?? "", strOrderID: self.dicOrderDetails?.vendorOrderInfo?._id ?? "", strTrackID: self.dicOrderDetails?._id ?? "", bodyParams: params) { (statusCode, error) in
            
            Helper.hideLoading(self.activityIndicator)
          
            if statusCode == 202 {
              
              let storyBoard = UIStoryboard(name: "OrderInfoVC", bundle: nil)
              if let objOrderInfoVC = storyBoard.instantiateViewController(withIdentifier:"OrderInfoVC") as? OrderInfoVC {
                objOrderInfoVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
                objOrderInfoVC.strTrackingID = self.dicOrderDetails?._id
                //royaltyQty = pickupQty
                self.navigationController?.pushViewController(objOrderInfoVC, animated: true)
              }
            } else {
              Helper.showToast(_strMessage: error)
              return
            }
            
          }
          
        }
      //}
      
    }else {
      GlobalFunction.noNetworkConnection()
    }
    
  }
  
}

// MARK: - UITextFieldDelegate
extension ChangeOrderQtyVC : UITextFieldDelegate {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
    
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
   
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    textField.resignFirstResponder()
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if (textField.text?.contains("."))! && string == "." {
      return false
    }
    
    if range.location == 0 && string == "." || range.location == 0 && string == " " {
      return false
    }
    
    let newLength: Int = textField.text!.count + string.count - range.length
    let numberOnly = NSCharacterSet.init(charactersIn: ACCEPTABLE_NUMBERS).inverted
    let strValid = string.rangeOfCharacter(from: numberOnly) == nil
    return (strValid && (newLength <= 10))
  }
}


//MARK:- PopUpProtocol
extension ChangeOrderQtyVC: PopUpProtocol {
  
  func handleAction(action: Bool) {
    if (action) {
      self.callUpdateQtyAPI()
    }
  }
  
}
