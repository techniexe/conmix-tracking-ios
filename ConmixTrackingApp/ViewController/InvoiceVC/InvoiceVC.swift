//
//  InvoiceVC.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 13/10/21.
//

import UIKit
import PDFKit
import WebKit

class InvoiceVC: UIViewController {
  
  //MARK:- @IBOutlets
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var mainView: UIView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
 
  //MARK:- @vars
  var strInvoice: String = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    displayPdf()
  }
  
  override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
    
  }
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  
  func setupView() {
    
    // Add PDFView to view controller.
    let pdfView = PDFView(frame: view.bounds)
    pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.view.addSubview(pdfView)
    
    // Fit content in PDFView.
    pdfView.autoScales = true
    
    // Load Sample.pdf file from app bundle.
    //let fileURL = Bundle.main.url(forResource: "Sample", withExtension: "pdf")
    //pdfView.document = PDFDocument(url: fileURL!)
    
    
    
    if strInvoice != ""{
      if let document = PDFDocument(url: URL(string: strInvoice)!) {
          pdfView.document = document
      }
    }
   
    
    
  }
  
  /*
   View PDF with PDFKit
   */
  private func createPdfView(withFrame frame: CGRect) -> PDFView {
      let pdfView = PDFView(frame: frame)
      pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      pdfView.autoScales = true
      
      return pdfView
  }
  
  private func createPdfDocument(forFileName fileName: String) -> PDFDocument? {
    //        if let resourceUrl = self.resourceUrl(forFileName: fileName) {
    //            return PDFDocument(url: resourceUrl)
    //        }
    //
    //        return nil
    
    /*
     Use the below code if you want to open a PDF from the remote URL.
     */
    
    if strInvoice != "" {
      if let resourceUrl = URL(string: strInvoice) {
        return PDFDocument(url: resourceUrl)
      }
      
    }
    
    return nil
  }
  
  private func displayPdf() {
    
    
    
    if #available(iOS 13.0, *) {
      let topPadding = UIApplication.shared.windows[0].safeAreaInsets.top
      let bottomPadding = UIApplication.shared.windows[0].safeAreaInsets.bottom
      
      print(topPadding)
      print(bottomPadding)
      
      let pdfView = self.createPdfView(withFrame: CGRect(x: self.view.bounds.origin.x, y: topPadding + self.topView.bounds.origin.y + self.topView.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height - topPadding - self.topView.bounds.height - bottomPadding))
        if let pdfDocument = self.createPdfDocument(forFileName: "heaps") {
            self.view.addSubview(pdfView)
            pdfView.document = pdfDocument
        }
    }

    
    
  }
  
}

//extension InvoiceVC {
//
//  /*
//   View PDF with WKWebView
//   */
//  private func createWebView(withFrame frame: CGRect) -> WKWebView? {
//      let webView = WKWebView(frame: frame)
//      webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
////        if let resourceUrl = self.resourceUrl(forFileName: "heaps") {
////            webView.loadFileURL(resourceUrl,
////                                allowingReadAccessTo: resourceUrl)
////
////            return webView
////        }
//
//      /*
//       Use the below if statement if you want to load the file from the remote URL
//       */ //https://web.stanford.edu/class/archive/cs/cs161/cs161.1168/lecture4.pdf
//      if let resourceUrl = URL(string: "https://d2tlw91f21wjoy.cloudfront.net/d/order_track/Invoice_2204539307?i=1630056256820") {
//          let request = URLRequest(url: resourceUrl)
//          webView.load(request)
//
//          return webView
//      }
//
//      return nil
//  }
//
//  private func displayWebView() {
//      if let webView = self.createWebView(withFrame: self.view.bounds) {
//          self.view.addSubview(webView)
//      }
//  }
//
//}
