//
//  VehicleDetailOrderVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import MapKit

class VehicleDetailOrderVC: UIViewController {
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet weak var lblOrderDate: UILabel!
  @IBOutlet weak var btnRoute: UIButton!
  @IBOutlet weak var btnDot: UIButton!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var widthConstraint: NSLayoutConstraint!
  
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var strOrderID: String? = nil
  var strTrackingID: String? = nil
  var isTM: Bool = false
  var alertIndex: Int = 0
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  
  // MARK: - setupView
  func setupView() {
  
    self.isTM = Helper.isTransitMixer()
    
    self.tblView.register(UINib(nibName: "LoadOrderCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.loadOrderCellID)
    self.tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    
    self.callGetOrderDetailsAPI()
    
    
  }
  
  
  // MARK: - Call Get Order API
  func callGetOrderDetailsAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
    
      if self.isTM == true {
       
        APIHandler.sharedInstance.getOrderDetails(strOrderID ?? "", strTrackID: strTrackingID ?? "") { (statusCode, response, error) in
          
          Helper.hideLoading(self.activityIndicator)
          
          if statusCode == 200 {
            
            if let orderResponse  = response?.order {
              
              self.dicOrderDetails = orderResponse
              
            }
            
          }else {
            Helper.showToast(_strMessage: error)
          }
          
          DispatchQueue.main.async {
            self.updateUI()
          }
          
        }
        
      }else {
        
        if let orderID = strOrderID, let trackingID = self.strTrackingID {
        
          APIHandler.sharedInstance.getCPOrderDetails(orderID, strTrackID: trackingID) { (statusCode, response, error) in
            
            Helper.hideLoading(self.activityIndicator)
            
            if statusCode == 200 {
              
              if let orderResponse  = response?.order {
                
                self.dicOrderDetails = orderResponse
                
              }
              
            }else {
              Helper.showToast(_strMessage: error)
            }
            
            DispatchQueue.main.async {
              self.updateUI()
            }
            
          }
          
        }
        
      }
    }
    else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
  
  // MARK: - Update UI
  func updateUI() {
    
    guard self.dicOrderDetails != nil else {
      self.lblOrderID.text = ""
      self.lblOrderDate.text = ""
      self.btnDot.isHidden = true
      let storyBoard = Helper.getStoryBorad()
      if let objNoDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC {
        objNoDataVC.view.frame = CGRect(x: 0.0, y: 44.0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 44)
        objNoDataVC.lblTitle.text = Constants.Text.NoOrderFound.localized()
        self.contentView.addSubview(objNoDataVC.view)
      }
      return
    }
    
    if let orderID = self.dicOrderDetails?.vendorOrderInfo?._id {
      
      let strOrderID = Constants.Text.ORDERID.localized()
      
      self.lblOrderID.text = String(format: "\(strOrderID) #%@", orderID)
    }
    
    if let createdAt = self.dicOrderDetails?.createdAt {
      self.lblOrderDate.text = Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: Constants.DateFormats.deliveryDate)
    }
    
    btnRoute.isHidden = false
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.isHidden = false
    self.tblView.reloadData()
    
  }
  
  @IBAction func actionBack(_ sender: Any) {
    
    
    if let viewControllers = self.navigationController?.viewControllers {
      
      for viewController in viewControllers {
        
        if let truckOrderListVC = viewController as? TruckOrderListVC {
          self.navigationController?.popToViewController(truckOrderListVC, animated: true)
        }
      }
    }
    
    //self.navigationController?.popViewController(animated: true)
  }
  
  //MARK:- Open Custom Alert Screen
  func openCustomAlert(_ index: Int, alert: CommonAlert = .cancelOrder) {
    
    self.alertIndex = index
    CommonAlertVC.showPopup(parentVC: self, options: alert)
    
  }
  
  @IBAction func actionRightMenu(_ sender: Any) {
    
    let btn = sender as! UIButton
    print(btn.tag)
    
    switch btn.tag {
    case 0:
      
      var pickupAddrs: [Double] = []
      var deliverAddrs: [Double] = []
      
      if let pickupAddressInfo = self.dicOrderDetails?.pickupAddressInfo {
        
        if let pickupLocation = pickupAddressInfo.pickupLocation {
          pickupAddrs.append((pickupLocation.coordinates?[0])!)
          pickupAddrs.append((pickupLocation.coordinates?[1])!)
        }
        
        
      }
      
      if let deliverAddressInfo = self.dicOrderDetails?.deliveryAddressInfo {
        
        if let deliveryLocation = deliverAddressInfo.deliveryLocation {
          deliverAddrs.append((deliveryLocation.coordinates?[0])!)
          deliverAddrs.append((deliveryLocation.coordinates?[1])!)
        }
      }
    
      if pickupAddrs.count > 0 && deliverAddrs.count > 0 {
        
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) { //comgooglemapsurl
          
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=\(pickupAddrs[1]),\(pickupAddrs[0])&daddr=\(deliverAddrs[1]),\(deliverAddrs[0])&directionsmode=driving&zoom=14&views=traffic")!)
            
          } else {
            //UIApplication.shared.openURL(url)
          }
          
        } else {
          let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: pickupAddrs[1], longitude: pickupAddrs[0])))
          
          let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: deliverAddrs[1], longitude: deliverAddrs[0])))
          
          MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
          
        }
      }
    case 1:
      
      let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
      
      let delayOrderAction = UIAlertAction(title: Constants.Text.DelayAlert.localized(), style: .default, handler: {
        (alert: UIAlertAction!) -> Void in
        
        let storyBoard = UIStoryboard(name: "DelayReasonVC", bundle: nil)
        if let objDelayReasonVC = storyBoard.instantiateViewController(withIdentifier:"DelayReasonVC") as? DelayReasonVC {
          objDelayReasonVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
          objDelayReasonVC.strTrackingID = self.dicOrderDetails?._id
          let navVC = UINavigationController(rootViewController:objDelayReasonVC)
          navVC.modalPresentationStyle = .fullScreen
          self.present(navVC, animated: true, completion: nil)
        }
        
      })
      
      let cancelOrderAction = UIAlertAction(title: Constants.Text.Complaint.localized(), style: .default, handler: {
        (alert: UIAlertAction!) -> Void in
        
        self.openCustomAlert(0, alert: .cancelOrder)
        
      })
      
      
      let cancelAction = UIAlertAction(title: Constants.Toast.cancel.localized(), style: .cancel, handler: {
        (alert: UIAlertAction!) -> Void in
      })
      
      delayOrderAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
      cancelOrderAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
      cancelAction.setValue(Constants.Colors.blackColor, forKey: "titleTextColor")
      
      alertController.addAction(delayOrderAction)
      
      if self.isTM == false {
        alertController.addAction(cancelOrderAction)
      }
      
      alertController.addAction(cancelAction)
      
      if #available(iOS 13.0, *) {
        alertController.overrideUserInterfaceStyle = .light
      } else {
        // Fallback on earlier versions
      }
      
      alertController.pruneNegativeWidthConstraints()
      self.present(alertController, animated: true, completion: nil)
      
      
    default:
      print(btn.tag)
    }
    
  }
  
  
  @objc func actionSupplierMobNo(_ tapGesture: UITapGestureRecognizer) {
    
    if let supplierInfo = self.dicOrderDetails?.vendorInfo {
      
      if let mobile = supplierInfo.mobileNumber {
        
        Helper.tapOnMobileNumber(mobile)
        
      }
      
    }
    
  }
   
  @objc func actionBuyerMobNo(_ tapGesture: UITapGestureRecognizer) {
    
    if let buyerInfo = self.dicOrderDetails?.buyerInfo {
      
      if let mobile = buyerInfo.mobileNumber {
        
        Helper.tapOnMobileNumber(mobile)
        
      }
      
    }
    
  }
  
  @objc func actionPickup(_ tapGesture: UITapGestureRecognizer) {
    
    if let pickUpAddressInfo = self.dicOrderDetails?.pickupAddressInfo {
      
      if let pickUpLocation = pickUpAddressInfo.pickupLocation {

        let storyBoard = UIStoryboard(name: "MapRouteVC", bundle: nil)

        if let objMapRouteVC = storyBoard.instantiateViewController(withIdentifier:"MapRouteVC") as? MapRouteVC {
          objMapRouteVC.fromScreen = .truckdetails
          objMapRouteVC.dicPickUpLocation = pickUpLocation
          self.navigationController?.pushViewController(objMapRouteVC, animated: true)
          
        }
        
      }else {
        Helper.DLog(message: "pickup location not found")
        return
      }
      
    }
    
  }
  
  @objc func actionDelivery(_ tapGesture: UITapGestureRecognizer) {
    
    if let deliveryAddressInfo = self.dicOrderDetails?.deliveryAddressInfo {
      
      if let deliveryLocation = deliveryAddressInfo.deliveryLocation {
    
        let storyBoard = UIStoryboard(name: "MapRouteVC", bundle: nil)

        if let objMapRouteVC = storyBoard.instantiateViewController(withIdentifier:"MapRouteVC") as? MapRouteVC {
          objMapRouteVC.fromScreen = .truckdetails
          objMapRouteVC.dicPickUpLocation = deliveryLocation
          self.navigationController?.pushViewController(objMapRouteVC, animated: true)
          
        }
        
      }
      
    }
  }
  
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension VehicleDetailOrderVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.loadOrderCellID, for: indexPath) as? LoadOrderCell else {
      return UITableViewCell()
    }
    
    objCell.delegate = self
    
    objCell.dottedLineView[0].backgroundColor = UIColor.clear
    objCell.dottedLineView[1].backgroundColor = UIColor.clear
    
    objCell.dottedLineView[0].createDottedLine(x: UIScreen.main.bounds.size.width - 20, width: 1.0, color: UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 193.0/255.0, alpha: 1.0).cgColor)
    objCell.dottedLineView[1].createDottedLine(x: UIScreen.main.bounds.size.width - 20, width: 1.0, color: UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 193.0/255.0, alpha: 1.0).cgColor)
    
    objCell.loadOrderView.isHidden = true
    objCell.bottomStackView.isHidden = false
    objCell.btnPickup.isUserInteractionEnabled = false
    if let order = self.dicOrderDetails {
      objCell.configureCell(order, isTransitMixer: self.isTM)
    }
    
    
    let tapGestureSupplier = UITapGestureRecognizer(target: self, action: #selector(self.actionSupplierMobNo(_:)))
    objCell.lblSupplierContact.isUserInteractionEnabled = true
    objCell.lblSupplierContact.addGestureRecognizer(tapGestureSupplier)
    
    let tapGestureBuyer = UITapGestureRecognizer(target: self, action: #selector(actionBuyerMobNo(_:)))
    objCell.lblBuyerContact.isUserInteractionEnabled = true
    objCell.lblBuyerContact.addGestureRecognizer(tapGestureBuyer)
    
    let tapGesturePickup = UITapGestureRecognizer(target: self, action: #selector(self.actionPickup(_:)))
    objCell.pickUpStackView.isUserInteractionEnabled = true
    objCell.pickUpStackView.addGestureRecognizer(tapGesturePickup)
    
    let tapGestureDelivery = UITapGestureRecognizer(target: self, action: #selector(self.actionDelivery(_:)))
    objCell.deliveryStackView.isUserInteractionEnabled = true
    objCell.deliveryStackView.addGestureRecognizer(tapGestureDelivery)
    
    return objCell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
  
}


// MARK: -  LoadOrderViewDelegate
extension VehicleDetailOrderVC: LoadOrderViewDelegate {
  
  func actionBottomView(_ tag: Int) {
  
    switch tag {
    case 0:
      /*APIHandler.shared.updateOrderStatus(dicOrder?._id ?? "") { (statusCode, error) in
        
        guard statusCode == 202 else {
          Helper.showToast(_strMessage: error)
          return
        }
        
        Helper.showToast(_strMessage: "Change order status as pickup")
      }*/
      print("Pickup")
    case 1:
      let storyBoard = UIStoryboard(name: "DelayReasonVC", bundle: nil)
      if let objDelayReasonVC = storyBoard.instantiateViewController(withIdentifier:"DelayReasonVC") as? DelayReasonVC {
        objDelayReasonVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
        objDelayReasonVC.strTrackingID = self.dicOrderDetails?._id
        let navVC = UINavigationController(rootViewController:objDelayReasonVC)
        navVC.modalPresentationStyle = .fullScreen
        self.present(navVC, animated: true, completion: nil)
      }
      
    case 2:
      
//      APIHandler.shared.deliverOrder(strVehicleID ?? "", strOrderID: orderDetailsViewModel.orderDetails?.logistics_order_info?._id ?? "") { (statusCode, error) in
//
//        if statusCode == 202 {
//          Helper.showToast(_strMessage: "Order delivered successfully")
//
//          if let objOrderInfoVC = Constants.kMainStoryBoard.instantiateViewController(withIdentifier:"OrderInfoVC") as? OrderInfoVC {
//            objOrderInfoVC.strOrderID = self.orderDetailsViewModel.orderDetails?.logistics_order_info?._id
//            objOrderInfoVC.fromScreen = .delayReason
//            self.navigationController?.pushViewController(objOrderInfoVC, animated: true)
//          }
//        } else {
//          Helper.showToast(_strMessage: error)
//          return
//        }
//
//
//      }
      
      if self.isTM == true {
        
        let storyBoard = UIStoryboard(name: "OrderInfoVC", bundle: nil)
        if let objOrderInfoVC = storyBoard.instantiateViewController(withIdentifier:"OrderInfoVC") as? OrderInfoVC {
          objOrderInfoVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
          objOrderInfoVC.strTrackingID = self.dicOrderDetails?._id
          objOrderInfoVC.fromScreen = .delayReason
          self.navigationController?.pushViewController(objOrderInfoVC, animated: true)
        }
        
      }else {
        
        /*let storyBoard = UIStoryboard(name: "UploadDocumentVC", bundle: nil)
        if let objUploadDocumentVC = storyBoard.instantiateViewController(withIdentifier:"UploadDocumentVC") as? UploadDocumentVC {
          objUploadDocumentVC.dicOrderDetails = self.dicOrderDetails
          self.navigationController?.pushViewController(objUploadDocumentVC, animated: true)
        }*/
        
        /*let storyBoard = UIStoryboard(name: "DrawSignatureVC", bundle: nil)
        if let objDrawSignatureVC = storyBoard.instantiateViewController(withIdentifier:"DrawSignatureVC") as? DrawSignatureVC {
          objDrawSignatureVC.isForDropOrder = true
          objDrawSignatureVC.dicOrderDetails = self.dicOrderDetails
          self.navigationController?.pushViewController(objDrawSignatureVC, animated: true)
        }*/
        
        
        
        self.callSendOTPAPI("ACCEPTED")
        
        
       
        
      }
    default:
      Helper.DLog(message: "\(tag)")
    }
    
    
  }
  
  
  func actionLoadOrder() {
    print("Order")
  }
  
  
  // MARK: - CALL SEND OTP
  func callSendOTPAPI(_ deliveryStatus: String) {
    
    if let buyerInfo = self.dicOrderDetails?.buyerInfo {
      
      if let mobileNumber = buyerInfo.mobileNumber {
        
        APIHandler.sharedInstance.requestOTPWithMobile(mobileNumber) { (statuscode, response, error) in
          
          if statuscode == 200 {
            if let code = response?.code, code != "" {
              
              Helper.showToast(_strMessage: Constants.SuccessMessage.codesuccess.localized())
              
              let storyBoard = UIStoryboard(name: "MobileOTPVC", bundle: nil)
             
              if let objMobileOTPVC = storyBoard.instantiateViewController(withIdentifier:"MobileOTPVC") as? MobileOTPVC {
                objMobileOTPVC.strDeliveryStatus = deliveryStatus
                objMobileOTPVC.dicOrderDetails = self.dicOrderDetails
                self.navigationController?.pushViewController(objMobileOTPVC, animated: true)
              }
              
            }
          }
          else {
            Helper.showToast(_strMessage: error)
          }
          
        }
        
      }
      
    }
  }
  
}


//MARK:- PopUpProtocol
extension VehicleDetailOrderVC: PopUpProtocol {
  
  func handleAction(action: Bool) {
    if (action) {
      
      if self.alertIndex == 0 {
        
        let storyBoard = UIStoryboard(name: "RejectReasonVC", bundle: nil)
        if let objRejectReasonVC = storyBoard.instantiateViewController(withIdentifier:"RejectReasonVC") as? RejectReasonVC {
          objRejectReasonVC.delegate = self
          objRejectReasonVC.strDeliveryStatus = "REJECTED"
          objRejectReasonVC.dicOrderDetails = self.dicOrderDetails
          objRejectReasonVC.modalPresentationStyle = .fullScreen
          self.present(objRejectReasonVC, animated: true, completion: nil)
          
        }
        
      }else {
        self.callSendOTPAPI("ACCEPTED")
        //self.callUpdateOrderStatusAPI()
      }
    }
  }
  
}

// MARK: - RejectReasonDelegate
extension VehicleDetailOrderVC: RejectReasonDelegate {
 
  func actionButton(_ strText: String, viewController: UIViewController) {
    
    let stroyBoard = UIStoryboard(name: "MobileOTPVC", bundle: nil)
    if let objMobileOTPVC = stroyBoard.instantiateViewController(withIdentifier:"MobileOTPVC") as? MobileOTPVC {
      objMobileOTPVC.strRejectReason = strText
      objMobileOTPVC.strDeliveryStatus = "REJECTED"
      objMobileOTPVC.dicOrderDetails = self.dicOrderDetails
      self.navigationController?.pushViewController(objMobileOTPVC, animated: true)
      
    }
  }

}
