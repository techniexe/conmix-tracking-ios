//
//  VehicleDetailLoadOrderVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 21/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class VehicleDetailLoadOrderVC: UIViewController {
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet weak var lblOrderDate: UILabel!
  @IBOutlet weak var tblView: UITableView!
  @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
  
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  var strVehicleId: String?
  var strOrderID: String? = nil
  var strTrackingID: String? = nil
  var isTM: Bool = false
  
  // MARK: - View LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
    // Do any additional setup after loading the view.
  }
  
  // MARK: - setupView
  func setupView() {
  
    self.isTM =  Helper.isTransitMixer()
    
    self.tblView.register(UINib(nibName: "LoadOrderCell", bundle: nil), forCellReuseIdentifier: Constants.TableViewCellIdentifier.loadOrderCellID)
    
    if self.isTM == true {
      
      self.callGetOrderByVehicleIdAPI()
      
    }else {
     
      self.callGetOrderByCPIdAPI()
    }
    
    
  }
  
  
  // MARK: - Call Get Order API
  func callGetOrderByVehicleIdAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
      
      APIHandler.sharedInstance.getOrderDetails(strOrderID ?? "", strTrackID: strTrackingID ?? "") { (statuscode, response, error) in
        
        Helper.hideLoading(self.activityIndicator)
        
        if statuscode == 200 {
          
          if let orderResponse  = response?.order {
            
            self.dicOrderDetails = orderResponse
            
          }
          
        }else {
          Helper.showToast(_strMessage: error)
        }
        
        
        
        DispatchQueue.main.async {
          self.updateUI()
        }
      }
      
    }
    else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
  
  // MARK: - Call Get Order API
  func callGetOrderByCPIdAPI() {
    
    if Reachability.isConnectedToNetwork() {
      
      Helper.showLoading(self.activityIndicator)
      
      if let orderID = strOrderID, let trackingID = self.strTrackingID {
        
        APIHandler.sharedInstance.getCPOrderDetails(orderID, strTrackID: trackingID) { (statuscode, response, error) in
          
          Helper.hideLoading(self.activityIndicator)
          
          if statuscode == 200 {
            
            if let orderResponse  = response?.order {
              
              self.dicOrderDetails = orderResponse
              
            }
            
          }else {
            Helper.showToast(_strMessage: error)
          }
          
          
          
          DispatchQueue.main.async {
            self.updateUI()
          }
        }
        
      }
      
      
      
      
    }
    else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
  }
  
  
  // MARK: - Update UI
  func updateUI() {
    
    guard self.dicOrderDetails != nil else {
      let storyBoard = Helper.getStoryBorad()
      if let objNoDataVC = storyBoard.instantiateViewController(withIdentifier: "NoDataVC") as? NoDataVC {
        objNoDataVC.view.frame = CGRect(x: 0.0, y: 44.0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 44)
        if self.isTM == true {
          objNoDataVC.lblTitle.text = Constants.Text.NoAssignOrder.localized()
        }else {
          objNoDataVC.lblTitle.text = Constants.Text.NoAssignCPOrder.localized()
        }
        self.contentView.addSubview(objNoDataVC.view)
      }
      return
    }
    
    if let strorderID = self.dicOrderDetails?.vendorOrderInfo?._id { //loadOrderViewModel.orderByVehicle?.logistics_order_info?._id {
      let orderID = Constants.Text.ORDERID.localized()
      self.lblOrderID.text = String(format: "\(orderID) #%@", strorderID)
    }
    
    if let createdAt = self.dicOrderDetails?.createdAt {  //loadOrderViewModel.orderByVehicle?.created_at {
      self.lblOrderDate.text = "\(Constants.Text.AssignedOn.localized()) : \(Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: Constants.DateFormats.deliveryDate))"
    }
    
    self.tblView.delegate = self
    self.tblView.dataSource = self
    self.tblView.isHidden = false
    self.tblView.reloadData()
    
  }
 
  
  @IBAction func actionBack(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
 
  @objc func actionSupplierMobNo(_ tapGesture: UITapGestureRecognizer) {
    
    if let supplierInfo = self.dicOrderDetails?.vendorInfo {
      
      if let mobile = supplierInfo.mobileNumber {
        
        Helper.tapOnMobileNumber(mobile)
        
      }
      
    }
    
  }
  
  @objc func actionBuyerMobNo(_ tapGesture: UITapGestureRecognizer) {
   
    if let buyerInfo = self.dicOrderDetails?.buyerInfo {
      
      if let mobile = buyerInfo.mobileNumber {
        
        Helper.tapOnMobileNumber(mobile)
        
      }
      
    }
    
  }
  
  @objc func actionPickup(_ tapGesture: UITapGestureRecognizer) {
    
    if let pickUpAddressInfo = self.dicOrderDetails?.pickupAddressInfo {
      
      if let pickUpLocation = pickUpAddressInfo.pickupLocation {

        let storyBoard = UIStoryboard(name: "MapRouteVC", bundle: nil)

        if let objMapRouteVC = storyBoard.instantiateViewController(withIdentifier:"MapRouteVC") as? MapRouteVC {
          objMapRouteVC.fromScreen = .truckdetails
          objMapRouteVC.dicPickUpLocation = pickUpLocation
          self.navigationController?.pushViewController(objMapRouteVC, animated: true)
          
        }
        
      }else {
        Helper.DLog(message: "pickup location not found")
        return
      }
      
    }
    
  }
  
  @objc func actionDelivery(_ tapGesture: UITapGestureRecognizer) {
    
    if let deliveryAddressInfo = self.dicOrderDetails?.deliveryAddressInfo {
      
      if let deliveryLocation = deliveryAddressInfo.deliveryLocation {
    
        let storyBoard = UIStoryboard(name: "MapRouteVC", bundle: nil)

        if let objMapRouteVC = storyBoard.instantiateViewController(withIdentifier:"MapRouteVC") as? MapRouteVC {
          objMapRouteVC.fromScreen = .truckdetails
          objMapRouteVC.dicPickUpLocation = deliveryLocation
          self.navigationController?.pushViewController(objMapRouteVC, animated: true)
          
        }
        
      }
      
    }
  }
}


// MARK: - UITableViewDelegate, UITableViewDataSource
extension VehicleDetailLoadOrderVC: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let objCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCellIdentifier.loadOrderCellID, for: indexPath) as? LoadOrderCell else {
      return UITableViewCell()
    }
    
    objCell.delegate = self

    objCell.dottedLineView[0].backgroundColor = UIColor.clear
    objCell.dottedLineView[1].backgroundColor = UIColor.clear

    objCell.dottedLineView[0].createDottedLine(x: UIScreen.main.bounds.size.width - 20, width: 1.0, color: UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 193.0/255.0, alpha: 1.0).cgColor)
    objCell.dottedLineView[1].createDottedLine(x: UIScreen.main.bounds.size.width - 20, width: 1.0, color: UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 193.0/255.0, alpha: 1.0).cgColor)
    
    objCell.loadOrderView.isHidden = false
    objCell.bottomStackView.isHidden = true
    
    let tapGestureSupplier = UITapGestureRecognizer(target: self, action: #selector(self.actionSupplierMobNo(_:)))
    objCell.lblSupplierContact.isUserInteractionEnabled = true
    objCell.lblSupplierContact.addGestureRecognizer(tapGestureSupplier)
    
    let tapGestureBuyer = UITapGestureRecognizer(target: self, action: #selector(actionBuyerMobNo(_:)))
    objCell.lblBuyerContact.isUserInteractionEnabled = true
    objCell.lblBuyerContact.addGestureRecognizer(tapGestureBuyer)
    
    
    let tapGesturePickup = UITapGestureRecognizer(target: self, action: #selector(self.actionPickup(_:)))
    objCell.pickUpStackView.isUserInteractionEnabled = true
    objCell.pickUpStackView.addGestureRecognizer(tapGesturePickup)
    
    let tapGestureDelivery = UITapGestureRecognizer(target: self, action: #selector(self.actionDelivery(_:)))
    objCell.deliveryStackView.isUserInteractionEnabled = true
    objCell.deliveryStackView.addGestureRecognizer(tapGestureDelivery)
    
    
    if let order = self.dicOrderDetails {  //loadOrderViewModel.orderByVehicle {
      objCell.configureCell(order, isTransitMixer: self.isTM)
    }
    
    return objCell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

   }
}


// MARK: -  LoadOrderViewDelegate
extension VehicleDetailLoadOrderVC: LoadOrderViewDelegate {
  func actionBottomView(_ tag: Int) {
    print(tag)
  }
  
  
  func actionLoadOrder() {
    
    if Reachability.isConnectedToNetwork() {
      
      /*let storyBoard = UIStoryboard(name: "ChangeOrderQtyVC", bundle: nil)
      
      if let objChangeOrderQtyVC = storyBoard.instantiateViewController(withIdentifier:"ChangeOrderQtyVC") as? ChangeOrderQtyVC {
        objChangeOrderQtyVC.dicOrderDetails = self.dicOrderDetails
        self.navigationController?.pushViewController(objChangeOrderQtyVC, animated: true)
      }*/
    
      if self.isTM == true {
    
        if let orderID = strOrderID {
         
          APIHandler.sharedInstance.checkCPStatus(orderID) { (statuscode, error) in
            
            if statuscode == 202 {
              
//              let storyBoard = UIStoryboard(name: "OrderInfoVC", bundle: nil)
//              if let objOrderInfoVC = storyBoard.instantiateViewController(withIdentifier:"OrderInfoVC") as? OrderInfoVC {
//                objOrderInfoVC.strOrderID = self.dicOrderDetails?.vendorOrderInfo?._id
//                objOrderInfoVC.strTrackingID = self.dicOrderDetails?._id
//                self.navigationController?.pushViewController(objOrderInfoVC, animated: true)
//              }
              
              let storyBoard = UIStoryboard(name: "UploadDocumentVC", bundle: nil)
              if let objUploadDocumentVC = storyBoard.instantiateViewController(withIdentifier:"UploadDocumentVC") as? UploadDocumentVC {
                objUploadDocumentVC.dicOrderDetails = self.dicOrderDetails
                self.navigationController?.pushViewController(objUploadDocumentVC, animated: true)
              }
              
            }else {
              Helper.showToast(_strMessage: error)
            }
            
          }
          
        }
        
      }else {
        
        let storyBoard = UIStoryboard(name: "UploadDocumentVC", bundle: nil)
        if let objUploadDocumentVC = storyBoard.instantiateViewController(withIdentifier:"UploadDocumentVC") as? UploadDocumentVC {
          objUploadDocumentVC.dicOrderDetails = self.dicOrderDetails
          self.navigationController?.pushViewController(objUploadDocumentVC, animated: true)
        }
        
      }
      
      
    }else {
      Helper.showToast(_strMessage: Constants.ErrorMessage.noNetwork.localized())
    }
    
    
    
  }
  
}

