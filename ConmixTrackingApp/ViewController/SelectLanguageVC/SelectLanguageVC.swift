//
//  SelectLanguageVC.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 04/02/21.
//  Copyright © 2021 Techniexe Infolabs. All rights reserved.
//

import UIKit
import DLRadioButton

class SelectLanguageVC: UIViewController {
  
  // MARK: - @IBOutlet
  @IBOutlet weak var viewContainer: UIView!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var btnVeify: UIButton!
  @IBOutlet var btnRadioCollection: [DLRadioButton]!
  
  // MARK: - @vars
  var selectedIndex: Int = 0
  
  // MARK: - View Life Cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    // Do any additional setup after loading the view.
  }
 
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewContainer.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 4)
    
  }
  
  // MARK: - setUp View
  func setupView() {
    self.lblTitle.text = Constants.Text.SelectLanguage.localized()
    self.btnVeify.setTitle(Constants.Text.Apply.localized(), for: .normal)
    
    let currentLanguage =  Localize.currentLanguage()
    
    if currentLanguage == "en" {
      self.btnRadioCollection[0].isSelected = true
      self.btnRadioCollection[1].isSelected = false
      selectedIndex = 0
    }else {
      self.btnRadioCollection[0].isSelected = false
      self.btnRadioCollection[1].isSelected = true
      selectedIndex = 1
    }
    
  }
  
  // MARK: - @IBAction
  @IBAction func actionRadioButton(_ sender: Any) {
    
    let btn = sender as! UIButton
    selectedIndex = btn.tag
    
    switch selectedIndex {
    case 0:
      self.btnRadioCollection[0].isSelected = true
      self.btnRadioCollection[1].isSelected = false
      selectedIndex = 0
    case 1:
      self.btnRadioCollection[0].isSelected = false
      self.btnRadioCollection[1].isSelected = true
      selectedIndex = 1
    default:
      break
    }
  }
  
  @IBAction func actionApply(_ sender: Any) {
    
    switch selectedIndex {
    case 0:
      Localize.setCurrentLanguage("en")
    case 1:
      Localize.setCurrentLanguage("hi")
    default:
      break
    }
    
    self.dismiss(animated: true, completion: nil)
    
  }
  
  
  @IBAction func actionClose(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
}
