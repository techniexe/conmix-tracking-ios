//
//  ImageViewCollectionCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 23/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class ImageViewCollectionCell: UICollectionViewCell {
  
  @IBOutlet weak var docImgView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  // MARK: - Configure Cell
  func configureCell(_ strurl: String) {
    self.docImgView.setImage(url: strurl)
  }
}
