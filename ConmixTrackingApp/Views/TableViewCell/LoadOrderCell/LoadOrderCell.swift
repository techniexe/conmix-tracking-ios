//
//  LoadOrderCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 21/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

@objc protocol LoadOrderViewDelegate: AnyObject {
 
  func actionLoadOrder()
  func actionBottomView(_ tag: Int)

}

class LoadOrderCell: UITableViewCell {
  
  @IBOutlet weak var imgVehicle: CustomImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblType: UILabel!
  @IBOutlet weak var lblNumber: UILabel!
  @IBOutlet weak var lblOrderQty: UILabel!
  @IBOutlet weak var lblSupplierName: UILabel!
  @IBOutlet weak var lblSupplierCompanyName: UILabel!
  @IBOutlet weak var lblSupplierContact: UILabel!
  @IBOutlet weak var lblPickupLocation: UILabel!
  @IBOutlet weak var lblDeliveryLocation: UILabel!
  @IBOutlet weak var lblBuyerName: UILabel!
  @IBOutlet weak var lblBuyerCompanyName: UILabel!
  @IBOutlet weak var lblBuyerContact: UILabel!
  @IBOutlet var dottedLineView: [UIView]!
  @IBOutlet var loadOrderView: UIView!
  @IBOutlet var bottomStackView: UIStackView!
  @IBOutlet var btnPickup: UIButton!
  @IBOutlet var btnDelay: UIButton!
  @IBOutlet var btnOrderDeliver: UIButton!
  @IBOutlet var btnLoadOrder: UIButton!
  @IBOutlet var supplierCompanyView: UIView!
  @IBOutlet var buyerCompanyView: UIView!
  
  @IBOutlet var lblSupplierInfoTitleCollection: [UILabel]!
  @IBOutlet var lblLocationInfoTitleCollection: [UILabel]!
  @IBOutlet var lblBuyerInfoTitleCollection: [UILabel]!
  
  @IBOutlet var deliveryStackView: UIStackView!
  @IBOutlet var pickUpStackView: UIStackView!
  
  var delegate : LoadOrderViewDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    
    lblSupplierInfoTitleCollection[0].text = Constants.Text.SupplierInfo.localized()
    //lblSupplierInfoTitleCollection[1].text = Constants.Text.PickupQty.localized()
    lblSupplierInfoTitleCollection[2].text = Constants.Text.CompanyName.localized()
    lblSupplierInfoTitleCollection[3].text = Constants.Text.SupplierName.localized()
    lblSupplierInfoTitleCollection[4].text = Constants.Text.SupplierContact.localized()
    
    lblLocationInfoTitleCollection[0].text = Constants.Text.PickupLocation.localized()
    lblLocationInfoTitleCollection[1].text = Constants.Text.DeliveryLocation.localized()
    
    lblBuyerInfoTitleCollection[0].text = Constants.Text.BuyerInfo.localized()
    lblBuyerInfoTitleCollection[1].text = Constants.Text.CompanyName.localized()
    lblBuyerInfoTitleCollection[2].text = Constants.Text.BuyerName.localized()
    lblBuyerInfoTitleCollection[3].text = Constants.Text.BuyerContact.localized()
    
    btnPickup.setTitle(Constants.Text.PickUp.localized(), for: .normal)
    btnDelay.setTitle(Constants.Text.Delay.localized(), for: .normal)
    btnOrderDeliver.setTitle(Constants.Text.OrderDelivered.localized(), for: .normal)
    
    btnLoadOrder.setTitle(Constants.Text.LoadOrder.localized(), for: .normal)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MAR: - Configure Cell
  func configureCell(_ dicOrder: VendorOrderDetailsForTracking, isTransitMixer: Bool) {
    
    if isTransitMixer == true {
      self.lblSupplierInfoTitleCollection[1].text = Constants.Text.PickupQty.localized()
    }else {
      self.lblSupplierInfoTitleCollection[1].text = Constants.Text.VehicleSubCategory.localized()
    }
    
    
    if let vCategoryInfo = dicOrder.tMCategoryInfo {
      
      if let name = vCategoryInfo.tMCategoryName {
        self.lblType.text = String(format: "%@", name)
      }
      
    }
    

    /*if let vSubCategorInfo = dicOrder.tMSubCategoryInfo {
      
      //if let minloadCapacity = vSubCategorInfo.min_load_capacity, let maxloadCapacity = vSubCategorInfo.max_load_capacity {
        
        if let name = vSubCategorInfo.tMSubCategoryName {
          
          //let minload = Helper.convertNumberToString(NSNumber(value: minloadCapacity))
          //let maxload = Helper.convertNumberToString(NSNumber(value: maxloadCapacity))
          
          
          self.lblType.text = String(format: "%@", arguments: [name])
          //self.lblType.text = String(format: "%@ - (%@ MT to %@ MT)", arguments: [name, minload, maxload])
        }
      //}
      
    }*/
    
    if let tmInfo = dicOrder.tMInfo {
      
      if let vRCNumber = tmInfo.tMRCNumber {
        self.lblNumber.text = String(format: "%@", vRCNumber)
      }
      
      if let imgurl = tmInfo.tMImageUrl {
        self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
      }
      
    }
    
    
    
    if let cpInfo = dicOrder.CPInfo {
      
      
      
      if let cpCapacity = cpInfo.concretePumpCapacity {
        self.lblNumber.text = "" //Helper.getCapacity(NSNumber(value: cpCapacity))
        self.lblOrderQty.text = Helper.getCapacity(NSNumber(value: cpCapacity))
      }
      
      if let imgurl = cpInfo.concretePumpImageUrl {
        self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
      }
      
      var strCompanyName: String = ""
      
      
      if let companyName = cpInfo.companyName {
        
        strCompanyName = companyName
        
      }
      
      if let concretePumpModel = cpInfo.concretePumpModel {
        
        if strCompanyName != "" {
          
          strCompanyName += " - \(concretePumpModel)"
          
        }else {
          
          strCompanyName += "\(concretePumpModel)"
          
        }
        
      }
      
      self.lblType.text = String(format: "%@", strCompanyName)
    }
    
    
    if let cpCategoryInfo = dicOrder.CPCategoryInfo {
      
      if let name = cpCategoryInfo.categoryName {
        self.lblName.text = String(format: "%@", name)
      }
      
    }
    
    if let pickupqty = dicOrder.pickupQuantity {
      let qty = Helper.getQuantity(NSNumber(value: pickupqty))
      self.lblOrderQty.text = String(format: "%@", qty)
      //self.lblOrderQty.text = String(format: "%.01f Cu.Mtr", pickupqty)
    }
    
    if let suppilerInfo = dicOrder.vendorInfo {
      
      if let companyName = suppilerInfo.companyName, companyName != "" {
        self.lblSupplierCompanyName.text = String(format: "%@", companyName)
        supplierCompanyView.isHidden = false
      }else {
        supplierCompanyView.isHidden = true
      }
      
      if let name = suppilerInfo.fullName {
        self.lblSupplierName.text = String(format: "%@", name)
      }
      if let number = suppilerInfo.mobileNumber {
        self.lblSupplierContact.text = String(format: "%@", number)
      }
    }
    
    if let buyerInfo = dicOrder.buyerInfo {
      
      if let companyName = buyerInfo.companyName {
        self.lblBuyerCompanyName.text = String(format: "%@", companyName)
        buyerCompanyView.isHidden = false
      }else {
        buyerCompanyView.isHidden = true
      }
      
  
      if let name = buyerInfo.fullName {
        self.lblBuyerName.text = String(format: "%@", name)
      }
      if let number = buyerInfo.mobileNumber {
        self.lblBuyerContact.text = String(format: "%@", number)
      }
    }
    
    if let pickupAddressInfo = dicOrder.pickupAddressInfo {
      
      let strAddress = Helper.getAddress(pickupAddressInfo.line1, strLine2: pickupAddressInfo.line2, strCityName: pickupAddressInfo.cityName, strStateName: pickupAddressInfo.stateName, pincode: pickupAddressInfo.pincode)
      
      self.lblPickupLocation.text = strAddress
    }
   
    
    
    if let deliveryAddressInfo = dicOrder.deliveryAddressInfo {
      
      let strAddress = Helper.getAddress(deliveryAddressInfo.line1, strLine2: deliveryAddressInfo.line2, strCityName: deliveryAddressInfo.cityName, strStateName: deliveryAddressInfo.stateName, pincode: deliveryAddressInfo.pincode)
      
      self.lblDeliveryLocation.text = strAddress
      
    }
    
  }
  
  
  @IBAction func actionLoadOrder(_ sender: Any) {
    self.delegate?.actionLoadOrder()
  }
  
  @IBAction func actionBottomStackView(_ sender: Any) {
    let btn = sender as! UIButton
    self.delegate?.actionBottomView(btn.tag)
  }
  
}
