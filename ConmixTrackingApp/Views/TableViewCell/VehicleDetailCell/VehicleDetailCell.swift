//
//  VehicleDetailCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 20/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//



import UIKit

protocol VehicleDetailsDelegate {
  
  func actionVehicleLocation()
  func actionPickup()
}


class VehicleDetailCell: UITableViewCell {

  var delegate: VehicleDetailsDelegate?
  
  @IBOutlet weak var imgVehicle: CustomImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblType: UILabel!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var lblNumber: UILabel!
  @IBOutlet weak var lblSubCategory: UILabel!
  @IBOutlet weak var lblMFName: UILabel!
  @IBOutlet weak var lblMFYear: UILabel!
  @IBOutlet weak var lblModelName: UILabel!
  @IBOutlet weak var lblDriver1Name: UILabel!
  @IBOutlet weak var lblDriver1Contact: UILabel!
  @IBOutlet weak var lblDriver2Name: UILabel!
  @IBOutlet weak var lblDriver2Contact: UILabel!
  @IBOutlet weak var btnPlantLocation: UIButton!
  @IBOutlet weak var lblPlantLocation: UILabel!
  @IBOutlet weak var btnCurrentLocation: UIButton!
  @IBOutlet weak var lblCurrentLocation: UILabel!
  @IBOutlet weak var btnNext: UIButton!
  
  @IBOutlet weak var driver1NameView: UIView!
  @IBOutlet weak var driver1ContactView: UIView!
  @IBOutlet weak var driver2NameView: UIView!
  @IBOutlet weak var driver2ContactView: UIView!
  @IBOutlet weak var plantLocationView: UIView!
  @IBOutlet weak var currentLocationView: UIView!
  
  @IBOutlet var lblTitle: [UILabel]!
  @IBOutlet var lineView: [UIView]!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    self.lblTitle[0].text = Constants.Text.VehicleSubCategory.localized()

    self.lblTitle[1].text = Constants.Text.ManufacturerName.localized()

    self.lblTitle[2].text = Constants.Text.ManufacturingYear.localized()

    self.lblTitle[3].text = Constants.Text.VehicleModel.localized()

    self.lblTitle[4].text = Constants.Text.Driver1Name.localized()

    self.lblTitle[5].text = Constants.Text.Driver1Contact.localized()

    self.lblTitle[6].text = Constants.Text.Driver2Name.localized()

    self.lblTitle[7].text = Constants.Text.Driver2Contact.localized()

    self.lblTitle[8].text = Constants.Text.PlantLocation.localized()
    
    self.lblTitle[8].text = Constants.Text.PlantLocation.localized()
    
    self.lblTitle[9].text = Constants.Text.CurrentLocation.localized()

    self.btnNext.setTitle(Constants.Text.Next.localized(), for: .normal)
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: - Configure Cell
  func configureCell(_ objVDetails: TMDetails) {
    
    if let vCategory = objVDetails.tMCategory {
      
      if let name = vCategory.tMCategoryName {
        self.lblType.text = String(format: "%@", name)
      }
      
    }
    
    
    if let vSubCategory = objVDetails.tMSubCategory {
      
      //if let minloadCapacity = vSubCategory.min_load_capacity, let maxloadCapacity = vSubCategory.max_load_capacity {
        
        if let name = vSubCategory.tMSubCategoryName{
          
          self.lblSubCategory.text = String(format: "%@", name)
          //let minload = Helper.convertNumberToString(NSNumber(value: minloadCapacity))
          //let maxload = Helper.convertNumberToString(NSNumber(value: maxloadCapacity))
          //self.lblType.text = String(format: "%@", arguments: [name])
          //self.lblType.text = String(format: "%@ - (%@ MT to %@ MT)", arguments: [name, minload, maxload])
        }
      //}
      
    }
    
    
    if let vRCNumber = objVDetails.tMRcNumber {
      self.lblNumber.text = String(format: "%@", vRCNumber)
    }
    
    /*if let createdAt = objVDetails.created_at {
      self.lblDate.text = Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: "dd MMM yyyy")
    }*/
    
    if let mfname = objVDetails.manufacturerName {
      self.lblMFName.text = String(format: "%@", mfname)
    }
    
    if let mfYear = objVDetails.manufactureYear {
      let model = Helper.convertNumberToYear(NSNumber(value: mfYear))
      self.lblMFYear.text = String(format: "%@", model)
    }
    
    if let vehiclemodel = objVDetails.tMModel {
      self.lblModelName.text = String(format: "%@", vehiclemodel)
    }
  
    if let driver1Info = objVDetails.driver1Info  {
      
      if let name = driver1Info.driverName {
        self.lblDriver1Name.text = String(format: "%@", name)
      }else {
         self.driver1NameView.isHidden = true
      }
      
      if let contact = driver1Info.driverMobileNumber {
        self.lblDriver1Contact.text = String(format: "%@", contact)
      }else {
        self.driver1ContactView.isHidden = true
      }
      
    }else {
      self.driver1NameView.isHidden = true
      self.driver1ContactView.isHidden = true
    }
    
    if let driver2Info = objVDetails.driver2Info  {
      
      if let name = driver2Info.driverName {
        self.lblDriver2Name.text = String(format: "%@", name)
      }else {
         self.driver2NameView.isHidden = true
      }
      
      if let contact = driver2Info.driverMobileNumber {
        self.lblDriver2Contact.text = String(format: "%@", contact)
      }else {
         self.driver2ContactView.isHidden = true
      }
      
    }else {
      self.driver2NameView.isHidden = true
      self.driver2ContactView.isHidden = true
    }
    
    let strAddress = Helper.getAddress(objVDetails.line1, strLine2: objVDetails.line2, strCityName: objVDetails.cityName, strStateName: objVDetails.stateName, pincode: objVDetails.pincode)
    
    if strAddress == "" {
      self.lineView[0].isHidden = true
      plantLocationView.isHidden = true
    }else {
      self.lineView[0].isHidden = false
      plantLocationView.isHidden = false
    }
    
    self.lblPlantLocation.text = strAddress
    //self.btnPlantLocation.setTitle(strAddress, for: .normal)
    
    if let imgurl = objVDetails.tMImageUrl {
      self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
  }
    
  @IBAction func actionLocation(_ sender: Any) {
    self.delegate?.actionVehicleLocation()
  }
  
  @IBAction func actionGoForPickup(_ sender: Any) {
    self.delegate?.actionPickup()
  }
}
