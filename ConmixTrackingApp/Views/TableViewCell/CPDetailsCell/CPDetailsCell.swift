//
//  CPDetailsCell.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 27/07/21.
//

import UIKit

class CPDetailsCell: UITableViewCell {
  
  @IBOutlet weak var imgView: CustomImageView!
  @IBOutlet weak var lblCategoryName: UILabel!
  @IBOutlet weak var btnNext: UIButton!
  @IBOutlet var lblTitle: [UILabel]!
  @IBOutlet var lblSubTitle: [UILabel]!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    self.lblSubTitle[0].text = Constants.Text.VehicleSubCategory.localized()

    self.lblSubTitle[1].text = Constants.Text.ManufacturerName.localized()

    self.lblSubTitle[2].text = Constants.Text.ManufacturingYear.localized()

    self.lblSubTitle[3].text = Constants.Text.CPModel.localized()

    self.lblSubTitle[4].text = Constants.Text.OperatorName.localized()

    self.lblSubTitle[5].text = Constants.Text.OperatorContact.localized()
    
    self.lblSubTitle[6].text = Constants.Text.SerialNumber.localized()

    self.btnNext.setTitle(Constants.Text.Next.localized(), for: .normal)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  //MARK:- Configure Cell
  func configureCell(_ objCPDetails: ConcretePumpDetails) {
    
    if let cpCategory = objCPDetails.concretePumpCategory {
      
      if let name = cpCategory.categoryName {
        self.lblCategoryName.text = String(format: "%@", name)
      }
      
    }
    
    if let cpCapacity = objCPDetails.concretePumpCapacity {
      self.lblSubTitle[0].text = Helper.getCapacity(NSNumber(value: cpCapacity))
    }
    
    if let companyName = objCPDetails.companyName {
      self.lblSubTitle[1].text = companyName
    }
    
    if let manufactureYear = objCPDetails.manufactureYear {
      let model = Helper.convertNumberToYear(NSNumber(value: manufactureYear))
      self.lblSubTitle[2].text = String(format: "%@", model)
    }
    
    if let concretePumpModel = objCPDetails.concretePumpModel {
      self.lblSubTitle[3].text = String(format: "%@", concretePumpModel)
    }
    
    if let operatorInfo = objCPDetails.operatorInfo {
      
      if let name = operatorInfo.operatorName {
        self.lblSubTitle[4].text = String(format: "%@", name)
      }
      
      if let number = operatorInfo.operatorMobileNumber {
        self.lblSubTitle[5].text = String(format: "%@", number)
      }
     
    }
    
    if let serialNo = objCPDetails.serialNumber, serialNo != "" {
      self.lblSubTitle[6].text = serialNo
    }else {
      self.lblSubTitle[6].text = "123456"
    }
    
    
    if let imgurl = objCPDetails.concretePumpImageUrl {
      self.imgView.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
  }
}
