//
//  CPOrderDetailReceiptCell.swift
//  ConmixTrackingApp
//
//  Created by Techniexe Infolabs on 28/07/21.
//

import UIKit

class CPOrderDetailReceiptCell: UITableViewCell {
  
  @IBOutlet weak var lblVendorName: UILabel!
  @IBOutlet weak var lblCreatedDate: UILabel!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet var lblTitleCollection: [UILabel]!
  @IBOutlet var lblDocumetTitleCollection: [UILabel]!
  @IBOutlet var documentimgViewCollection: [UIImageView]!
  @IBOutlet weak var btnChallan: UIButton!
  @IBOutlet weak var imgBuyerSign: UIImageView!
  @IBOutlet weak var imgDriverSign: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    self.lblTitleCollection[0].text = Constants.Text.BuyerSign.localized()
    self.lblTitleCollection[2].text = Constants.Text.Document.localized()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  //MARK:- Configure Cell
  func configureCell(_ dicOrderDetails: VendorOrderDetailsForTracking, isTransitMixer: Bool) {
    
    if isTransitMixer == true {
      self.lblTitleCollection[1].text = Constants.Text.DriverSign.localized()
    }else {
      self.lblTitleCollection[1].text = Constants.Text.OperatorSign.localized()
    }
    
    if let vendorInfo = dicOrderDetails.vendorInfo {
      
      if let companyname = vendorInfo.companyName {
       
        //self.lblVendorName.text = String(format: "%@", companyname)
        
        var strTitle: NSString!
        
        let sellerName = Constants.Text.SupplierTitle.localized()
        
        strTitle = "\(sellerName) : \(companyname)" as NSString
        
        let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(sellerName) : "))
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(companyname)"))
        
        
        self.lblVendorName.attributedText = strMutableAttributed
        
      }else {
        self.lblVendorName.text = ""
      }
    
    }else {
      self.lblVendorName.text = ""
    }

    
    
    if let vendorOrderInfo = dicOrderDetails.vendorOrderInfo {
      
      if let strOrderID = vendorOrderInfo._id {
        let orderID = Constants.Text.OrderID.localized()
        self.lblOrderID.text = String(format: "\(orderID) #%@", strOrderID)
      }else { self.lblOrderID.text = "" }
      
    }else { self.lblOrderID.text = "" }
    
    if let imgurl = dicOrderDetails.buyerDropSignature {
      self.imgBuyerSign.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
    if let imgurl = dicOrderDetails.driverDropSignature {
      self.imgDriverSign.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
   
    if let challanImgUrl = dicOrderDetails.challanImageUrl {
      self.documentimgViewCollection[0].setImage(url: challanImgUrl, style: .rounded, completion: nil)
      self.lblDocumetTitleCollection[0].text = Constants.Text.ChallanImage.localized()
    }
    
  }
  
}
