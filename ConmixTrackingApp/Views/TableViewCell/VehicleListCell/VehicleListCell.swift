//
//  VehicleListCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 17/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

@objc protocol VehicleListCellDelegate: AnyObject {
  
  func actionButton(_ tag: Int)
}

class VehicleListCell: UITableViewCell {
  
  @IBOutlet weak var imgVehicle: CustomImageView!
  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblType: UILabel!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var lblNumber: UILabel!
  @IBOutlet weak var btnList: UIButton!
  
  var delegate : VehicleListCellDelegate?
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: - Configure Cell
  func configureCell(_ objVDetails: TMDetails) {
    
    if let vCategory = objVDetails.tMCategory {
      
      if let name = vCategory.tMCategoryName {
        self.lblType.text = String(format: "%@", name)
      }
      
    }
    
    /*if let vSubCategory = objVDetails.tMSubCategory {
      
      //if let minloadCapacity = vSubCategory.loadCapacity, let maxloadCapacity = vSubCategory.loadCapacity {
        
        if let name = vSubCategory.tMSubCategoryName {
          
          //let minload = Helper.convertNumberToString(NSNumber(value: minloadCapacity))
          //let maxload = Helper.convertNumberToString(NSNumber(value: maxloadCapacity))
          self.lblType.text = String(format: "%@", arguments: [name])
          //self.lblType.text = String(format: "%@ - (%@ MT to %@ MT)", arguments: [name, minload, maxload])
        }
      //}
      
    }*/
    
    if let vRCNumber = objVDetails.tMRcNumber {
      self.lblNumber.text = String(format: "%@", vRCNumber)
    }
    
   /* if let createdAt = objVDetails.created_at {
      self.lblDate.text = Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: "dd MMM yyyy")
    }*/
    
    if let imgurl = objVDetails.tMImageUrl, imgurl != "" {
      self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
    }
  }
  
  @IBAction func actionOnCell(_ sender: Any) {
    let btn = sender as! UIButton
    self.delegate?.actionButton(btn.tag)
  }
  
  //MARK:- ConfigureCell
  func configureCell(_ objCPDetails: ConcretePumpDetails) {
    
    if let cpCategory = objCPDetails.concretePumpCategory {
      
      if let name = cpCategory.categoryName {
        self.lblName.text = String(format: "%@", name)
      }
      
    }
    
    if let cpCapacity = objCPDetails.concretePumpCapacity {
      self.lblNumber.text = Helper.getCapacity(NSNumber(value: cpCapacity))
    }
    
    
    if let imgurl = objCPDetails.concretePumpImageUrl {
      self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
    
    var strCompanyName: String = ""
    
    if let companyName = objCPDetails.companyName {
      
      strCompanyName = companyName
     
    }
    
    if let concretePumpModel = objCPDetails.concretePumpModel {
     
      if strCompanyName != "" {
      
        strCompanyName += " - \(concretePumpModel)"
      
      }else {
      
        strCompanyName += "\(concretePumpModel)"
      
      }
      
    }
    
    self.lblType.text = String(format: "%@", strCompanyName)
    
  }
}
