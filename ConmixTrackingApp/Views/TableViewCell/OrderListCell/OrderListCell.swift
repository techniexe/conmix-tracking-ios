//
//  OrderListCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 27/01/21.
//  Copyright © 2021 Techniexe Infolabs. All rights reserved.
//

import UIKit

@objc protocol OrderListCellDelegate: AnyObject {
  
  func actionButton(_ tag: Int)
}

class OrderListCell: UITableViewCell {
  
  // MARK: - @IBOutlets
  @IBOutlet weak var imgVehicle: CustomImageView!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet weak var lblBuyerCompanyName: UILabel!
  @IBOutlet weak var lblType: UILabel!
  @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var lblNumber: UILabel!
  @IBOutlet weak var lblStatus: UILabel!
  @IBOutlet weak var btnList: UIButton!
  @IBOutlet weak var btnStatus: UIButton!
  
  // MARK: - @vars
  var delegate : OrderListCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: - Configure Cell
  func configureCell(_ dicOrder: VendorOrderDetailsForTracking, isTransitMixer: Bool) {
    
    if let vendorOrderInfo = dicOrder.vendorOrderInfo {
      
      if let infoID = vendorOrderInfo._id {
        let orderID = Constants.Text.ORDERID.localized()
        self.lblOrderID.text = String(format: "\(orderID) #%@", infoID)
      }else { self.lblOrderID.text = "" }
      
    }
    
    if let buyerInfo = dicOrder.buyerInfo {
      
      if let companyname = buyerInfo.companyName {
        self.lblBuyerCompanyName.text = String(format: "%@", companyname)
      }else {
        self.lblBuyerCompanyName.text = ""
      }
      
    }
  
    
    if isTransitMixer == true {
      
      if let vSubCategory = dicOrder.tMSubCategoryInfo {
        
        //if let minloadCapacity = vSubCategory.min_load_capacity, let maxloadCapacity = vSubCategory.max_load_capacity {
          
          if let name = vSubCategory.tMSubCategoryName {
            
            //let minload = Helper.convertNumberToString(NSNumber(value: minloadCapacity))
            //let maxload = Helper.convertNumberToString(NSNumber(value: maxloadCapacity))
            self.lblType.text = String(format: "%@", arguments: [name])
            //self.lblType.text = String(format: "%@ - (%@ MT to %@ MT)", arguments: [name, minload, maxload])
          
          }else { self.lblType.text = ""}
        
        //}
        
      }
      
      if let vehicleInfo = dicOrder.tMInfo {
        
        if let vRCNumber = vehicleInfo.tMRCNumber {
          self.lblNumber.text = String(format: "%@", vRCNumber)
        }else { self.lblNumber.text = "" }
        
        if let imgurl = vehicleInfo.tMImageUrl {
          self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
        }
      }
      
      
    }
    
    else {
      
      var strCompanyName: String = ""
      
      if let dicCPInfo = dicOrder.CPInfo {
        
        if let companyName = dicCPInfo.companyName {
          
          strCompanyName = companyName
          
        }
        
        if let concretePumpModel = dicCPInfo.concretePumpModel {
          
          if strCompanyName != "" {
            
            strCompanyName += " - \(concretePumpModel)"
            
          }else {
            
            strCompanyName += "\(concretePumpModel)"
            
          }
          
        }
        
        self.lblType.text = String(format: "%@", strCompanyName)
        
        if let cpCapacity = dicCPInfo.concretePumpCapacity {
          self.lblNumber.text = Helper.getCapacity(NSNumber(value: cpCapacity))
        }
        
        
        if let imgurl = dicCPInfo.concretePumpImageUrl {
          self.imgVehicle.setImage(url: imgurl, style: .squared, completion: nil)
        }
        
        
      }
      
    }
    
    let AssignedOn = Constants.Text.AssignedOn.localized()
    
    if let createdAt = dicOrder.createdAt {
      self.lblDate.text = "\(AssignedOn) : \(Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: Constants.DateFormats.deliveryDate))"
    }else { self.lblDate.text = ""}
    
    if let eventStatus = dicOrder.eventStatus {
      
      var strStatus: String = ""
      
      
      if eventStatus == Constants.EventStatus.TMASSIGNED {
        strStatus = Constants.Text.TruckAssign.localized()
        self.btnStatus.backgroundColor = Constants.Colors.assignedColor
      }else if eventStatus == Constants.EventStatus.PICKUP {
        self.btnStatus.backgroundColor = Constants.Colors.pickupColor
        strStatus = Constants.Text.PickUp.localized()
      }else if eventStatus == Constants.EventStatus.CPASSIGNED {
        self.btnStatus.backgroundColor = Constants.Colors.assignedColor
        strStatus = Constants.Text.CPAssigned.localized()
      }else if eventStatus == Constants.EventStatus.DELAYED {
        self.btnStatus.backgroundColor = Constants.Colors.delayedColor
        strStatus = Constants.Text.Delay.localized()
      }
      
      //self.btnStatus.backgroundColor = UIColor.init(hexString: "#FFC107")
      self.btnStatus.setTitle(strStatus, for: .normal)
      
      
      /*let status = Constants.Text.Status.localized()
      
      let strTitle = String(format: "\(status) : %@",strStatus) as NSString
      let strMutableAttributed =  NSMutableAttributedString(string: strTitle as String)
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(status) : "))
      strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0), NSAttributedString.Key.foregroundColor: UIColor.red], range: strTitle.range(of: "\(strStatus)"))
      
      self.lblStatus.attributedText = strMutableAttributed*/
      
    }else {
      
      //self.lblStatus.text = ""
      
    }
    
  }
  
  @IBAction func actionOnCell(_ sender: Any) {
     let btn = sender as! UIButton
     self.delegate?.actionButton(btn.tag)
   }
}
