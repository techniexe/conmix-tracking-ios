//
//  OrderInfoCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

@objc protocol OrderInfoViewDelegate: AnyObject {
 
  func actionProceedOrder()

}

class OrderInfoCell: UITableViewCell {
  
  @IBOutlet weak var lblSupplierInfo: UILabel!
  @IBOutlet weak var lblBuyerInfo: UILabel!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet weak var lblOrderQty: UILabel!
  @IBOutlet weak var lblPickupQty: UILabel!
  @IBOutlet weak var lblRoyalityPass: UILabel!
  @IBOutlet weak var lblRemainingQty: UILabel!
  @IBOutlet weak var btnProceed: UIButton!
  @IBOutlet var lblTitleCollection: [UILabel]!
  
  var delegate : OrderInfoViewDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    //self.lblSupplierTitle.text = Constants.Text.SupplierTitle.localized()
    //self.lblBuyerTitle.text = Constants.Text.BuyerTitle.localized()
    self.lblTitleCollection[0].text = Constants.Text.PickUpQty.localized()
    self.lblTitleCollection[1].text = Constants.Text.RoyaltyPass.localized()
    self.lblTitleCollection[2].text = Constants.Text.BalanceQty.localized()
    //self.lblTitleCollection[3].text = Constants.Text.OrderQty.localized()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MAR: - Configure Cell
  func configureCell(_ dicOrder: VendorOrderDetailsForTracking) {
    
    if let supplierInfo = dicOrder.vendorInfo {
      
      if let companyname = supplierInfo.companyName {
        
        //self.lblSupplierInfo.text = String(format: "%@", name)
        self.lblSupplierInfo.isHidden = false
        
        var strTitle: NSString!
        
        let sellerName = Constants.Text.SupplierTitle.localized()
        
        strTitle = "\(sellerName) : \(companyname)" as NSString
        
        let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(sellerName) : "))
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(companyname)"))
        
        
        self.lblSupplierInfo.attributedText = strMutableAttributed
        
        
        
        
        
      }else {
        self.lblSupplierInfo.text = ""
        self.lblSupplierInfo.isHidden = true
      }
    }else {
      self.lblSupplierInfo.text = ""
      self.lblSupplierInfo.isHidden = false
    }
    
    if let buyerInfo = dicOrder.buyerInfo {
      
      if let companyname = buyerInfo.companyName {
        //self.lblBuyerInfo.text = String(format: "%@", name)
        self.lblBuyerInfo.isHidden = false
        
        var strTitle: NSString!
        
        let sellerName = Constants.Text.BuyerTitle.localized()
        
        strTitle = "\(sellerName) : \(companyname)" as NSString
        
        let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(sellerName) : "))
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(companyname)"))
        
        
        self.lblBuyerInfo.attributedText = strMutableAttributed
        
      }else {
        self.lblBuyerInfo.text = ""
        self.lblBuyerInfo.isHidden = true
      }
    }else {
      self.lblBuyerInfo.text = ""
      self.lblBuyerInfo.isHidden = true
    }
    
    if let strOrderID = dicOrder.vendorOrderInfo?._id {
      
      let orderID = Constants.Text.OrderID.localized()
      
      self.lblOrderID.text = String(format: "\(orderID) #%@", strOrderID)
    }else {
      self.lblOrderID.text = ""
    }
    
    /*if let deliveredQty = dicOrder.delivered_quantity {
      let qty = Helper.getQuantity(NSNumber(value: deliveredQty))
      self.lblOrderQty.text = String(format: "%@ MT", qty)
    }*/
    
    if let pickupQty = dicOrder.pickupQuantity {
       let qty = Helper.getQuantity(NSNumber(value: pickupQty))
       self.lblPickupQty.text = String(format: "%@", qty)
      //self.lblPickupQty.text = String(format: "%.01f Cu.Mtr", pickupQty)
    }
    
    if let royalityQty = dicOrder.pickupQuantity {
      let qty = Helper.getQuantity(NSNumber(value: royalityQty))
      self.lblRoyalityPass.text = String(format: "%@", qty)
      //self.lblRoyalityPass.text = String(format: "%.01f Cu.Mtr", royalityQty)
    }
  
    if let remQty = dicOrder.remainingQuantity {
      let qty = Helper.getQuantity(NSNumber(value: remQty))
      self.lblRemainingQty.text = String(format: "%@", qty)
      //self.lblRemainingQty.text = String(format: "%.01f Cu.Mtr", remQty)
    }
  }
  
  @IBAction func actionProceed(_ sender: Any) {
    self.delegate?.actionProceedOrder()
  }
  
}

extension Double {
    static func / (left: Double, right: Double) -> Double {
        return left - right
    }
}
