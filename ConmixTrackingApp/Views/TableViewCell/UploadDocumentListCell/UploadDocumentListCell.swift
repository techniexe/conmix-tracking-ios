//
//  UploadDocumentListCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit

class UploadDocumentListCell: UITableViewCell {
  
  @IBOutlet weak var imgDocument: CustomImageView!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblLine: UILabel!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: - Configure Cell
  func configureCell(_ dicDoc: UploadDocument) {
    
    if let title = dicDoc.title {
      self.lblTitle.text = title
    }
    
    if let img = dicDoc.img {
      self.imgDocument.image = img
    }
    
    
  }
}
