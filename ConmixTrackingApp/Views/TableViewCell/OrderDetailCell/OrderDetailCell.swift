//
//  OrderDetailCell.swift
//  Logistic
//
//  Created by Techniexe Infolabs on 22/07/20.
//  Copyright © 2020 Techniexe Infolabs. All rights reserved.
//

import UIKit
import SDWebImage

protocol OrderDocumentDelegate {
  func documentUrl(_ strURL: String, index:  Int)
}

class OrderDetailCell: UITableViewCell {

  @IBOutlet weak var lblTitle: UILabel!
 // @IBOutlet weak var lblDate: UILabel!
  @IBOutlet weak var lblOrderID: UILabel!
  @IBOutlet weak var lblPickupQty: UILabel!
  @IBOutlet weak var lblRoyalityPass: UILabel!
  @IBOutlet weak var lblRemainingQty: UILabel!
//  @IBOutlet weak var objCollectionView: UICollectionView!
  @IBOutlet weak var imgBuyerSign: UIImageView!
  @IBOutlet weak var imgDriverSign: UIImageView!
  @IBOutlet var lblDocumetTitleCollection: [UILabel]!
  @IBOutlet var documentimgViewCollection: [UIImageView]!
  @IBOutlet var lblTitleCollection: [UILabel]!
  @IBOutlet weak var invoiceDataView: UIView!
  
  var arrDocument: [String] = []
  var delegate: OrderDocumentDelegate? = nil
  
  var dicOrderDetails: VendorOrderDetailsForTracking? = nil
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    // Initialization code
    //self.objCollectionView.register(UINib(nibName:"ImageViewCollectionCell", bundle:Bundle.main), forCellWithReuseIdentifier: Constants.CollectionViewCellIdentifier.imageViewCollectionCellID )
    //self.objCollectionView.delegate  = self
    //self.objCollectionView.dataSource = self
    self.lblTitleCollection[0].text = Constants.Text.PickUpQty.localized()
    self.lblTitleCollection[1].text = Constants.Text.RoyaltyPass.localized()
    self.lblTitleCollection[2].text = Constants.Text.BalanceQty.localized()
    self.lblTitleCollection[3].text = Constants.Text.BuyerSign.localized()
    self.lblTitleCollection[4].text = Constants.Text.DriverSign.localized()
    self.lblTitleCollection[5].text = Constants.Text.Document.localized()
  
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  
  // MAR: - Configure Cell
  func configureCell(_ dicOrder: VendorOrderDetailsForTracking) {
    
    self.dicOrderDetails = nil
    
    self.dicOrderDetails = dicOrder
    
    if let supplierInfoInfo = dicOrder.vendorInfo {
      
      if let companyname = supplierInfoInfo.companyName {
        
        var strTitle: NSString!
        
        let sellerName = Constants.Text.SupplierTitle.localized()
        
        strTitle = "\(sellerName) : \(companyname)" as NSString
        
        let strMutableAttributed =  NSMutableAttributedString(string:strTitle as String)
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15.0), NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(sellerName) : "))
        
        strMutableAttributed.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13.0),NSAttributedString.Key.foregroundColor: UIColor.black], range: strTitle.range(of: "\(companyname)"))
        
        
        self.lblTitle.attributedText = strMutableAttributed
        
        
        //self.lblTitle.text = String(format: "%@", name)
      }
    }
    
//    if let createdAt = dicOrder.createdAt {
//      self.lblDate.text = Date.convertDateUTCToLocal(strDate: createdAt , oldFormate: Constants.DateFormats.iso, newFormate: Constants.DateFormats.deliveryDate)
//    }
    
    if let strOrderID = dicOrder.vendorOrderInfo?._id {
      let orderID = Constants.Text.OrderID.localized()
      self.lblOrderID.text = String(format: "\(orderID) #%@", strOrderID)
    }
    
//    if let deliveredQty = dicOrder.delivered_quantity {
//      let qty = Helper.getQuantity(NSNumber(value: deliveredQty))
//      self.lblOrderQty.text = String(format: "%@ MT", qty)
//
//    }
    
    if let pickupQty = dicOrder.pickupQuantity {
      let qty = Helper.getQuantity(NSNumber(value: pickupQty))
      self.lblPickupQty.text = String(format: "%@", qty)
      //self.lblPickupQty.text = String(format: "%.01f Cu.Mtr", pickupQty)
    }
    
    if let royalityQty = dicOrder.pickupQuantity {
      let qty = Helper.getQuantity(NSNumber(value: royalityQty))
      self.lblRoyalityPass.text = String(format: "%@", qty)
      //self.lblRoyalityPass.text = String(format: "%.01f Cu.Mtr", royalityQty)
    }
    
    if let remainingQty = dicOrder.remainingQuantity {
      let qty = Helper.getQuantity(NSNumber(value: remainingQty))
      self.lblRemainingQty.text = String(format: "%@", qty)
      //self.lblRemainingQty.text = String(format: "%.01f Cu.Mtr", remainingQty)
    }
    
    
    SDImageCache.shared.clearMemory()
    SDImageCache.shared.clearDisk()
    
    if let imgurl = dicOrder.buyerDropSignature, imgurl != "" {
      //self.imgBuyerSign = nil
      self.imgBuyerSign.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
    if let imgurl = dicOrder.driverDropSignature, imgurl != "" {
      //self.imgDriverSign = nil
      
      self.imgDriverSign.setImage(url: imgurl, style: .squared, completion: nil)
    }
    
    
    if let royaltyImgUrl = dicOrder.royaltyPassImageUrl {
      self.documentimgViewCollection[0].setImage(url: royaltyImgUrl, style: .rounded, completion: nil)
      self.lblDocumetTitleCollection[0].text = Constants.Text.RoyaltyImage.localized()
    }
    
    if let wayslipUrl = dicOrder.waySlipImageUrl {
      self.documentimgViewCollection[1].setImage(url: wayslipUrl, style: .rounded, completion: nil)
      self.lblDocumetTitleCollection[1].text = Constants.Text.WaySlipImage.localized()
    }
    
    if let challanImgUrl = dicOrder.challanImageUrl {
      self.documentimgViewCollection[2].setImage(url: challanImgUrl, style: .rounded, completion: nil)
      self.lblDocumetTitleCollection[2].text = Constants.Text.ChallanImage.localized()
    }
    
    /*if let invoiceUrl = dicOrder.invoiceImageUrl, invoiceUrl != "" {
      self.documentimgViewCollection[3].setImage(url: invoiceUrl, style: .rounded, completion: nil)
      self.lblDocumetTitleCollection[3].text = Constants.Text.InvoiceImage.localized()
      self.invoiceDataView.isHidden = false
    }else {
      self.invoiceDataView.isHidden = true
    }*/
    
    if let invoiceUrl = dicOrder.invoiceUrl, invoiceUrl != "" {
      //self.documentimgViewCollection[3].setImage(url: invoiceUrl, style: .rounded, completion: nil)
      self.lblDocumetTitleCollection[3].text = Constants.Text.BuyerInvoice.localized()
      self.invoiceDataView.isHidden = false
    }else {
      self.invoiceDataView.isHidden = true
    }
    
    //self.objCollectionView.reloadData()
  }
  
  @IBAction func actionDocument(_ sender: Any) {
    
    let btn = sender as! UIButton
    
    switch btn.tag {
    case 0:
      if let imgurl = dicOrderDetails?.buyerDropSignature {
        self.delegate?.documentUrl(imgurl, index: 0)
      }
    case 1:
      if let imgurl = dicOrderDetails?.driverDropSignature {
        self.delegate?.documentUrl(imgurl, index: 1)
      }
    case 2:
      if let imgurl = dicOrderDetails?.royaltyPassImageUrl {
        self.delegate?.documentUrl(imgurl, index: 2)
      }
    case 3:
      if let imgurl = dicOrderDetails?.waySlipImageUrl {
        self.delegate?.documentUrl(imgurl, index: 3)
      }
    case 4:
      if let imgurl = dicOrderDetails?.challanImageUrl {
        self.delegate?.documentUrl(imgurl, index: 4)
      }
    case 5:
      /*if let imgurl = dicOrderDetails?.invoiceImageUrl {
        self.delegate?.documentUrl(imgurl)
      }*/
      if let invoiceUrl = dicOrderDetails?.invoiceUrl, invoiceUrl != "" {
        self.delegate?.documentUrl(invoiceUrl, index: 5)
      }
      
    default:
      print("")
    }
    
  }
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
//extension OrderDetailCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//  func numberOfSections(in collectionView: UICollectionView) -> Int {
//    return 1
//  }
//
//  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//    return self.arrDocument.count
//
//  }
//
//  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//    guard let objCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCellIdentifier.imageViewCollectionCellID, for: indexPath) as? ImageViewCollectionCell else {
//      return UICollectionViewCell()
//    }
//
//    objCell.configureCell(self.arrDocument[indexPath.item])
//
//    return objCell
//
//  }
//
////  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
////
////    return CGSize.
////
////  }
//
//}
